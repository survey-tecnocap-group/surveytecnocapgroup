
<?php

//includo il SETTING per il DEFINE per i valori interessati alle pagine
include_once ($_SERVER['DOCUMENT_ROOT']."/newform/setting/define_path.php");

class json_getComuni { 	

    /**
     * init la ricerca passando come valori:
     * @var string key_Value= testo di ricerca;
     * @var string key = indicare la KEY nel JSON: nome, cap, etc....
     */
    public function init_getComuni($key_value, $key) {
        //apro il file JSON 
        $file = file_get_contents(HOME_WEBAPP."json/comuni.json");

        //mi prendo i valori del file JSON
        $json = json_decode($file, true);

        //chiamo la funzione per cercare all'interno del JSON
        $returnValue = $this->findJSON($json, $key_value, $key);

        //ritorno il valore
        return json_encode($returnValue);
    }

    private function findJSON($json, $search_val, $key) {

        //inizio a fare il ciclo all'interno del contenuto del JSON
        foreach($json as $elem) {
            //dichiaro l'array da passare come ritorno
            $arrayFind=array();

        if(is_array($elem[$key])) $arrayFind=$elem[$key];
        else array_push($arrayFind,$elem[$key]);

            //questo ultieriore ciclo mi permette di controllare all'interno del valore
            //del campo KEY trovato nel JSON, perchè alcuni valori di ritorno possono essere
            //STRING oppure ARRAY 
            foreach($arrayFind as $findSTR) {
                if (strpos(strtolower($findSTR),strtolower($search_val)) !== false) {
                    $citta=$elem['nome'];
                    $zona=$elem['zona']['nome'];
                    $regione=$elem['regione']['nome'];
                    $provincia=$elem['provincia']['nome'];
                    $siglaProvinciale=$elem['sigla'];
                    $popolazione=$elem['popolazione'];
                    $arrayCap=$elem['cap'];
                    if(count($arrayCap))
                        $cap=$arrayCap;
                    else
                        $cap=$arrayCap;

                    return ["citta"=>$citta,
                            "zona"=>$zona, 
                            "regione"=>$regione,
                            "provincia"=>$provincia,
                            "sigla_provincia"=>$siglaProvinciale,
                            "cap"=>$cap,
                            "popolazione"=>$popolazione];
                }
            }
        }

        //ritorno un array vuoto perchè significa che la ricerca non ha avuto successo
        return array();
    }
}
?>