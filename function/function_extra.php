<?php

//includo le variabili definite globali
include_once ($_SERVER['DOCUMENT_ROOT']."/newform/setting/define_variable.php");

//includo il file per la gestione dei comuni
include_once ($_SERVER['DOCUMENT_ROOT']."/newform/function/get_comuni.php");

//in base al TYPE capisco quale operazione è stata richiesta
if(isset($_GET['type'])) {
    switch ($_GET['type']) {
        //mi cerco il comune il base al nome/cap
        case "getComuni":
            echo getComuni("milano;;nome");
            break;
        
        //mi calcolo da distanza in MT tra 2 coordinate GPS
        case "getDistancePoint":
            $coordinate_start = "40.7328872,14.568861";
            $coordinate_end = "40.7403473,14.5746768";
            echo points_distance($coordinate_start,$coordinate_end). " Km";
    }
}

/**
 * Mi ricavo i dati del comune
 * @param class Session = passo il valore della classe session creata
 * @return json_encode valore con il risultato
 * 
 */
function getComuni($data) {
    //creo la il collegamento con la CLASS json_getComuni
    $json_getComuni = new json_getComuni();

    //leggo il contenuto nella session
	$array_jsonGetComuni=explode(ARRAY_IMPLODE,$data);

    //controllo se l'array con i valori di ritorno è conforme per la ricerca
    if(count($array_jsonGetComuni)<=1)
        return -1;

    //ritorno il valore della ricerca per il comune
    //key_value, key
    return $json_getComuni->init_getComuni($array_jsonGetComuni[0], $array_jsonGetComuni[1]);
}

/**
 * Mi ricavo la distanza tra 2 coordine GPS
 * @param string coordinate_start = punto di partenza
 * @param string coordinate_end = punto d'arrivo
 * @return float ritorna la distanza in mt
 * 
 */
function points_distance ($coordinate_start, $coordinate_end) {
    define('RAGGIO_QUADRATICO_MEDIO', 6372.795477598);
    list($decLatA, $decLonA) = array_map('trim', explode(',', $coordinate_start));
    list($decLatB, $decLonB) = array_map('trim', explode(',', $coordinate_end));
    $radLatA = pi() * $decLatA / 180;
    $radLonA = pi() * $decLonA / 180;
    $radLatB = pi() * $decLatB / 180;
    $radLonB = pi() * $decLonB / 180;
    $phi = abs($radLonA - $radLonB);
    $distanza = acos (
         (sin($radLatA) * sin($radLatB)) +
         (cos($radLatA) * cos($radLatB) * cos($phi))
    );
    return round($distanza * RAGGIO_QUADRATICO_MEDIO,2);
}

/**
 * mi genero un random color
 * @return string ritorna il valore HEX
 * 
 */
public function alg_random_color() {
    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
}

/**
 * mi pulisco le stringhe prima di aggiungere il testo nell'SQL
 * @param string prendo la strina come input
 * @return string ritorno la stringa pulita da caratteri speciali causare problemi ai campi SQL
 * 
 */
public function mysql_escape_text($txt) {
    if(is_array($txt))
        return array_map(__METHOD__, $txt);

    if(!empty($txt) && is_string($txt)) {
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $txt);
    }
    return $txt;
}
    
/**
 * converto eventuali caratteri speciale che può causare error
 * nella codifica, converto tutto in UTF-8
 * @param string prende in ingresso una string e la converte in UTF-8
 * @return string ritorno la stringa convertita
 *  
 */
function convert_to_utf8_recursively($data){
    if( is_string($data) ){
       return mb_convert_encoding($data, 'UTF-8', 'UTF-8');
    }
    elseif( is_array($data) ){
       $ret = [];
       foreach($data as $i => $d){
         $ret[$i] = $this->convert_to_utf8_recursively($d);
       }
       return $ret;
    }
    else{
       return $data;
    }
}

/**
 * aggiungo dei minuti al time che viene passato come start
 * posso aggiungere i minuti al time con la variabile $timeAdd
 * e togliere i minuti al time con la variabile $timeSub
 * @param string $timeStart orario iniziale H:i:s (ore/minuti/secondi)
 * @param string $timeAdd format H:i:s (ore/minuti/secondi) aggiunge minuti al time
 * @param string $timeSub format H:i:s (ore/minuti/secondi) sottrae minuti al time
 * @return date_time format H:i:s (ore/minuti/secondi) ritorna il valore del time iniziale modificato con l'operazione scelta
 * 
 */
public function time_addsub_minute($timeStart, $timeAdd, $timeSub) {
    $dateTime = date_create_from_format('H:i:s', $timeStart);
    if($timeSub!="") {
        $arrayTime=explode(":", $timeSub);
        $hour=$arrayTime[0];
        $minute=$arrayTime[1];
        date_sub($dateTime, date_interval_create_from_date_string($minute.' minutes'));
        return date_format($dateTime, 'H:i:s');
    }
    if($timeAdd!="") { 
        $arrayTime=explode(":", $timeAdd);
        $hour=$arrayTime[0];
        $minute=$arrayTime[1];
        date_add($dateTime, date_interval_create_from_date_string($minute.' minutes'));
        return date_format($dateTime, 'H:i:s');
    }
}

/**
 * converto il time in secondi
 * @param string format H:i:s (ora/minuti/secondi);
 * @return integer ritorno un valore intero che sono i secondi del time convertito
 * 
 */
public function time_to_sec($time) {
    $sec = 0;
    foreach (array_reverse(explode(':', $time)) as $k => $v) $sec += pow(60, $k) * $v;
    return intval($sec);
}

/**
 * 	prendo una data in format "Y-m-d" e mi ritorna un'array[giorno->nome del giorno, 
 *                                                          mese->nome del mese, 
 *                                                          anno->anno in numero,
 *                                                          numero_giorno->numero del giorno];
 * @param date prende come valore in ingresso la data in format Y-m-d
 * @return array ["giorno", "mese", "anno", "numero_giorno"];
 * 
 */
public function date_to_italiano($data){
    // definisco due array arbitrarie
    $giorni = array("Domenica", "Lunedì", "Martedì", "Mercoledì", "Giovedì", "Venerdì", "Sabato");
    $mesi = array("Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre","Novembre", "Dicembre");

    // giorno della settimana in italiano
    $numero_giorno_settimana = date_format(date_create($d), "w");
    $nome_giorno = $giorni[intval($numero_giorno_settimana)];

    // nome del mese in italiano
    $numero_mese = date_format(date_create($d), "n");;
    $nome_mese = $mesi[intval($numero_mese)-1];

    // numero dell'anno
    $anno = date("Y");
    return ["giorno"=>ucfirst($nome_giorno), "mese"=>ucfirst($nome_mese), "anno"=>$anno, "numero_giorno"=> date_format(date_create($d), "d")];
}

?>