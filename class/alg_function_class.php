<?PHP


class FunctionPrimaryClass { 	
	//mi dichiaro le variabili pubbliche da utilizzare nelle funzioni
    public $dbConnection;

    /**
     * mi setto il valore della connessione MySql nella variabile pubblica
     * @param string passo la connessione come parametro
     */
    public function alg_fnt_setConnection($connection) {
      $this->dbConnection=$connection;
  }  

  public function fnt_Login($username, $password) {
        //converto la password in md5 per la privacy
    $password=md5($password);

        //eseguo la query
    $query="SELECT * FROM tbl_user WHERE LOWER(user_name)='".strtolower($username)."' AND user_password='".$password."'";
    $result=mysqli_query($this->dbConnection,$query);
    if($result) {
        if($row=mysqli_fetch_array($result))
            return $row['id_user'];
    }
    return -1;        
}


public function alg_fnt_getTextView($view, $language){
    $query="SELECT * FROM tbl_text WHERE view='".$view."'";
    $result=mysqli_query($this->dbConnection, $query);
    if($result){
        return $this->alg_fnt_getTypeView($view, $result, $language);
    }

    return 0;
}



public function alg_fnt_getInfoSurvey($id_survey){
    $query="SELECT * FROM tbl_survey WHERE id_survey='".$id_survey."'";
    $result=mysqli_query($this->dbConnection, $query);
    if($result){
        while($row=mysqli_fetch_array($result)){

            $data = [
                'id_survey'=>$row['id_survey'],
                'title_survey'=>htmlentities($row['title_survey']),
                'description_survey'=>htmlentities($row['description_survey']),
                'language_survey'=>$row['language_survey']
            ];

            return $data;

        }
    }

    return 0;
}


public function alg_fnt_getFullSurvey($id_survey){
        //RICAVO I DATI RELATIVI AL SURVEY: TITOLO, DESCRIZIONE...
    $data_full_survey = array();
    $data_final_survey = array();
    $query="SELECT * FROM tbl_survey WHERE id_survey='".$id_survey."'";
    $result=mysqli_query($this->dbConnection, $query);
    if($result){
        while($row=mysqli_fetch_array($result)){
            $data_survey = [
                'id_survey'=>$row['id_survey'],
                'title_survey'=>htmlentities($row['title_survey']),
                'description_survey'=>htmlentities($row['description_survey']),
                'language_survey'=>$row['language_survey']
            ];

        }


        //... E LI INSERISCO NELL'ARRAY
        array_push($data_full_survey,  $data_survey);


        //ARRAY VUOTO DA POPOLARE CON TUTTE LE CATEGORIE:
        //AVREMO LA CATEGORIA CON I SUOI DATI E AL SUO INTERNO LE DOMANDE
        $data_categories = array();
        $query_categories="SELECT DISTINCT tbl_category.id_category, tbl_category.title_category, tbl_category.description_category FROM tbl_category, relation_survey_category WHERE relation_survey_category.id_survey_rsc = $id_survey AND relation_survey_category.id_category_rsc = tbl_category.id_category; ";
        $result_categories=mysqli_query($this->dbConnection, $query_categories);
        
        if($result_categories){

            while($row_categories=mysqli_fetch_array($result_categories)){

        //RICAVIAMO LE DOMANDE IN BASE ALL'ID DELLA CATEGORIA
                $category = $row_categories['id_category'];
                $query_questions="SELECT DISTINCT tbl_question.id_question, tbl_question.title_question FROM tbl_question, relation_category_question WHERE relation_category_question.id_category_rcq = $category AND relation_category_question.id_question_rcq = tbl_question.id_question";
                $result_questions=mysqli_query($this->dbConnection, $query_questions);

                if($result_questions){
                    $grouped_data_questions = array();


                    while($row_questions=mysqli_fetch_array($result_questions)){
                        $single_data_questions = array();

                //DATI DELLA SINGOLA CATEGORIA + LE DOMANDE ASSOCIATE
                        $single_data_questions = [
                            'id_question' => $row_questions['id_question'],
                            'title_question' => htmlentities($row_questions['title_question']),

                        ];

                        array_push($grouped_data_questions, $single_data_questions);


                    }

                }


        //DATI DELLA SINGOLA CATEGORIA + LE DOMANDE ASSOCIATE
                $single_data_category = [
                    'id_category' => $row_categories['id_category'],
                    'title_category' => htmlentities($row_categories['title_category']),
                    'description_category' => htmlentities($row_categories['description_category']),
                    'questions_category' => $grouped_data_questions
                ];

                
                array_push($data_categories, array("single_category" => $single_data_category));

                






            }

        }
        //array_push($data_full_survey, array("all_categories" => $data_categories));
        array_push($data_full_survey, array("all_categories" => $data_categories));
        return json_encode($data_full_survey, JSON_PRETTY_PRINT);

    }



    return 0;
}


public function alg_fnt_fromSurveyToCategories($id_survey){
    $data_categories = array();
    $query="SELECT DISTINCT tbl_category.id_category, tbl_category.title_category, tbl_category.description_category FROM tbl_category, relation_survey_category WHERE relation_survey_category.id_survey_rsc = '".$id_survey."'";
    $result=mysqli_query($this->dbConnection, $query);
    if($result){
        while($row=mysqli_fetch_array($result)){

            array_push($data_categories, [
                'id_category' => $row['id_category'],
                'title_category' => htmlentities($row['title_category']),
                'description_category' => htmlentities($row['description_category'])
            ]);

            

        }

        return json_encode($data_categories);
    }

    return 0;
} 


public function alg_fnt_fromCategoryToQuestions($id_category){
    $query="SELECT * FROM relation_category_question WHERE id_category_rcq='".$id_category."'";
    $result=mysqli_query($this->dbConnection, $query);
    if($result){
        while($row=mysqli_fetch_array($result)){

            return alg_fnt_getQuestionsByCategory($row['id_category_rcs']);

        }
    }

    return 0;
}



public function alg_fnt_getAllSurveys(){
    $data_survey = array();
    $query="SELECT * FROM tbl_survey";
    $result=mysqli_query($this->dbConnection, $query);
    if($result){
        while($row=mysqli_fetch_array($result)){
            $survey_related_form = $row['id_survey'];
            $query_total="SELECT * FROM tbl_form WHERE survey_related_form = '$survey_related_form'";
            $result_total=mysqli_query($this->dbConnection, $query_total);
            if($result_total){
            $rowcount=mysqli_num_rows($result_total);

            array_push($data_survey, [
            'id_survey' => $row['id_survey'],
            'title_survey' => htmlentities($row['title_survey']),
            'description_survey' => htmlentities($row['description_survey']),
            'language_survey' => htmlentities($row['language_survey']),
            'data_create_survey' => $row['data_create_survey'], 
            'data_start_survey' => $row['data_start_survey'], 
            'data_end_survey' => $row['data_end_survey'], 
            'is_active_survey' => $row['is_active_survey'],
            'interviewed_number_survey' => $rowcount
        ]);

        }
    }

    return $data_survey;
    }

    return 0;
}


public function alg_fnt_getInfoCategory($id_category){
    $query="SELECT * FROM tbl_category WHERE id_category='".$id_category."'";
    $result=mysqli_query($this->dbConnection, $query);
    if($result){
        while($row=mysqli_fetch_array($result)){

            return [
                'id_category'=>$row['id_category'],
                'title_category'=>$row['title_category'],
                'description_category'=>$row['description_category']
            ];

        }
    }

    return 0;
}


public function alg_fnt_getInfoQuestion($id_question){
    $query="SELECT * FROM tbl_question WHERE id_question='".$id_question."'";
    $result=mysqli_query($this->dbConnection, $query);
    if($result){
        while($row=mysqli_fetch_array($result)){

            return [
                'id_question'=>$row['id_question'],
                'title_question'=>$row['title_question'],
            ];

        }
    }

    return 0;
}




    /**
    * ----------------------TYPE VIEW---------------------------------
    * TYPE VIEW => TRAMITE I VALORI RICAVATI DALLA VIEW VADO AD AVVALORARE LE VARIABILI
    * CHE PASSERO' SUCCESSIVAMENTE COME SEGNAPOSTI
    * @param string $view = nome della view che vado a visualizzare;
    * @param string $language = nome della lingua scelta;
    * @return integer ritorna -1 in caso di errore/non presente in database, oppure id_user se viene trovato l'utente 
    *
    */

    public function alg_fnt_getTypeView($view, $result, $language){
        switch( strtolower($view)){
            case 'global':
            $title = "";
            $submit = "";
            $cancel = "";

            while($row=mysqli_fetch_array($result)){
                if((in_array("title_page_".$language, $row)) && ($title=="")){ $title=$row['value'];} 
                if((in_array("submit_".$language, $row)) && ($submit=="")){ $submit=$row['value'];} 
                if((in_array("cancel_".$language, $row)) && ($cancel=="")){ $cancel=$row['value'];} 

            }

            return ['title_page'=>$title,
            'submit'=>$submit,
            'cancel'=>$cancel
        ];
    }

    return 0;

}




/* ------------------ INSERT ----------------------- */


//SALVO IL FORM DEL DATABASE
public function alg_fnt_insertForm($role, $company, $nation, $email, $phone, $id_survey){
    //controllo se i campi obbligatori sono stati inseriti
    if(!(
       (isset($role)) && 
       (isset($company)) &&
       (isset($nation)) &&
       (isset($id_survey)) &&
       (isset($email)))) {
        return 0;
}

        //controllo tramite email se il form è già stato compilato
$query_check="SELECT * FROM tbl_form WHERE email_form = '$email' AND survey_related_form = '$id_survey'";
$result_check=mysqli_query($this->dbConnection, $query_check);
if($result_check){
    while($row=mysqli_fetch_array($result_check)){

        return -1;

    }
}

        //dichiaro gli array per la creazione dinamica della query
$arrayAdd=Array();
$arrayAdd_Key=Array();
array_push($arrayAdd,"");
array_push($arrayAdd_Key,"");


        //mi creo in modo dinamico la query per l'update field
        //invece di creare 2 query, 1 quick e l'altra full ho creato in modo dinamico
        //l'array che si popola ad ogni controllo del campo se esiste
if(isset($phone)) {
    array_push($arrayAdd_Key,"phone_form");
    array_push($arrayAdd,"'".$phone."'");
}


        //se ho valori extra li passo come parametro della table della query
        //e come values da inserire
$strAdd="";
$strAdd_Key="";
if(count($arrayAdd)>1) {
    $strAdd=join(", ", $arrayAdd);
    $strAdd_Key=join(", ", $arrayAdd_Key);
}


$query="INSERT INTO tbl_form (role_form, company_form, nation_form, email_form, survey_related_form ".$strAdd_Key.")
VALUES('$role', '$company', '$nation', '$email', '$id_survey'".$strAdd.")"; 
$result=mysqli_query($this->dbConnection, $query);
if($result){


    $last_id = $this->dbConnection->insert_id;
    return $last_id;


}

return 0;

}





public function alg_fnt_insertResult($id_survey_result, $id_form_result, $id_category_result, $id_answer_result, $value_answer_result){
    //controllo se i campi obbligatori sono stati inseriti
    if(!((isset($id_survey_result)) && 
       (isset($id_form_result)) && 
       (isset($id_category_result)) && 
       (isset($id_answer_result)) &&
       (isset($value_answer_result)))) {
        return 0;
}


$query="INSERT INTO tbl_result (id_survey_result, id_form_result, id_category_result, id_answer_result, value_answer_result)
VALUES('$id_survey_result', '$id_form_result', '$id_category_result', '$id_answer_result', '$value_answer_result')"; 
$result=mysqli_query($this->dbConnection, $query);
if($result){


    $last_id = $this->dbConnection->insert_id;
    return $last_id;


} else {
    return -1;
}

return 0;

}


/* -------------------------- CHECK ----------------------- */

    //FUNZIONE PER VERIFICARE SE IL FORM CHE è STATO COMPILATO è GIA' STATO COMPILATO DA QUELLA EMAIL
    //RITORNO TRUE SE è GIA' STATO COMPILATO, ALTRIMENTI FALSE
public function alg_fnt_checkSubmittedForm($email, $id_survey){
    //controllo se i campi obbligatori sono stati inseriti
    if(!((isset($email)) && 
       (isset($id_survey)))) {
        return -1;
}


$query="SELECT * FROM tbl_form WHERE email_form = '$email' AND survey_related_form = '$id_survey'"; 
$result=mysqli_query($this->dbConnection, $query);
if($result){

            //RITORNO IL NUMERO DI ROW TROVATE
    return mysqli_num_rows($result);
} else {

    return 0;
}

}




public function alg_fnt_checkAddRole($name_form_role, $language_form_role){
    //controllo se i campi obbligatori sono stati inseriti
    if(!(isset($name_form_role))) {
        return -1;
}


$query="INSERT INTO tbl_form_role (name_form_role, language_form_role) VALUES ('$name_form_role', '$language_form_role')"; 
$result=mysqli_query($this->dbConnection, $query);
if($result){


    $last_id = $this->dbConnection->insert_id;
    return $last_id;


} else {

    return 0;
}

}




public function alg_fnt_checkEditRole($id_form_role, $name_form_role){
    //controllo se i campi obbligatori sono stati inseriti
    if(!(isset($name_form_role))) {
        return -1;
}


$query="UPDATE tbl_form_role
SET name_form_role = '$name_form_role'
WHERE id_form_role = '$id_form_role'"; 
$result=mysqli_query($this->dbConnection, $query);
if($result){


    return 1;


} else {

    return 0;
}

}



public function alg_fnt_updateSurveyVisibility($id_survey, $active_value){
    //controllo se i campi obbligatori sono stati inseriti
    if(!(isset($id_survey))) {
        return -1;
}


$query="UPDATE tbl_survey
SET isActive = '$active_value'
WHERE id_survey = '$id_survey'"; 
$result=mysqli_query($this->dbConnection, $query);
if($result){


    return 1;


} else {

    return 0;
}

}




public function alg_fnt_checkDeleteRole($id_form_role){
    //controllo se i campi obbligatori sono stati inseriti
    if(!(isset($id_form_role))) {
        return -1;
}


$query="DELETE FROM tbl_form_role
WHERE id_form_role = '$id_form_role'"; 
$result=mysqli_query($this->dbConnection, $query);
if($result){

    return 1;


} else {

    return 0;
}

}





/*--------------- LOGIN PART ------------*/


    //mi controllo se il login esiste già
public function alg_fnt_checkLogin($login) {
    $query="SELECT * FROM tbl_login WHERE LOWER(user_login)='".strtolower($login)."'";
    $result=mysqli_query($this->dbConnection,$query);
    if($result) {
        if($row=mysqli_fetch_array($result))
            return $row;
    }
    return 0; 
}

    //mi controllo se il login esiste già
public function alg_fnt_checkFullLogin($login, $password) {
    $query="SELECT * FROM tbl_login WHERE user_login='".strtolower($login)."' AND password_login='".$password."'";
    $result=mysqli_query($this->dbConnection,$query);
    if($result) {
        if($row=mysqli_fetch_array($result)) {
                //controllo se l'account è disabilitato 
            if($row['isActive']==0)
                return "-1";

            $idOperatore=$row['id_login'];
            if(isset($_SESSION["survey-tecnocapgroup"])) {
                $idOperatore=$_SESSION["survey-tecnocapgroup"];
            }
            $query="INSERT INTO tbl_history_login (user_history_login)
            VALUES('$idOperatore')";

            $result=mysqli_query($this->dbConnection,$query);
                //return $row;
            return 1;
        }
    }
    return 0; 
}


/* --------------------- FUNZIONI PER LE STATISTICHE -------------- */

    //RICAVO TUTTI I FORM
public function alg_fnt_getAllForm() {
    $data_form = array();
    $query="SELECT * FROM tbl_form";
    $result=mysqli_query($this->dbConnection,$query);
    if($result) {
       while($row=mysqli_fetch_array($result)) {

        $id_form = $row['id_form'];
        $id_related_survey = $row['survey_related_form'];

        $query_survey="SELECT * FROM tbl_survey WHERE id_survey = '$id_related_survey'";
        $result_survey=mysqli_query($this->dbConnection,$query_survey);
        if($result_survey){
            if($row_survey=mysqli_fetch_array($result_survey)) {
                $survey_id = $row_survey['id_survey'];
                $survey_title = $row_survey['title_survey'];
            }
        }

        $query_percentage="SELECT * FROM tbl_result WHERE id_form_result = '$id_form' AND id_survey_result = '$id_related_survey' ";
        $result_percentage=mysqli_query($this->dbConnection,$query_percentage);
        $data_result = 0;
        $data_result_answered = 0;
        $data_result_skipped = 0;
        if($result_percentage){
            while($row_percentage=mysqli_fetch_array($result_percentage)) {

                if($row_percentage['value_answer_result'] != -1){
                    $data_result_answered++;
                } else {
                    $data_result_skipped++;
                }

                $data_result++;

            }
        }

        $data_percent_answered = round((($data_result - $data_result_skipped) / $data_result) * 100);
        array_push($data_form, [
            'id_form' => $row['id_form'],
            'role_form' => htmlentities($row['role_form']),
            'company_form' => htmlentities($row['company_form']),
            'nation_form' => htmlentities($row['nation_form']),
            'phone_form' => $row['phone_form'],
            'email_form' => htmlentities($row['email_form']),
            'id_survey_form' => $survey_id,
            'title_survey_form' =>$survey_title,
            'percentage_answer_form' => $data_percent_answered,
        ]);

                

    }
    return $data_form;
}

return 0; 
}


public function alg_fnt_getFormByIdForm($id_form) {
    $query="SELECT * FROM tbl_form INNER JOIN tbl_form_role ON tbl_form.role_form = tbl_form_role.id_form_role WHERE id_form = '$id_form'";
    $result=mysqli_query($this->dbConnection,$query);
    if($result) {
       if($row=mysqli_fetch_array($result)) {

        $data_form = [
            'id_form' => $row['id_form'],
            'role_form' => htmlentities($row['name_form_role']),
            'company_form' => htmlentities($row['company_form']),
            'nation_form' => htmlentities($row['nation_form']),
            'phone_form' => $row['phone_form'],
            'email_form' => htmlentities($row['email_form']),
            'survey_related_form' => $row['survey_related_form'],
        ];

                

    }
    return $data_form;
}

return 0; 
}



public function alg_fnt_getFormRoles($language) {
    $data_roles = array();
    $query="SELECT * FROM tbl_form_role WHERE language_form_role = '$language' ";
    $result=mysqli_query($this->dbConnection,$query);
    if($result) {
    
       while($row=mysqli_fetch_array($result)) {
        $id_role_form = $row['id_form_role'];
        $query_count="SELECT COUNT(*) as role_number FROM tbl_form WHERE tbl_form.role_form = '$id_role_form'";
        $result_count=mysqli_query($this->dbConnection,$query_count);
         if($result_count) {
        $data_count=mysqli_fetch_assoc($result_count);
        array_push($data_roles, [
            'id_form_role' => $row['id_form_role'],
            'name_form_role' => htmlentities($row['name_form_role']),
            'language_form_role' => $row['language_form_role'],
            'count_role' => $data_count['role_number']
        ]);

                

        }
    }
    return $data_roles;
}

return 0; 
}



public function alg_fnt_getResultByForm($id_survey_result, $id_form_result, $id_category_result, $id_answer_result) {
    $query="SELECT * FROM tbl_result WHERE id_survey_result = '$id_survey_result' 
    AND id_form_result = '$id_form_result'
    AND id_category_result = '$id_category_result'
    AND id_answer_result = '$id_answer_result'";
    $result=mysqli_query($this->dbConnection,$query);
    if($result) {
       if($row=mysqli_fetch_array($result)) {

        $result_answer = $row['value_answer_result'];
                

    }
    return $result_answer;
}

return 0; 
}

public function alg_fnt_getFormsByIdSurvey($id_related_survey) {
    $data_form = array();
    $query="SELECT * FROM tbl_form INNER JOIN tbl_form_role ON tbl_form.role_form = tbl_form_role.id_form_role WHERE survey_related_form = '$id_related_survey'";
    $result=mysqli_query($this->dbConnection,$query);
    if($result) {
       while($row=mysqli_fetch_array($result)) {

        $id_form = $row['id_form'];

        $query_survey="SELECT * FROM tbl_survey WHERE id_survey = '$id_related_survey'";
        $result_survey=mysqli_query($this->dbConnection,$query_survey);
        if($result_survey){
            if($row_survey=mysqli_fetch_array($result_survey)) {
                $survey_id = $row_survey['id_survey'];
                $survey_title = $row_survey['title_survey'];
            }
        }

        $query_percentage="SELECT * FROM tbl_result WHERE id_form_result = '$id_form' AND id_survey_result = '$id_related_survey' ";
        $result_percentage=mysqli_query($this->dbConnection,$query_percentage);
        $data_result = 0;
        $data_result_answered = 0;
        $data_result_skipped = 0;
        if($result_percentage){
            while($row_percentage=mysqli_fetch_array($result_percentage)) {

                if($row_percentage['value_answer_result'] != -1){
                    $data_result_answered++;
                } else {
                    $data_result_skipped++;
                }

                $data_result++;

            }
        }

        $data_percent_answered = round((($data_result - $data_result_skipped) / $data_result) * 100);
        array_push($data_form, [
            'id_form' => $row['id_form'],
            'role_form' => htmlentities($row['name_form_role']),
            'company_form' => htmlentities($row['company_form']),
            'nation_form' => htmlentities($row['nation_form']),
            'phone_form' => $row['phone_form'],
            'email_form' => htmlentities($row['email_form']),
            'id_survey_form' => $survey_id,
            'title_survey_form' =>$survey_title,
            'percentage_answer_form' => $data_percent_answered,
        ]);

                

    }
    return $data_form;
}

return 0; 
}


public function alg_fnt_getAnsweredSurvey($id_survey){
    $data_total_result = array();
    $query="SELECT * FROM tbl_form INNER JOIN tbl_form_role ON tbl_form.role_form = tbl_form_role.id_form_role WHERE survey_related_form = '$id_survey'";
    $result=mysqli_query($this->dbConnection,$query);
    if($result) {
       while($row=mysqli_fetch_array($result)) {
        //ARRAY DEL SINGOLO FORM
        $data_single_result = array();

        $id_form = $row['id_form'];

        //RICAVO LE INFO DEL FORM SINGOLO
        $info_form = [
            'id_form' => $row['id_form'],
            'role_form' => htmlentities($row['name_form_role']),
            'company_form' => htmlentities($row['company_form']),
            'nation_form' => htmlentities($row['nation_form']),
            'phone_form' => $row['phone_form'],
            'email_form' => htmlentities($row['email_form']),
            'survey_related_form' => $row['survey_related_form'],
        ];

        //PUSHO LE INFO DEL FORM NEL SINGOLO RESULT
        array_push($data_single_result, array("info_form" => $info_form));


        //RICAVO LE CATEGORIE CORRELATE
        $data_categories = array();
        $query_categories="SELECT DISTINCT tbl_category.id_category, tbl_category.title_category, tbl_category.description_category FROM tbl_category, relation_survey_category WHERE relation_survey_category.id_survey_rsc = '$id_survey' AND relation_survey_category.id_category_rsc = tbl_category.id_category";
        $result_categories=mysqli_query($this->dbConnection, $query_categories);
        
        while($row_category=mysqli_fetch_array($result_categories)){
        $info_category = [
            'id_category' => $row_category['id_category'],
            'title_category' => htmlentities($row_category['title_category']),
            'description_category' => htmlentities($row_category['description_category']),
            ];


                $id_category = $row_category['id_category'];
                $query_questions="SELECT DISTINCT tbl_question.id_question, tbl_question.title_question FROM tbl_question, relation_category_question WHERE relation_category_question.id_category_rcq = '$id_category' AND relation_category_question.id_question_rcq = tbl_question.id_question";
                $result_questions=mysqli_query($this->dbConnection, $query_questions);

                if($result_questions){
                    $grouped_data_questions = array();


                    while($row_questions=mysqli_fetch_array($result_questions)){
                        $single_data_questions = array();


                $id_question = $row_questions['id_question'];

                $query_answer = "SELECT * FROM tbl_result WHERE id_survey_result = '$id_survey' AND id_form_result = '$id_form' AND id_category_result = '$id_category' AND id_answer_result = '$id_question'";

                $result_answer=mysqli_query($this->dbConnection, $query_answer);

                if($result_answer){

                if($row_answer = mysqli_fetch_array($result_answer)){



                //DATI DELLA SINGOLA CATEGORIA + LE DOMANDE ASSOCIATE
                        $single_data_questions = [
                            'id_question' => $row_questions['id_question'],
                            'title_question' => htmlentities($row_questions['title_question']),
                            'answer_question' => $row_answer['value_answer_result'],
                           

                        ];

                        array_push($grouped_data_questions, $single_data_questions);



                    }

                }

                   


                }

            }

                //PUSHO L'ARRAY DI DOMANDE DENTRO LA CATEGORIA
                 array_push($info_category, array("info_questions" => $grouped_data_questions));
        

        

        //PUSHO NELL'ARRAY DELLE CATEGORIE LA SINGOLA CATEGORIA
        array_push($data_categories, $info_category);
        
        }

        array_push($data_single_result, array("info_categories" => $data_categories));

        array_push($data_total_result, $data_single_result);
       



                
    }

    return json_encode($data_total_result);
}

return 0; 
}





    //RICAVO TUTTE LE RISPOSTE
public function alg_fnt_getAllResult() {
    $data_result = array();
    $query="SELECT * FROM tbl_result";
    $result=mysqli_query($this->dbConnection,$query);
    if($result) {
       while($row=mysqli_fetch_array($result)) {

        array_push($data_result, [
            'id_survey_result' => $row['id_survey_result'],
            'id_form_result' => $row['firstname_form'],
            'id_category_result' => $row['id_category_result'],
            'id_answer_result' => $row['id_answer_result'],
            'value_answer_result' => $row['value_answer_result']
        ]);


    }

    return $data_result;
}

return 0; 
}


public function alg_fnt_getAllNumberResult() {
    $data_numbers_result = array();
    $data_result = 0;
    $data_result_answered = 0;
    $data_result_skipped = 0;

    $one_star = 0;
    $two_star = 0; 
    $three_star = 0; 
    $four_star = 0; 
    $five_star = 0;
    $query="SELECT * FROM tbl_result";
    $result=mysqli_query($this->dbConnection,$query);
    if($result) {
       while($row=mysqli_fetch_array($result)) {

        if($row['value_answer_result'] != "-1"){

            if($row['value_answer_result'] == "1"){
                $one_star++;
            }
            else if($row['value_answer_result'] == "2"){
                $two_star++;
            }
            else if($row['value_answer_result'] == "3"){
                $three_star++;
            }
            else if($row['value_answer_result'] == "4"){
                $four_star++;
            }

            else if($row['value_answer_result'] == "5"){
                $five_star++;
            }


            $data_result_answered++;
        } else {
            $data_result_skipped++;
        }

        $data_result++;
    }


}

$data_percent_answered = round((($data_result - $data_result_skipped) / $data_result) * 100);

array_push($data_numbers_result, [
    'total_result' => $data_result,
    'total_answered' => $data_result_answered,
    'total_skipped' => $data_result_skipped,
    'total_one_star' => $one_star,
    'total_two_star' => $two_star,
    'total_three_star' => $three_star,
    'total_four_star' => $four_star,
    'total_five_star' => $five_star,
    'percent_answered_result'=>$data_percent_answered

]);

return $data_numbers_result;

return 0; 
}

/* ------------------- LOGIN ------------------ */


public function alg_fnt_getAllLogin(){
    $data_login = array();
    $query="SELECT * FROM tbl_login";
    $result=mysqli_query($this->dbConnection, $query);
    if($result){
        while($row=mysqli_fetch_array($result)){

            array_push($data_login, [

                'id_login'=>$row['id_login'],
                'user_login'=>$row['user_login'],
                'password_login'=>$row['password_login'],
                'role_login'=>$row['role_login'],
                'isActive'=>$row['isActive'],
                'nome_login'=>$row['nome_login'],
                'cognome_login'=>$row['cognome_login'],
                'tel_login'=>$row['tel_login']
            ]);

        }

    return $data_login;
    }

    return 0;
}

/* --------------------- END -------------- */

}


?>
