
<?php

    DEFINE("HOME_WEBAPP","http://$_SERVER[HTTP_HOST]/newform/");
    DEFINE("LINK_LOGIN", "http://$_SERVER[HTTP_HOST]/newform/login.php");

    DEFINE("IMAGE_PROFILE_UPLOAD",__DIR__."/assets/media/users/");
    DEFINE("IMAGE_PROJECT",__DIR__."/assets/media/project-logos/");

    DEFINE("FILE_CV",str_replace("\\","/",__DIR__."/code/cv-upload/"));
    DEFINE("FILE_COVER",str_replace("\\","/",__DIR__."/code/cover-upload/"));
    
    DEFINE("FILE_CV_COPY",str_replace("\\","/",__DIR__."/code/cv-upload/"));
    DEFINE("FILE_COVER_COPY",str_replace("\\","/",__DIR__."/code/cover-upload/"));
    DEFINE("FILE_TMP_TOKEN",str_replace("\\","/",__DIR__."/tmp/"));

    DEFINE("IMAGE_PROFILE","./assets/media/users/");
    DEFINE("IMAGE_PROJECT_VIEW","./assets/media/project-logos/");

?>