<?php

    //mi setto il time e data per l'europa
    date_default_timezone_set('Europe/Rome');

    //mi creo una variabile che mi richiamo ovunque con data e time
    $datatime_now=date("Y-m-d H:i:s");
    $dateYear=date("Y");

    //creo un time version per evitare la formazione della cache per CSS e JS
    $timeVersion=date_timestamp_get(date_create());

    //data inizio e fine mese attuale
    $month_start = strtotime('first day of this month', time());
    $month_end = strtotime('last day of this month', time());
    define('START_MONTH',date('d/m/Y', $month_start));
    define('END_MONTH', date('d/m/Y', $month_end));

    //variabili GLOBAL
    DEFINE("ARRAY_IMPLODE", ";;");

?>