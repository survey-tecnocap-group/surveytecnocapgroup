
class Car {  
  constructor(brand) {
    this.carname = brand;
  }
  present() {
    return 'I have una ' + this.carname;
  }
}
  
class PartnersMain extends Car {
  constructor(brand, mod) {
    super(brand);
      this.model = mod;
  }
  show() {
    var test=this.present() + ' model. ' + this.model;
      alert(test);
  }
}