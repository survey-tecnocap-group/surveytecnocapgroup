<?php 
header('Content-Type: text/html; charset=utf-8');
include "class/alg_function_class.php";
include "class/alg_mysql_class.php";
include "class/alg_connection_function.php";

//includo le variabili definite globali
include ("setting/define_variable.php");

//includo il SETTING per il DEFINE per i valori interessati alle pagine
include ("setting/define_page.php");


?>
<script type="text/javascript">	

	//FUNZIONE PER CLONARE LA CATEGORIA
	function addCategory() {
		var itm = document.getElementById("category-default");
		var cln = itm.cloneNode(true);
		cln.querySelector(".fw-bold-category").innerHTML = "Categoria";
		document.getElementById("draggable-zone").appendChild(cln);
	}



</script>


<style>
div.dataTables_wrapper div.dataTables_filter {
	padding: .5rem 0;
	display: none;
}
</style>
<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic - Bootstrap 5 HTML, VueJS, React, Angular & Laravel Admin Dashboard Theme
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
<!--begin::Head-->
<head><base href="">
	<title>Surveys Tecnocap Group - Statistics</title>
	<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
	<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta charset="utf-8" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
	<meta property="og:url" content="https://keenthemes.com/metronic" />
	<meta property="og:site_name" content="Keenthemes | Metronic" />
	<link rel="canonical" href="Https://preview.keenthemes.com/metronic8" />
	<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
	<!--begin::Fonts-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
	<!--end::Fonts-->
	<!--begin::Page Vendor Stylesheets(used by this page)-->
	<link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Page Vendor Stylesheets-->
	<!--begin::Global Stylesheets Bundle(used by all pages)-->
	<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed">
	<!--begin::Main-->
	<!--begin::Root-->
	<div class="d-flex flex-column flex-root">
		<!--begin::Page-->
		<div class="page d-flex flex-row flex-column-fluid">

			<!--begin::Wrapper-->
			<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
				<!--begin::Header-->
				<?php include ("pages/default_header.php"); 

				?>
				<!--end::Header-->
				<!--begin::Toolbar-->

				<!--begin::Content-->
				<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
					<!--begin::Container-->
					<div id="kt_content_container" class="container-xxl">
						<!--begin::Row-->

						<!--end::Row-->
						<!--begin::Row-->

						<!--end::Row-->
						<!--begin::Row-->
						<div id="survey-container" class="row gy-5 g-xl-8">
							<!--begin::Col-->

							<!--end::Col-->

							<!--CARD TITLE AND DESCRIPTION -->
							<div class="card card-flush pt-3 mb-5 mb-lg-10">
								<!--begin::Card header-->
								<div class="card-header">
									<!--begin::Card title-->
									<div class="card-title">
										<h2 class="fw-bolder">Crea un nuovo sondaggio</h2>
									</div>
									<!--begin::Card title-->
								</div>
								<!--end::Card header-->
								<!--begin::Card body-->
								<div class="card-body pt-0">
									<!--begin::Custom fields-->
									<div class="d-flex flex-column mb-15 fv-row">
										<!--begin::Label-->
										

										<div class="d-flex flex-column mb-10 fv-row">
											<!--begin::Label-->
											<div class="fs-5 fw-bolder form-label mb-3">Titolo sondaggio
												<i tabindex="0" class="cursor-pointer fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-html="true" data-bs-content="Inserisci il nome del sondaggio"></i></div>
												<!--end::Label-->
												<textarea class="form-control form-control-solid rounded-3" rows="1"></textarea>
											</div>

											<div class="d-flex flex-column mb-10 fv-row">
												<!--begin::Label-->
												<div class="fs-5 fw-bolder form-label mb-3">Descrizione sondaggio
													<i tabindex="0" class="cursor-pointer fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-html="true" data-bs-content="Puoi aggiungere una breve descrizione del sondaggio"></i></div>
													<!--end::Label-->
													<textarea class="form-control form-control-solid rounded-3" rows="3"></textarea>
												</div>
												<!--end::Label-->
												<!--begin::Table wrapper-->
												
												<!--end::Table wrapper-->
												<!--begin::Add custom field-->
												
												<!--end::Add custom field-->
											</div>
											<!--end::Custom fields-->
											<!--begin::Invoice footer-->
											
											<!--end::Invoice footer-->
											<!--begin::Option-->

											<!--end::Option-->
										</div>
										<!--end::Card body-->
									</div>

									
									

									<!--begin::Col-->

									<!--end::Col-->
								</div>
								<!--end::Row-->
								<!--begin::Row-->

								<!--end::Row-->
								<!--begin::Row-->

								<!--INIZIO DRAGGABLE CARD CATEGORY -->
								<div id="draggable-zone" class="row row-cols-lg-1 g-10 draggable-zone">
									<div id="category-default" class="col draggable">
										<!--begin::Card-->
										<div id="card-category" class="card card-flush pt-3 mb-5 mb-lg-10 draggable col-lg-12">
											<!--begin::Card header-->
											<div class="card-header">
												<!--begin::Card title-->
												<div class="card-title">
													<h2 class="fw-bolder fw-bold-category">Categoria</h2>

												</div>

												<div class="card-toolbar">
													<a href="#" class="btn btn-icon btn-sm btn-hover-light-primary draggable-handle">
														<!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
														<span class="svg-icon svg-icon-2x">
															<img src="assets/media/svg/icons/Navigation/Up-down.svg"/>
														</span>
														<!--end::Svg Icon-->
													</a>
												</div>
												<!--begin::Card title-->
											</div>
											<!--end::Card header-->
											<!--begin::Card body-->
											<div class="card-body pt-0">
												<!--begin::Custom fields-->
												<div class="d-flex flex-column mb-15 fv-row">
													<!--begin::Label-->


													<div class="d-flex flex-column mb-10 fv-row">
														<!--begin::Label-->
														<div class="fs-5 fw-bolder form-label mb-3">Titolo categoria
															<i tabindex="0" class="cursor-pointer fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-html="true" data-bs-content="Inserisci il nome della categoria"></i></div>
															<!--end::Label-->
															<textarea class="form-control form-control-solid rounded-3" rows="1"></textarea>
														</div>

														<div class="d-flex flex-column mb-10 fv-row">
															<!--begin::Label-->
															<div class="fs-5 fw-bolder form-label mb-3">Descrizione categoria
																<i tabindex="0" class="cursor-pointer fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="popover" data-bs-trigger="hover focus" data-bs-html="true" data-bs-content="Puoi aggiungere una breve descrizione della categoria"></i></div>
																<!--end::Label-->
																<textarea class="form-control form-control-solid rounded-3" rows="3"></textarea>
															</div>
															<!--end::Label-->
															<!--begin::Table wrapper-->
															<div class="table-responsive" style="overflow-x: hidden;">
																<!--begin::Table-->
																<table id="kt_create_new_custom_fields" class="table align-middle table-row-dashed fw-bold fs-6 gy-5">
																	<!--begin::Table head-->
																	<thead>
																		<tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
																			<th class="pt-0">Domande</th>

																			<th class="pt-0">Rimuovi</th>

																		</tr>
																	</thead>
																	<!--end::Table head-->
																	<!--begin::Table body-->
																	<tbody>
																		<tr>
																			<td class="col-md-11">
																				<input type="text" class="form-control form-control-solid" name="row-name" value="" />
																			</td>

																			<td>
																				<button type="button" class="btn btn-icon btn-flex btn-active-light-primary w-30px h-30px me-3" data-kt-action="field_remove" >
																					<!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
																					<span class="svg-icon svg-icon-3">
																						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																							<path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="black" />
																							<path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="black" />
																							<path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="black" />
																						</svg>
																					</span>
																					<!--end::Svg Icon-->
																				</button>
																			</td>
																		</tr>
																	</tbody>
																	<!--end::Table body-->
																</table>
																<!--end:Table-->
															</div>
															<!--end::Table wrapper-->
															<!--begin::Add custom field-->
															<div><button type="button" class="btn btn-light-primary me-auto" id="kt_create_new_custom_fields_add">Aggiungi domanda</button>
																<button type="button" class="btn btn-light-primary me-auto" onclick="addCategory();">Aggiungi Categoria</button></div>
																<!--end::Add custom field-->
															</div>
															<!--end::Custom fields-->
															<!--begin::Invoice footer-->

															<!--end::Invoice footer-->
															<!--begin::Option-->

															<!--end::Option-->
														</div>
														<!--end::Card body-->
													</div>

													<!--begin::Col-->

													<!--end::Col-->
												</div>
															<!--end::Row-->
															<!--begin::Row-->

															<!--end::Row-->
															<!--begin::Row-->

														</div>
														<!--end::Container-->
													</div>
													<!--end::Card-->
												</div>


											</div>
											<!-- FINE DRAGGABLE CARD CATEGORY -->
										</div>
										<!--end::Container-->
									</div>
									<!--end::Content-->
									<!--begin::Footer-->
									<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
										<!--begin::Container-->
										<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
											<!--begin::Copyright-->
											<div class="text-dark order-2 order-md-1">
												<span class="text-muted fw-bold me-1">2021©</span>
												<a href="#" target="_blank" class="text-gray-800 text-hover-primary">Surveys Tecnocap Group</a>
											</div>
											<!--end::Copyright-->
											<!--begin::Menu-->
											<ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
												<li class="menu-item">
													<a href="#" target="_blank" class="menu-link px-2"></a>
												</li>
												<li class="menu-item">
													<a href="#" target="_blank" class="menu-link px-2"></a>
												</li>
												<li class="menu-item">
													<a href="#" target="_blank" class="menu-link px-2">Concept by Algoritmica</a>
												</li>
											</ul>
											<!--end::Menu-->
										</div>
										<!--end::Container-->
									</div>
									<!--end::Footer-->
								</div>
								<!--end::Wrapper-->
							</div>
							<!--end::Page-->
						</div>
						<!-- END CONTAINER SURVEY -->

						<!--end::Root-->

						<!--begin::Scrolltop-->
						<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
							<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
							<span class="svg-icon">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
									<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
						<!--end::Scrolltop-->
						<!--end::Main-->
						<script>var hostUrl = "assets/";</script>
						<!--begin::Javascript-->
						<!--begin::Global Javascript Bundle(used by all pages)-->
						<script src="assets/plugins/global/plugins.bundle.js"></script>
						<script src="assets/js/scripts.bundle.js"></script>
						<!--end::Global Javascript Bundle-->
						<!--begin::Page Vendors Javascript(used by this page)-->
						<script src="assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
						<!--end::Page Vendors Javascript-->
						<!--end::Global Javascript Bundle-->
						<!--begin::Page Vendors Javascript(used by this page)-->
						<script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
						<!--end::Page Vendors Javascript-->
						<!--begin::Page Custom Javascript(used by this page)-->
						<script src="assets/js/custom/apps/subscriptions/add/advanced.js"></script>
						<script src="assets/plugins/custom/draggable/draggable.bundle.js" type="text/javascript"></script>
						<script src="assets/js/custom/documentation/general/draggable/cards.js"></script>

						<script src="assets/js/custom/modals/new-card.js"></script>
						<script src="assets/js/custom/widgets.js"></script>
						<!--end::Page Custom Javascript-->
						<!--end::Javascript-->
					</body>
					<!--end::Body-->
					</html>