<?php 
header('Content-Type: text/html; charset=utf-8');
include "class/alg_function_class.php";
include "class/alg_mysql_class.php";
include "class/alg_connection_function.php";

//includo le variabili definite globali
include ("setting/define_variable.php");

//includo il SETTING per il DEFINE per i valori interessati alle pagine
include ("setting/define_page.php");


?>

<script type="text/javascript">
	function openPopupLink(id_survey) {
		$("#survey-link").val("https://surveys.tecnocapgroup.com/?survey=" + id_survey);
		$("#modalSurveyLink").modal('show');
	}


	function copyPopupLink() {
		var copyText = document.getElementById("survey-link");

		copyText.select();
		copyText.setSelectionRange(0, 99999); /* For mobile devices */

		navigator.clipboard.writeText(copyText.value);
	}

	function changeVisibility(checkbox){
		if (checkbox.checked){
			checkbox.checked = false;
		} else {
			checkbox.checked = true;
		}
	}


</script>


<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic - Bootstrap 5 HTML, VueJS, React, Angular & Laravel Admin Dashboard Theme
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
<!--begin::Head-->
<head><base href="">
	<title>Surveys Tecnocap Group - Statistics</title>
	<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
	<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta charset="utf-8" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:type" content="article" />
	<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
	<meta property="og:url" content="https://keenthemes.com/metronic" />
	<meta property="og:site_name" content="Keenthemes | Metronic" />
	<link rel="canonical" href="Https://preview.keenthemes.com/metronic8" />
	<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
	<!--begin::Fonts-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
	<!--end::Fonts-->
	<!--begin::Page Vendor Stylesheets(used by this page)-->
	<link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Page Vendor Stylesheets-->
	<!--begin::Global Stylesheets Bundle(used by all pages)-->
	<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Global Stylesheets Bundle-->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed">
	<!--begin::Main-->
	<!--begin::Root-->
	<div class="d-flex flex-column flex-root">
		<!--begin::Page-->
		<div class="page d-flex flex-row flex-column-fluid">

			<!--begin::Wrapper-->
			<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
				<!--begin::Header-->
				<?php include ("pages/default_header.php"); 

				?>
				<!--end::Header-->
				<!--begin::Toolbar-->

				<!--begin::Content-->
				<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
					<!--begin::Container-->
					<div id="kt_content_container" class="container-xxl">
						<!--begin::Row-->

						<!--end::Row-->
						<!--begin::Row-->

						<!--end::Row-->
						<!--begin::Row-->
						<div class="row gy-5 g-xl-8">
							<!--begin::Col-->

							<!--end::Col-->

							<div class="col-xxl-12">
								<!--begin::Tables Widget 9-->
								<div class="card card-xxl-stretch mb-5 mb-xl-8">
									<!--begin::Header-->
									<div class="card-header border-0 pt-5">
										<h3 class="card-title align-items-start flex-column">
											<span class="card-label fw-bolder fs-3 mb-1">Surveys</span>
											<span class="text-muted mt-1 fw-bold fs-7">Lista dei surveys</span>
										</h3>
										<div class="card-toolbar" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-trigger="hover" title="Clicca per creare un nuovo sondaggio">
											<a href="new-survey.php" class="btn btn-sm btn-light btn-active-primary">
												<!--begin::Svg Icon | path: icons/duotune/arrows/arr075.svg-->
												<span class="svg-icon svg-icon-3">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black" />
														<rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon-->Crea Nuovo Survey</a>
											</div>
										</div>
										<!--end::Header-->
										<!--begin::Body-->
										<div class="card-body py-3">
											<!--begin::Table container-->
											<div class="table-responsive">
												<!--begin::Table-->
												<table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4">
													<!--begin::Table head-->
													<thead>
														<tr class="fw-bolder text-muted">
															<th class="w-25px">
																<div class="form-check form-check-sm form-check-custom form-check-solid">
																	<input class="form-check-input" type="checkbox" value="1" data-kt-check="true" data-kt-check-target=".widget-9-check" />
																</div>
															</th>
															<th class="min-w-150px">Titolo</th>
															<th class="min-w-140px">Data Creazione</th>
															<th class="min-w-140px">Intervistati</th>
															<th class="min-w-140px">Attivo</th>
															<th class="min-w-120px">Lingua</th>
															<th class="min-w-120px">Azioni</th>

														</tr>
													</thead>
													<!--end::Table head-->
													<!--begin::Table body-->
													<tbody>
														<?php 
														$rowSurvey=$alg_class_myfunction->alg_fnt_getAllSurveys();

														foreach ($rowSurvey as $surv) {
															// code...

															?>
															<tr>
																<td>
																	<div class="form-check form-check-sm form-check-custom form-check-solid">
																		<input class="form-check-input widget-9-check" type="checkbox" value="1" />
																	</div>
																</td>
																<td>
																	<div class="d-flex align-items-center">
																		<div class="symbol symbol-45px me-5">
																			<span class="symbol-label bg-light-danger text-danger fw-bolder"><?php echo $surv['id_survey']?></span>
																		</div>
																		<div class="d-flex justify-content-start flex-column">
																			<a href="/detail-survey.php?survey=<?php echo $surv['id_survey']?>" class="text-dark fw-bolder text-hover-primary fs-6"><?php echo $surv['title_survey']?></a>
																		</div>
																	</div>
																</td>
																<td>
																	<!-- data creazione -->
																	<div class="d-flex justify-content-start flex-column">

																		<?php 
																		echo $surv['data_create_survey'];



																		?>
																	</div>
																</td>
																<td>
																	<!-- data creazione -->
																	<div class="d-flex justify-content-start flex-column">

																		<?php 
																		echo $surv['interviewed_number_survey'];



																		?>
																	</div>
																</td>
																<td>
																	<div class="d-flex justify-content-start flex-column">
																		<?php 

																		if($surv['is_active_survey'] == 1){ ?>
																			<div class="form-group row">
																				
																				<div class="col-3">
																					<label class="switch">
																						<input id="<?php echo $surv['is_active_survey']?>" type="checkbox" checked="checked">
																						<span class="slider round"></span>
																					</label>
																				</div>
																			
																			<?php }
																			
																			else { ?>

																				<div class="form-group row">
																					
																					<div class="col-3">
																						<span class="switch switch-outline switch-icon switch-success">
																							<div class="col-3">
																								<label class="switch">
																									<input  id="<?php echo $surv['is_active_survey']?>" type="checkbox">
																									<span class="slider round"></span>
																								</label>
																							</div>
																						</span>
																						</div>
																					<?php }

																					?>

																				</div>
																			</td>

																			<td>

																				<img class="w-15px h-15px rounded-1 ms-2" src="assets/media/flags/italy.svg" alt="" /></span></span>
																			</td>
																			<td>
																				<div class="d-flex flex-shrink-0">
																					<a href="https://surveys.tecnocapgroup.com/?survey=<?php echo $surv['id_survey']?>" target="_blank"class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
																						<!--begin::Svg Icon | path: icons/duotune/art/art005.svg-->
																						<span class="svg-icon svg-icon-3">
																							<!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo1/dist/../src/media/svg/icons/Code/Info-circle.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																							        <polygon points="0 0 24 0 24 24 0 24"/>
																							        <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
																							        <rect fill="#000000" x="6" y="11" width="9" height="2" rx="1"/>
																							        <rect fill="#000000" x="6" y="15" width="5" height="2" rx="1"/>
																							    </g>
																							</svg><!--end::Svg Icon-->
																						</span>
																						<!--end::Svg Icon-->
																					</a>


																					<a href="javascript:openPopupLink('<?php echo $surv['id_survey']?>');" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
																						<!--begin::Svg Icon | path: icons/duotune/art/art005.svg-->
																						<span class="svg-icon svg-icon-3">
																							<!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo1/dist/../src/media/svg/icons/Code/Info-circle.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																									<rect x="0" y="0" width="24" height="24"/>
																									<path d="M10.9,2 C11.4522847,2 11.9,2.44771525 11.9,3 C11.9,3.55228475 11.4522847,4 10.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,16 C20,15.4477153 20.4477153,15 21,15 C21.5522847,15 22,15.4477153 22,16 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L10.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
																									<path d="M24.0690576,13.8973499 C24.0690576,13.1346331 24.2324969,10.1246259 21.8580869,7.73659596 C20.2600137,6.12944276 17.8683518,5.85068794 15.0081639,5.72356847 L15.0081639,1.83791555 C15.0081639,1.42370199 14.6723775,1.08791555 14.2581639,1.08791555 C14.0718537,1.08791555 13.892213,1.15726043 13.7542266,1.28244533 L7.24606818,7.18681951 C6.93929045,7.46513642 6.9162184,7.93944934 7.1945353,8.24622707 C7.20914339,8.26232899 7.22444472,8.27778811 7.24039592,8.29256062 L13.7485543,14.3198102 C14.0524605,14.6012598 14.5269852,14.5830551 14.8084348,14.2791489 C14.9368329,14.140506 15.0081639,13.9585047 15.0081639,13.7695393 L15.0081639,9.90761477 C16.8241562,9.95755456 18.1177196,10.0730665 19.2929978,10.4469645 C20.9778605,10.9829796 22.2816185,12.4994368 23.2042718,14.996336 L23.2043032,14.9963244 C23.313119,15.2908036 23.5938372,15.4863432 23.9077781,15.4863432 L24.0735976,15.4863432 C24.0735976,15.0278051 24.0690576,14.3014082 24.0690576,13.8973499 Z" fill="#000000" fill-rule="nonzero" transform="translate(15.536799, 8.287129) scale(-1, 1) translate(-15.536799, -8.287129) "/>
																								</g>

																							</svg><!--end::Svg Icon-->
																						</span>
																						<!--end::Svg Icon-->
																					</a>
																					<a href="/detail-survey.php?survey=<?php echo $surv['id_survey']?>" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
																						<!--begin::Svg Icon | path: icons/duotune/art/art005.svg-->
																						<span class="svg-icon svg-icon-3">
																							<!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo1/dist/../src/media/svg/icons/Code/Info-circle.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																								<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																									<rect x="0" y="0" width="24" height="24"/>
																									<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
																									<rect fill="#000000" x="11" y="10" width="2" height="7" rx="1"/>
																									<rect fill="#000000" x="11" y="7" width="2" height="2" rx="1"/>
																								</g>
																							</svg><!--end::Svg Icon-->
																						</span>
																						<!--end::Svg Icon-->
																					</a>

																					<a href="#" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1">
																						<!--begin::Svg Icon | path: icons/duotune/art/art005.svg-->
																						<span class="svg-icon svg-icon-3">
																							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																								<path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="black" />
																								<path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="black" />
																							</svg>
																						</span>
																						<!--end::Svg Icon-->
																					</a>
																					<a href="#" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm">
																						<!--begin::Svg Icon | path: icons/duotune/general/gen027.svg-->
																						<span class="svg-icon svg-icon-3">
																							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																								<path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="black" />
																								<path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="black" />
																								<path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="black" />
																							</svg>
																						</span>
																						<!--end::Svg Icon-->
																					</a>
																				</div>
																			</td>
																		</tr>
																		<?php 

															//END SINGLE LOOP FORM
																	}
																	?>
																</tbody>
																<!--end::Table body-->
															</table>
															<!--end::Table-->
														</div>
														<!--end::Table container-->
													</div>
													<!--begin::Body-->
												</div>
												<!--end::Tables Widget 9-->
											</div>
											<!--begin::Col-->

											<!--end::Col-->
										</div>
										<!--end::Row-->
										<!--begin::Row-->

										<!--end::Row-->
										<!--begin::Row-->

									</div>
									<!--end::Container-->
								</div>


								<div class="modal fade" id="modalSurveyLink" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeDefault" style="display: none;" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title" id="exampleModalAddRole"></h5>

												<img style="cursor: pointer;" onClick="$('#modalSurveyLink').modal('hide');" src="/assets/media/svg/icons/Navigation/Close.svg"/>

											</div>
											<div class="modal-body">
												<div class="form-group row">
													<div class="col-lg-12">
														<label>Link di condivisione:</label>
														<span class="label label-dot label-danger ml-1"></span>
														<input id="survey-link" class="form-control form-control-lg form-control-solid" type="text" value="">


													</div>

												</div>			

												<div class="modal-footer">
													<button type="button" onClick="$('#modalSurveyLink').modal('hide');" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Annulla</button>

													<button type="button" class="btn btn-primary font-weight-bold" onclick="javascript:copyPopupLink()">Copia link</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end::Content-->
								<!--begin::Footer-->

								<!--SWITCH TEST -->




								<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
									<!--begin::Container-->
									<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
										<!--begin::Copyright-->
										<div class="text-dark order-2 order-md-1">
											<span class="text-muted fw-bold me-1">2021©</span>
											<a href="#" target="_blank" class="text-gray-800 text-hover-primary">Surveys Tecnocap Group</a>
										</div>
										<!--end::Copyright-->
										<!--begin::Menu-->
										<ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
											<li class="menu-item">
												<a href="#" target="_blank" class="menu-link px-2"></a>
											</li>
											<li class="menu-item">
												<a href="#" target="_blank" class="menu-link px-2"></a>
											</li>
											<li class="menu-item">
												<a href="#" target="_blank" class="menu-link px-2">Concept by Algoritmica</a>
											</li>
										</ul>
										<!--end::Menu-->
									</div>
									<!--end::Container-->
								</div>
								<!--end::Footer-->
							</div>
							<!--end::Wrapper-->
						</div>
						<!--end::Page-->
					</div>
					<!--end::Root-->
					<!--begin::Drawers-->
					<!--begin::Activities drawer-->


					<!--end::Exolore drawer toggle-->
					<!--begin::Exolore drawer-->

					<!--end::Scrolltop-->
					<!--end::Main-->
					<script>var hostUrl = "assets/";</script>
					<!--begin::Javascript-->
					<!--begin::Global Javascript Bundle(used by all pages)-->
					<script src="assets/plugins/global/plugins.bundle.js"></script>
					<script src="assets/js/scripts.bundle.js"></script>
					<!--end::Global Javascript Bundle-->
					<!--begin::Page Vendors Javascript(used by this page)-->
					<script src="assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
					<!--end::Page Vendors Javascript-->
					<!--begin::Page Custom Javascript(used by this page)-->
					<script src="assets/js/custom/widgets.js"></script>
					<script src="assets/js/custom/apps/chat/chat.js"></script>
					<script src="assets/js/custom/modals/create-app.js"></script>
					<script src="assets/js/custom/modals/upgrade-plan.js"></script>
					<!--end::Page Custom Javascript-->
					<!--end::Javascript-->
				</body>
				<!--end::Body-->
				</html>