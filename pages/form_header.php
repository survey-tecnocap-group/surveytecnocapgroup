<div id="kt_header" class="header header-fixed" style="background-color: white !important;">
						<!--begin::Container-->
						<div class="container d-flex align-items-stretch justify-content-between">
							<!--begin::Left-->
							<div class="d-flex align-items-stretch mr-3">
								<!--begin::Header Logo-->
								
								<!--begin::Header Menu Wrapper-->
								<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
									<!--begin::Header Menu-->
									<div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">
										<!--begin::Header Nav-->
										<ul class="menu-nav">
											<li class="menu-item menu-item-submenu menu-item-rel">
												<a href="https://www.tecnocapclosures.com/it/" target="_blank" class="menu-link" style="padding: 0.75rem 0.5rem!important">
													<span class="menu-text font-weight-light" style="color:#666666!important">» Italiano</span>
													<i class="menu-arrow"></i>
												</a>												
											</li>
											<li class="menu-item menu-item-submenu menu-item-rel">
												<a href="https://www.tecnocapclosures.cz/" target="_blank" class="menu-link" style="padding: 0.75rem 0.5rem!important">
													<span class="menu-text font-weight-light" style="color:#666666!important">» Čeština</span>
													<i class="menu-arrow"></i>
												</a>												
											</li>
											<li class="menu-item menu-item-submenu menu-item-rel">
												<a href="https://www.tecnocapclosures.com/es/" target="_blank" class="menu-link" style="padding: 0.75rem 0.5rem!important">
													<span class="menu-text font-weight-light" style="color:#666666!important">» Español</span>
													<i class="menu-arrow"></i>
												</a>												
											</li>
											<li class="menu-item menu-item-submenu menu-item-rel">
												<a href="https://www.tecnocapclosures.com/" target="_blank" class="menu-link" style="padding: 0.75rem 0.5rem!important">
													<span class="menu-text font-weight-light" style="color:#666666!important">» English</span>
													<i class="menu-arrow"></i>
												</a>												
											</li>
											<li class="menu-item menu-item-submenu menu-item-rel">
												<a href="http://www.tecnocapclosures.ru/" target="_blank" class="menu-link" style="padding: 0.75rem 0.5rem!important">
													<span class="menu-text font-weight-light" style="color:#666666!important">» Pусский</span>
													<i class="menu-arrow"></i>
												</a>												
											</li>											
										</ul>
										<!--end::Header Nav-->
									</div>
									<!--end::Header Menu-->
								</div>
								<!--end::Header Menu Wrapper-->
							</div>
							<!--end::Left-->
							<!--begin::Topbar-->
							<div class="topbar">
								<!--begin::Search-->
								
								<!--end::Search-->
								
								<!--begin::Languages-->
								<div class="dropdown">
									<!--begin::Toggle-->
									<div class="topbar-item" data-offset="10px,0px">
										<!--<div class="h6 mr-3 mt-1">Home</div>
										<a href="https://www.tecnocapclosures.com/" target="_self"><i class="fas fa-home mr-10"></i></a>-->

										<div class="h6 mr-3 mt-1">Follow us</div>
										<a href="https://www.linkedin.com/company/tecnocap/" target="_blank"><i class="socicon-linkedin mr-5"></i></a>
										<a href="https://twitter.com/tecnocapgroup" target="_blank"><i class="socicon-twitter mr-5"></i></a>
										<a href="https://www.youtube.com/channel/UCqNIFaI6bp_r6Mt5P8m4pUA/" target="_blank"><i class="socicon-youtube mr-5"></i></a>
									</div>
									<!--end::Toggle-->
								</div>
								<!--end::Languages-->
								
							</div>
							<!--end::Topbar-->
						</div>
						<!--end::Container-->
					</div>