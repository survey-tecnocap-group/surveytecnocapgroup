<div class="footer bg-white py-4 d-flex flex-lg-column mt-10" style="background-color:#888888!important" id="kt_footer">
	<!--begin::Container-->
	<div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
		<!--begin::Copyright-->
		<div class="text-dark order-2 order-md-1">
			<span class="text-muted font-weight-bold mr-2">
				<strong style="color:#bb141a">EUROPA, AFRICA, PACIFICO</strong><span class="text-white font-weight-normal"> Phone: +39 089 441522 | Email: <a class="text-white" href="https://www.tecnocapclosures.com/en/?p=7956&ceid=15153">request info</a></span><br>
				<strong style="color:#bb141a">AMERICHE</strong><span class="text-white font-weight-normal"> | Phone: +1 304 845 3402 | Toll Free Call: 800 999 2567 | Email: <a class="text-white" href="https://www.tecnocapclosures.com/en/?p=7956&ceid=15154">request info</a> | <a class="text-white" target="_blank" href="https://www.tecnocapclosures.com/privacy/">Privacy</a> | <a class="text-white" target="_blank" href="https://www.tecnocapclosures.com/terms-of-use/">General Conditions</a></span>
			</span>
		</div>
		<!--end::Copyright-->
	</div>
	<!--end::Container-->
</div>
<div class="footer bg-white py-4 d-flex flex-lg-column pb-10" id="kt_footer_1">
	<!--begin::Container-->
	<div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
		<!--begin::Copyright-->
		<div class="text-dark order-2 order-md-1">
			<span class="text-muted font-weight-bold mr-2">© 2021 | Tecnocap S.p.A. - via Starza 4 bis - 84013 Cava de' Tirreni - Salerno, Italy | p.iva/vat/c.f. IT 02865960658 - Cap. Soc. EUR 6.310.490,32 i.v. - Rea Salerno n° 246560</span>
		</div>
		<!--end::Copyright-->
	</div>
	<!--end::Container-->
</div>