<div id="kt_header_mobile" class="header-mobile" style="display:table; padding:0px">
			<div class="row mt-3 header-mobile" style="padding:0px; height:35px; min-height:35px">
			<!--begin::Toolbar-->
			<div class="d-flex align-items-center ">
				<div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">
					<!--begin::Header Nav-->
					<span class="menu-item menu-item-submenu menu-item-rel">
						<a href="https://www.tecnocapclosures.com/it/" target="_blank" class="menu-link" style="padding: 0.75rem 0.5rem!important">
							<span class="menu-text font-weight-light" style="color:#666666!important">» Italiano</span>
							<i class="menu-arrow"></i>
						</a>												
					</span>																
					<!--end::Header Nav-->
					<!--begin::Header Nav-->
					<span class="menu-item menu-item-submenu menu-item-rel">
						<a href="index.php?language=czh" class="menu-link" style="padding: 0.75rem 0.5rem!important">
							<span class="menu-text font-weight-light" style="color:#666666!important">» Čeština</span>
							<i class="menu-arrow"></i>
						</a>												
					</span>																
					<!--end::Header Nav-->
					<!--begin::Header Nav-->
					<span class="menu-item menu-item-submenu menu-item-rel">
						<a href="https://www.tecnocapclosures.com/es/" target="_blank" class="menu-link" style="padding: 0.75rem 0.5rem!important">
							<span class="menu-text font-weight-light" style="color:#666666!important">» Español</span>
							<i class="menu-arrow"></i>
						</a>												
					</span>																
					<!--end::Header Nav-->
					<!--begin::Header Nav-->
					<span class="menu-item menu-item-submenu menu-item-rel">
						<a href="index.php?language=eng" class="menu-link" style="padding: 0.75rem 0.5rem!important">
							<span class="menu-text font-weight-light" style="color:#666666!important">» English</span>
							<i class="menu-arrow"></i>
						</a>												
					</span>																
					<!--end::Header Nav-->
					<!--begin::Header Nav-->
					<span class="menu-item menu-item-submenu menu-item-rel">
						<a href="index.php?language=czh" class="menu-link" style="padding: 0.75rem 0.5rem!important">
							<span class="menu-text font-weight-light" style="color:#666666!important">» Pусский</span>
							<i class="menu-arrow"></i>
						</a>												
					</span>																
					<!--end::Header Nav-->
				</div>				
			</div>			
			<!--end::Toolbar-->
			</div>
			<div class="row header-mobile" style="height:35px; min-height:35px; padding:0px" align="center">
				<div class="mt-3" style="width:100%">
					<div class="h6 mr-3 mt-1">
						<span class="h6 mr-3 mt-1">Home</span>
						<a href="https://www.tecnocapclosures.com/" target="_self"><i class="fas fa-home mr-10"></i></a>
						<span style="width:100%">&nbsp;</span>
						<span class="mr-5">Follow us</span>
						<a href="https://www.linkedin.com/company/tecnocap/" target="_blank"><i class="socicon-linkedin mr-5"></i></a>
						<a href="https://twitter.com/tecnocapgroup" target="_blank"><i class="socicon-twitter mr-5"></i></a>
						<a href="https://www.youtube.com/channel/UCqNIFaI6bp_r6Mt5P8m4pUA/" target="_blank"><i class="socicon-youtube mr-5"></i></a>
					</div>
				</div>
			</div>
		</div>