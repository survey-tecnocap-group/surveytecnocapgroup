<?php
header('Content-Type: text/html; charset=utf-8');
include "class/alg_function_class.php";
include "class/alg_mysql_class.php";
include "class/alg_connection_function.php";

//includo le variabili definite globali
include ("setting/define_variable.php");

//includo il SETTING per il DEFINE per i valori interessati alle pagine
include ("setting/define_page.php");

define ("STAR", "M11.1359 4.48359C11.5216 3.82132 12.4784 3.82132 12.8641 4.48359L15.011 8.16962C15.1523 8.41222 15.3891 8.58425 15.6635 8.64367L19.8326 9.54646C20.5816 9.70867 20.8773 10.6186 20.3666 11.1901L17.5244 14.371C17.3374 14.5803 17.2469 14.8587 17.2752 15.138L17.7049 19.382C17.7821 20.1445 17.0081 20.7069 16.3067 20.3978L12.4032 18.6777C12.1463 18.5645 11.8537 18.5645 11.5968 18.6777L7.69326 20.3978C6.99192 20.7069 6.21789 20.1445 6.2951 19.382L6.7248 15.138C6.75308 14.8587 6.66264 14.5803 6.47558 14.371L3.63339 11.1901C3.12273 10.6186 3.41838 9.70867 4.16744 9.54646L8.3365 8.64367C8.61089 8.58425 8.84767 8.41222 8.98897 8.16962L11.1359 4.48359Z");

if (empty($_GET['survey']))
{
  header('Location: error-page.php');
  exit; 
} 

?>


<style type="text/css">
	@media (min-width: 576px){
.col-sm-1 {
    flex: 0 0 auto;
    width: 11% !important;
	}
}
</style>

<script type="text/javascript">
	
		
	validate=function(){
		form_role_value =document.getElementById("form_role").value;
		form_company_value =document.getElementById("form_company").value;
		form_email_value =document.getElementById("form_email").value;
		form_id_survey_value =document.getElementById("form_id_survey").value;
		check_email = "";
		var check = checkNumberWithSelected();


 
		if(form_role_value == "" ||
			form_company_value == "" ||
			form_email_value == "" ||
			form_id_survey_value == ""){
			Swal.fire({
				title: "Errore nella compilazione del form!",
				text: "Uno o più campi obbligatori sono vuoti, inserire un valore valido.",
				icon: "error",
				buttonsStyling: false,
				confirmButtonText: "Chiudi",
				customClass: {
					confirmButton: "btn btn-danger"
				}
			});
			return false;

		}
else
{
	$.ajax({
		url: "control/check_survey_mail_relation.php",
		data: {"email" : form_email_value,
		"id_survey" : form_id_survey_value},
		type: 'POST',
		async: false,
		success: function(result){
			
			if(result == 1){
				
				check_email = "exist";
			}
			else{				
				return true;				
			}

		}







	});

	if (check_email == "exist"){

		Swal.fire({
			title: "Email già in uso!",
			text: "Questo questionario è già stato compilato con questa email.",
			icon: "warning",
			buttonsStyling: false,
			confirmButtonText: "Chiudi",
			customClass: {
				confirmButton: "btn btn-danger"
			}
		});
		
		return false;
	} else if(check == false){
			
			Swal.fire({
				title: "Questionario non completo",
				text: "Si prega di rispondere alle domande oppure di saltarle.",
				icon: "error",
				buttonsStyling: false,
				confirmButtonText: "Chiudi",
				customClass: {
					confirmButton: "btn btn-danger"
				}
			});

			return false;
		}
	

}



}


</script>


<script>

	
	//ARRAY IN JS CHE POI CONVERTO IN PHP
	var list_questions_answers = new Array();

	//FUNZIONE PER CONTROLLARE SE UN JSON CONTIENE UN VALORE


	//SCORRO TUTTO L'ARRAY DELLE DOMANDE PER VEDERE SE ESISTE
	//IL NUMERO DELLA DOMANDA CHE HO GIA' RISPOSTO
	function _isContains(json, number) {
		let contains = false;
		for (var i = 0; i < json.length; i++) {
			if (json[i].answer_number === number) {
				contains = true;
				return contains;
			}
		}
		return contains;
	}

	//SCORRO TUTTO L'ARRAY DELLE DOMANDE PER VEDERE SE ESISTE
	//IL NUMERO DELLA DOMANDA CHE HO GIA' RISPOSTO
	function _removeQuestion(json, number) {
		
		for (var i = 0; i < json.length; i++) {
			if (json[i].answer_number === number) {
				
				
				  json.splice(i, 1);
				
				
				return true;
			}
		}
		return false;
	}



 	//FUNZIONE PER SOSTITUIRE IL VALORE NEL JSON
 	function setAnswerValue(value_question, value_answer) {
 		for (var i = 0; i < list_questions_answers.length; i++) {
 			if (list_questions_answers[i].answer_number === value_question) {
 				list_questions_answers[i].answered_value = value_answer;
 				return;
 			}
 		}
 	}





 	function retrieveAnswer(obj, id_checkbox, label_name){
 		var data = obj.getAttribute("data-jsondata");
 		data = JSON.parse(data);

   //_isContains MI DICE SE LA DOMANDA CLICCATA è GIA STATA INSERITA NELL'ARRAY DI DOMANDA
   var valore = _isContains(list_questions_answers,  data.answer_number);
   if(valore){

   	//QUI AGGIORNO LA DOMANDA SE ERA GIA' PRESENTE
   	setAnswerValue(data.answer_number, data.answered_value);

   	if(document.getElementById(id_checkbox).checked == false){

   		//SE IL VALORE RICAVATO è DIVERSO DA -1 DISABILITO IL CHECK
   		 selected_questions--;
 			_removeQuestion(list_questions_answers, data.answer_number);
   		
   	}

   	else {
   		
   		document.getElementById(id_checkbox).checked = true;
   		document.getElementById(label_name).click();
   	}
   	
   } else{

   	
   	

   	//DOPO LA RIMOZIONE O SE NON ERA SKIPPATA RIAGGIORNO
   	selected_questions++;
   	list_questions_answers.push(data);

}

	//AGGIORNO IL VALORE INPUT DA PASSARE
	document.getElementById('survey_list').value = JSON.stringify(list_questions_answers);
	//document.getElementById('survey_skipped').value = JSON.stringify(list_skipped_questions_answers);
	
}
	//NUMERO DI DOMANDE DEL QUESTIONARIO
	var number_questions = 0;

	//NUMERO DI DOMANDE DEL QUESTIONARIO RISPOSTE O SALTATE
	var selected_questions = 0;
	function incrementNumberQuestion(increment){	
	 number_questions = number_questions + increment;
	}

	function checkNumberWithSelected(){	
	 if(number_questions != selected_questions){
	 	return false;
	 } else {
	 	return true;
	 }
	}

		
</script>

<!DOCTYPE html>

<html lang="en">
<!--begin::Head-->
<head><base href="">
	<meta charset="utf-8" />
	<title><?php echo PAGE_TITLE; ?></title>
	<meta name="description" content="<?php echo PAGE_DESCRIPTION; ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<link rel="canonical" href="<?php echo LINK_CANONICAL; ?>" />
	<!--begin::Fonts-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
	<!--end::Fonts-->
	<!--begin::Page Vendors Styles(used by this page)-->
	<link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

	<!--end::Page Vendors Styles-->

	<link href="assets/css/panel.css" rel="stylesheet" type="text/css" />

	<!--begin::Global Theme Styles(used by all pages)-->
	<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Global Theme Styles-->
	<!--begin::Layout Themes(used by all pages)-->
	<!--end::Layout Themes-->
	<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />

	<!-- IMPORT RATING START -->
	<link rel="canonical" href="Https://preview.keenthemes.com/metronic8" />
	<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
	<!--begin::Fonts-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700" />
	<!--end::Fonts-->
	<!--begin::Page Vendor Stylesheets(used by this page)-->
	<link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Page Vendor Stylesheets-->
	<!--begin::Global Stylesheets Bundle(used by all pages)-->
	<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<!-- IMPORT RATING END -->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="header-mobile-fixed subheader-enabled aside-enabled aside-fixed aside-secondary-enabled page-loading" style="background-color: #F5F5F5 !important;">
	<!--begin::Main-->
	<!--begin::Header Mobile-->
	<?php include ("pages/templates/header_mobile.php"); 

	?>
	<!--end::Header Mobile-->
	<div class="d-flex flex-column flex-root">
		<!--begin::Page-->
		<div class="d-flex flex-row flex-column-fluid page">
			<!--begin::Aside-->
				<!--<div class="aside aside-left d-flex aside-fixed" id="kt_aside">
				 php include ("pages/templates/menu.php");
				</div> -->
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="flex-row-fluid" id="kt_wrapper">

					<!--begin::Header-->
					<?php include ("pages/form_header.php"); 

					?>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
						<?php include ("pages/form_subheader.php"); 

						?>
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container" id="mainBody">
								<?php 

								$question_number = 1;
								$id_survey =  $_GET['survey'];


								$rowSurvey=$alg_class_myfunction->alg_fnt_getFullSurvey($id_survey);

								//JSON DEL SURVEY CHE DEVO RICAVARE
								$survey = json_decode($rowSurvey, true);
								//var_dump($survey);

								//ACCESSO AI DATI DELLA CATEGORIA SINGOLA
								//echo $survey[1]['all_categories'][0]['single_category']['title_category'];

								//ACCESSO AI DATI DELLA SINGOLA CATEGORIA
								//echo $survey[1]['all_categories'][0]['single_category']['questions_category'][0]['title_question'];

								//echo $rowSurvey;

								?>

								<!--begin::Survey Title and Description -->

								<main class="cd-main-content">
									<div class="survey-title-tecnocap"><?php echo($survey[0]['title_survey']);
								?></div>
								<div class="survey-subtitle-tecnocap"><?php echo($survey[0]['description_survey']);
							?></div>
							<br>
							<!--end::Survey Title and Description -->

							<?php

							//FOR PER LE CATEGORIE
							for ($i = 0; $i < count($survey[1]['all_categories']); $i++){
								
								
								?>


								<!--begin::Survey -->
								<form class="form" method="post" id="contact-form" action="thanks-page.php" onsubmit="return validate();">
									<div class="card card-custom">
										<div class="card-header">
											<h3 class="col-lg-12 text-align-sinistra padding-top-20-px"><?php echo $survey[1]['all_categories'][$i]['single_category']['title_category'];?></h3>
											<h4 class="col-lg-12 text-align-sinistra"><?php echo $survey[1]['all_categories'][$i]['single_category']['description_category'];?></h4>

											<div class="card-toolbar">
											</div>
										</div>


										<div class="card-body">
											<div class="row">

												<div class="col-sm-4">
													<p class="valutation-label text-align-sinistra">Domande</p>
												</div>

												<div class="col-sm-1">
													<p style="display: inline;" class="valutation-label">Completamente<br>insoddisfatto</p>
												</div>
												<div class="col-sm-1">
													<p style="display: inline;" class="valutation-label">Insoddisfatto</p>
												</div>
												<div class="col-sm-1">
													<p style="display: inline;" class="valutation-label">Nella media</p>
												</div>
												<div class="col-sm-1">
													<p style="display: inline;" class="valutation-label">Soddisfatto</p>
												</div>
												<div class="col-sm-1">
													<p style="display: inline;" class="valutation-label">Estremamente<br>soddisfatto</p>
												</div>
												<div class="col-sm-1">
													<p style="display: inline;" class="valutation-label">Salta domanda</p>
												</div>
											</div>



											<?php 

											foreach($survey[1]['all_categories'][$i]['single_category']['questions_category'] as $cat){

												?>




												<div class="row">
													<div class="rating">
														<div class="col-sm-4">
															<!-- TITOLO DOMANDA -->
															<script type="text/javascript">
																incrementNumberQuestion(1);
															</script>
															<p style="text-align: left;
															font-weight: bold;"><?php echo $cat['title_question'];?></p>
														</div>


														<!--begin::Reset rating-->

														<input class="rating-input" name="<?php echo $cat['id_question']; ?>_rating" value="0" checked type="radio" id="<?php echo $cat['id_question']; ?>_kt_rating_input_0"/>
														<!--end::Reset rating-->

														<!--begin::Star 1-->
														<label class="rating-label col-sm-1" for="<?php echo $cat['id_question']; ?>_kt_rating_input_1">
															<span class="svg-icon svg-icon-1"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<path d="<?php echo STAR ?>" fill="black"/>
																</svg></span>
															</label>
															<input class="rating-input" name="<?php echo $cat['id_question']; ?>_rating" value="1" type="radio" id="<?php echo $cat['id_question']; ?>_kt_rating_input_1" data-jsondata='{"answer_number":"<?php echo $question_number;?>","answered_category_id":"<?php echo $survey[1]['all_categories'][$i]['single_category']['id_category'];?>","answered_category_title":"<?php echo htmlspecialchars($survey[1]['all_categories'][$i]['single_category']['title_category'], ENT_QUOTES);?>","answered_category_subtitle":"<?php echo htmlspecialchars($survey[1]['all_categories'][$i]['single_category']['description_category'], ENT_QUOTES);?>", "answered_id":"<?php echo $cat['id_question']; ?>", "answered_question":"<?php echo $cat['title_question'];?>","answered_value":"1"}' onclick="retrieveAnswer(this, '<?php echo $cat['id_question']; ?>_id_check_skip', '<?php echo $cat['id_question']; ?>_kt_rating_input_reset')"/>
															<!--end::Star 1-->

															<!--begin::Star 2-->
															<label class="rating-label col-sm-1" for="<?php echo $cat['id_question']; ?>_kt_rating_input_2">
																<span class="svg-icon svg-icon-1"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																	<path d="<?php echo STAR ?>" fill="black" />
																	</svg></span>
																</label>
																<input class="rating-input" name="<?php echo $cat['id_question']; ?>_rating" value="2" type="radio" id="<?php echo $cat['id_question']; ?>_kt_rating_input_2" data-jsondata='{"answer_number":"<?php echo $question_number;?>","answered_category_id":"<?php echo $survey[1]['all_categories'][$i]['single_category']['id_category'];?>","answered_category_title":"<?php echo htmlspecialchars($survey[1]['all_categories'][$i]['single_category']['title_category'], ENT_QUOTES);?>","answered_category_subtitle":"<?php echo htmlspecialchars($survey[1]['all_categories'][$i]['single_category']['description_category'], ENT_QUOTES);?>", "answered_id":"<?php echo $cat['id_question']; ?>", "answered_question":"<?php echo $cat['title_question'];?>","answered_value":"2"}' onclick="retrieveAnswer(this, '<?php echo $cat['id_question']; ?>_id_check_skip', '<?php echo $cat['id_question']; ?>_kt_rating_input_reset')"/>
																<!--end::Star 2-->

																<!--begin::Star 3-->
																<label class="rating-label col-sm-1" for="<?php echo $cat['id_question']; ?>_kt_rating_input_3">
																	<span class="svg-icon svg-icon-1"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																		<path d="<?php echo STAR ?>" fill="black" />
																		</svg></span>
																	</label>
																	<input class="rating-input" name="<?php echo $cat['id_question']; ?>_rating" value="3" type="radio" id="<?php echo $cat['id_question']; ?>_kt_rating_input_3" data-jsondata='{"answer_number":"<?php echo $question_number;?>","answered_category_id":"<?php echo $survey[1]['all_categories'][$i]['single_category']['id_category'];?>","answered_category_title":"<?php echo htmlspecialchars($survey[1]['all_categories'][$i]['single_category']['title_category'], ENT_QUOTES);?>","answered_category_subtitle":"<?php echo htmlspecialchars($survey[1]['all_categories'][$i]['single_category']['description_category'], ENT_QUOTES);?>", "answered_id":"<?php echo $cat['id_question']; ?>", "answered_question":"<?php echo $cat['title_question'];?>","answered_value":"3"}' onclick="retrieveAnswer(this, '<?php echo $cat['id_question']; ?>_id_check_skip', '<?php echo $cat['id_question']; ?>_kt_rating_input_reset')"/>
																	<!--end::Star 3-->

																	<!--begin::Star 4-->
																	<label class="rating-label col-sm-1" for="<?php echo $cat['id_question']; ?>_kt_rating_input_4">
																		<span class="svg-icon svg-icon-1"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																			<path d="<?php echo STAR ?>" fill="black" />
																			</svg></span>
																		</label>
																		<input class="rating-input" name="<?php echo $cat['id_question']; ?>_rating" value="4" type="radio" id="<?php echo $cat['id_question']; ?>_kt_rating_input_4"data-jsondata='{"answer_number":"<?php echo $question_number;?>","answered_category_id":"<?php echo $survey[1]['all_categories'][$i]['single_category']['id_category'];?>","answered_category_title":"<?php echo htmlspecialchars($survey[1]['all_categories'][$i]['single_category']['title_category'], ENT_QUOTES);?>","answered_category_subtitle":"<?php echo htmlspecialchars($survey[1]['all_categories'][$i]['single_category']['description_category'], ENT_QUOTES);?>", "answered_id":"<?php echo $cat['id_question']; ?>", "answered_question":"<?php echo $cat['title_question'];?>","answered_value":"4"}' onclick="retrieveAnswer(this, '<?php echo $cat['id_question']; ?>_id_check_skip', '<?php echo $cat['id_question']; ?>_kt_rating_input_reset')"/>
																		<!--end::Star 4-->

																		<!--begin::Star 5-->
																		<label class="rating-label col-sm-1" for="<?php echo $cat['id_question']; ?>_kt_rating_input_5">
																			<span class="svg-icon svg-icon-1"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																				<path d="<?php echo STAR ?>" fill="black" />
																				</svg></span>
																			</label>
																			<input class="rating-input" name="<?php echo $cat['id_question']; ?>_rating" value="5" type="radio" id="<?php echo $cat['id_question']; ?>_kt_rating_input_5" data-jsondata='{"answer_number":"<?php echo $question_number;?>","answered_category_id":"<?php echo $survey[1]['all_categories'][$i]['single_category']['id_category'];?>","answered_category_title":"<?php echo htmlspecialchars($survey[1]['all_categories'][$i]['single_category']['title_category'], ENT_QUOTES);?>","answered_category_subtitle":"<?php echo htmlspecialchars($survey[1]['all_categories'][$i]['single_category']['description_category'], ENT_QUOTES);?>", "answered_id":"<?php echo $cat['id_question']; ?>", "answered_question":"<?php echo $cat['title_question'];?>","answered_value":"5"}' onclick="retrieveAnswer(this, '<?php echo $cat['id_question']; ?>_id_check_skip', '<?php echo $cat['id_question']; ?>_kt_rating_input_reset' )"/>
																			<!--end::Star 5-->




																			<!--start::skipButton-->
																			




																			
																				<label id="<?php echo $cat['id_question']; ?>_kt_rating_input_reset" class="col-sm-1 " for="<?php echo $cat['id_question']; ?>_kt_rating_input_0">
																					<input type="checkbox" name="<?php echo $cat['id_question']; ?>_check_skip" id="<?php echo $cat['id_question']; ?>_id_check_skip" data-jsondata='{"answer_number":"<?php echo $question_number;?>","answered_category_id":"<?php echo $survey[1]['all_categories'][$i]['single_category']['id_category'];?>","answered_category_title":"<?php echo htmlspecialchars($survey[1]['all_categories'][$i]['single_category']['title_category'], ENT_QUOTES);?>","answered_category_subtitle":"<?php echo htmlspecialchars($survey[1]['all_categories'][$i]['single_category']['description_category'], ENT_QUOTES);?>", "answered_id":"<?php echo $cat['id_question']; ?>", "answered_question":"<?php echo $cat['title_question'];?>","answered_value":"-1"}' onclick="retrieveAnswer(this, '<?php echo $cat['id_question']; ?>_id_check_skip', '<?php echo $cat['id_question']; ?>_kt_rating_input_reset')"/>

																			 </label>



																			
																			<!--end::skipButton-->
																		</div>
																	</div>

																	<?php
									//END LOOP DOMANDE
																	$question_number++;
																}

																?>




															</div>
															<div class="card-footer d-flex justify-content-between">

															</div>
														</div>
														<br>

														<?php 
							//END LOOP CATEGORIE
													}

													?>
													<!--end::Survey -->

													<!--start::SubmitButton-->
													<div class="flex-column-fluid">
	<!--begin::Container-->
	<div class="">
		<div class="row">
			<div class="col-lg-12">
				<!--begin::Card-->
				<div class="card card-custom gutter-b example example-compact">
					<div class="card-header">
						<h3 class="col-lg-12 text-align-sinistra padding-top-20-px">La tua opinione è importante per il miglioramento dei nostri prodotti e servizi.</h3>
						<h4 class="col-lg-12 text-align-sinistra">Ti chiediamo di compilare il seguente modulo di soddisfazione.</h4>
						<h6 class="col-lg-12 text-align-sinistra">I campi contrassegnati con il simbolo * (asterisco) sono obbligatori.</h6>
						<div class="card-toolbar">
						</div>
					</div>
					<!--begin::Form-->
					
						<div class="card-body">
							<div class="form-group row">
								<label class="col-lg-1 col-form-label text-lg-right">Ruolo* :</label>
								<div class="col-lg-4">
									<div class="input-group spinner spinner-success spinner-right" >
										<select id="form_role" name="form_role" class="form-control">
											<?php 
														$language = "it";
														$roleForm=$alg_class_myfunction->alg_fnt_getFormRoles($language);
														

														foreach ($roleForm as $role) {

															$idRole = $role['id_form_role'];
															$nameRole = $role['name_form_role'];
															

															?>
											<option value="<?php echo $idRole ?>"><?php echo $nameRole ?></option>
											
										<?php }?>
										</select>
									</div>
									<!--<input type="text" name="form_role" id="form_role" class="form-control" placeholder="" />
									<span class="form-text text-muted"></span>-->
								</div>
								<div class="col-lg-1"></div>
								<label class="col-lg-1 col-form-label text-lg-right">Azienda* :</label>
								<div class="col-lg-4">
									<input type="text" name="form_company" id="form_company" class="form-control" placeholder="" />
									<span class="form-text text-muted"></span>
								</div>
								<div class="col-lg-1"></div>
							</div>
							<br>

							<div class="form-group row">
								<label class="col-lg-1 col-form-label text-lg-right">Email* :</label>
								<div class="col-lg-4">
									<div class="input-group spinner spinner-success spinner-right" >
										<input class="form-control" type="email" name="form_email" id="form_email" placeholder="" />
										
									</div>
									<span class="form-text text-muted"></span>
								</div>
								<div class="col-lg-1"></div>
								<label class="col-lg-1 col-form-label text-lg-right">Telefono :</label>
								<div class="col-lg-4">
									<div class="input-group">
										<input type="tel" class="form-control" name="form_phone" id="form_phone" placeholder="" />
										
									</div>
									<span class="form-text text-muted"></span>
								</div>
								<div class="col-lg-1"></div>
							</div>


							<br>

							<div class="form-group row">
								<label class="col-lg-1 col-form-label text-lg-right">Nazione* :</label>
								<div class="col-lg-4">
									<div class="input-group spinner spinner-success spinner-right" >
										<select id="form_country" name="form_country" class="form-control">
											<option value="Afghanistan">Afghanistan</option>
											<option value="Åland Islands">Åland Islands</option>
											<option value="Albania">Albania</option>
											<option value="Algeria">Algeria</option>
											<option value="American Samoa">American Samoa</option>
											<option value="Andorra">Andorra</option>
											<option value="Angola">Angola</option>
											<option value="Anguilla">Anguilla</option>
											<option value="Antarctica">Antarctica</option>
											<option value="Antigua and Barbuda">Antigua and Barbuda</option>
											<option value="Argentina">Argentina</option>
											<option value="Armenia">Armenia</option>
											<option value="Aruba">Aruba</option>
											<option value="Australia">Australia</option>
											<option value="Austria">Austria</option>
											<option value="Azerbaijan">Azerbaijan</option>
											<option value="Bahamas">Bahamas</option>
											<option value="Bahrain">Bahrain</option>
											<option value="Bangladesh">Bangladesh</option>
											<option value="Barbados">Barbados</option>
											<option value="Belarus">Belarus</option>
											<option value="Belgium">Belgium</option>
											<option value="Belize">Belize</option>
											<option value="Benin">Benin</option>
											<option value="Bermuda">Bermuda</option>
											<option value="Bhutan">Bhutan</option>
											<option value="Bolivia">Bolivia</option>
											<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
											<option value="Botswana">Botswana</option>
											<option value="Bouvet Island">Bouvet Island</option>
											<option value="Brazil">Brazil</option>
											<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
											<option value="Brunei Darussalam">Brunei Darussalam</option>
											<option value="Bulgaria">Bulgaria</option>
											<option value="Burkina Faso">Burkina Faso</option>
											<option value="Burundi">Burundi</option>
											<option value="Cambodia">Cambodia</option>
											<option value="Cameroon">Cameroon</option>
											<option value="Canada">Canada</option>
											<option value="Cape Verde">Cape Verde</option>
											<option value="Cayman Islands">Cayman Islands</option>
											<option value="Central African Republic">Central African Republic</option>
											<option value="Chad">Chad</option>
											<option value="Chile">Chile</option>
											<option value="China">China</option>
											<option value="Christmas Island">Christmas Island</option>
											<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
											<option value="Colombia">Colombia</option>
											<option value="Comoros">Comoros</option>
											<option value="Congo">Congo</option>
											<option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
											<option value="Cook Islands">Cook Islands</option>
											<option value="Costa Rica">Costa Rica</option>
											<option value="Cote D'ivoire">Cote D'ivoire</option>
											<option value="Croatia">Croatia</option>
											<option value="Cuba">Cuba</option>
											<option value="Cyprus">Cyprus</option>
											<option value="Czech Republic">Czech Republic</option>
											<option value="Denmark">Denmark</option>
											<option value="Djibouti">Djibouti</option>
											<option value="Dominica">Dominica</option>
											<option value="Dominican Republic">Dominican Republic</option>
											<option value="Ecuador">Ecuador</option>
											<option value="Egypt">Egypt</option>
											<option value="El Salvador">El Salvador</option>
											<option value="Equatorial Guinea">Equatorial Guinea</option>
											<option value="Eritrea">Eritrea</option>
											<option value="Estonia">Estonia</option>
											<option value="Ethiopia">Ethiopia</option>
											<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
											<option value="Faroe Islands">Faroe Islands</option>
											<option value="Fiji">Fiji</option>
											<option value="Finland">Finland</option>
											<option value="France">France</option>
											<option value="French Guiana">French Guiana</option>
											<option value="French Polynesia">French Polynesia</option>
											<option value="French Southern Territories">French Southern Territories</option>
											<option value="Gabon">Gabon</option>
											<option value="Gambia">Gambia</option>
											<option value="Georgia">Georgia</option>
											<option value="Germany">Germany</option>
											<option value="Ghana">Ghana</option>
											<option value="Gibraltar">Gibraltar</option>
											<option value="Greece">Greece</option>
											<option value="Greenland">Greenland</option>
											<option value="Grenada">Grenada</option>
											<option value="Guadeloupe">Guadeloupe</option>
											<option value="Guam">Guam</option>
											<option value="Guatemala">Guatemala</option>
											<option value="Guernsey">Guernsey</option>
											<option value="Guinea">Guinea</option>
											<option value="Guinea-bissau">Guinea-bissau</option>
											<option value="Guyana">Guyana</option>
											<option value="Haiti">Haiti</option>
											<option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
											<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
											<option value="Honduras">Honduras</option>
											<option value="Hong Kong">Hong Kong</option>
											<option value="Hungary">Hungary</option>
											<option value="Iceland">Iceland</option>
											<option value="India">India</option>
											<option value="Indonesia">Indonesia</option>
											<option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
											<option value="Iraq">Iraq</option>
											<option value="Ireland">Ireland</option>
											<option value="Isle of Man">Isle of Man</option>
											<option value="Israel">Israel</option>
											<option selected value="Italy">Italy</option>
											<option value="Jamaica">Jamaica</option>
											<option value="Japan">Japan</option>
											<option value="Jersey">Jersey</option>
											<option value="Jordan">Jordan</option>
											<option value="Kazakhstan">Kazakhstan</option>
											<option value="Kenya">Kenya</option>
											<option value="Kiribati">Kiribati</option>
											<option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
											<option value="Korea, Republic of">Korea, Republic of</option>
											<option value="Kuwait">Kuwait</option>
											<option value="Kyrgyzstan">Kyrgyzstan</option>
											<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
											<option value="Latvia">Latvia</option>
											<option value="Lebanon">Lebanon</option>
											<option value="Lesotho">Lesotho</option>
											<option value="Liberia">Liberia</option>
											<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
											<option value="Liechtenstein">Liechtenstein</option>
											<option value="Lithuania">Lithuania</option>
											<option value="Luxembourg">Luxembourg</option>
											<option value="Macao">Macao</option>
											<option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
											<option value="Madagascar">Madagascar</option>
											<option value="Malawi">Malawi</option>
											<option value="Malaysia">Malaysia</option>
											<option value="Maldives">Maldives</option>
											<option value="Mali">Mali</option>
											<option value="Malta">Malta</option>
											<option value="Marshall Islands">Marshall Islands</option>
											<option value="Martinique">Martinique</option>
											<option value="Mauritania">Mauritania</option>
											<option value="Mauritius">Mauritius</option>
											<option value="Mayotte">Mayotte</option>
											<option value="Mexico">Mexico</option>
											<option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
											<option value="Moldova, Republic of">Moldova, Republic of</option>
											<option value="Monaco">Monaco</option>
											<option value="Mongolia">Mongolia</option>
											<option value="Montenegro">Montenegro</option>
											<option value="Montserrat">Montserrat</option>
											<option value="Morocco">Morocco</option>
											<option value="Mozambique">Mozambique</option>
											<option value="Myanmar">Myanmar</option>
											<option value="Namibia">Namibia</option>
											<option value="Nauru">Nauru</option>
											<option value="Nepal">Nepal</option>
											<option value="Netherlands">Netherlands</option>
											<option value="Netherlands Antilles">Netherlands Antilles</option>
											<option value="New Caledonia">New Caledonia</option>
											<option value="New Zealand">New Zealand</option>
											<option value="Nicaragua">Nicaragua</option>
											<option value="Niger">Niger</option>
											<option value="Nigeria">Nigeria</option>
											<option value="Niue">Niue</option>
											<option value="Norfolk Island">Norfolk Island</option>
											<option value="Northern Mariana Islands">Northern Mariana Islands</option>
											<option value="Norway">Norway</option>
											<option value="Oman">Oman</option>
											<option value="Pakistan">Pakistan</option>
											<option value="Palau">Palau</option>
											<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
											<option value="Panama">Panama</option>
											<option value="Papua New Guinea">Papua New Guinea</option>
											<option value="Paraguay">Paraguay</option>
											<option value="Peru">Peru</option>
											<option value="Philippines">Philippines</option>
											<option value="Pitcairn">Pitcairn</option>
											<option value="Poland">Poland</option>
											<option value="Portugal">Portugal</option>
											<option value="Puerto Rico">Puerto Rico</option>
											<option value="Qatar">Qatar</option>
											<option value="Reunion">Reunion</option>
											<option value="Romania">Romania</option>
											<option value="Russian Federation">Russian Federation</option>
											<option value="Rwanda">Rwanda</option>
											<option value="Saint Helena">Saint Helena</option>
											<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
											<option value="Saint Lucia">Saint Lucia</option>
											<option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
											<option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
											<option value="Samoa">Samoa</option>
											<option value="San Marino">San Marino</option>
											<option value="Sao Tome and Principe">Sao Tome and Principe</option>
											<option value="Saudi Arabia">Saudi Arabia</option>
											<option value="Senegal">Senegal</option>
											<option value="Serbia">Serbia</option>
											<option value="Seychelles">Seychelles</option>
											<option value="Sierra Leone">Sierra Leone</option>
											<option value="Singapore">Singapore</option>
											<option value="Slovakia">Slovakia</option>
											<option value="Slovenia">Slovenia</option>
											<option value="Solomon Islands">Solomon Islands</option>
											<option value="Somalia">Somalia</option>
											<option value="South Africa">South Africa</option>
											<option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
											<option value="Spain">Spain</option>
											<option value="Sri Lanka">Sri Lanka</option>
											<option value="Sudan">Sudan</option>
											<option value="Suriname">Suriname</option>
											<option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
											<option value="Swaziland">Swaziland</option>
											<option value="Sweden">Sweden</option>
											<option value="Switzerland">Switzerland</option>
											<option value="Syrian Arab Republic">Syrian Arab Republic</option>
											<option value="Taiwan">Taiwan</option>
											<option value="Tajikistan">Tajikistan</option>
											<option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
											<option value="Thailand">Thailand</option>
											<option value="Timor-leste">Timor-leste</option>
											<option value="Togo">Togo</option>
											<option value="Tokelau">Tokelau</option>
											<option value="Tonga">Tonga</option>
											<option value="Trinidad and Tobago">Trinidad and Tobago</option>
											<option value="Tunisia">Tunisia</option>
											<option value="Turkey">Turkey</option>
											<option value="Turkmenistan">Turkmenistan</option>
											<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
											<option value="Tuvalu">Tuvalu</option>
											<option value="Uganda">Uganda</option>
											<option value="Ukraine">Ukraine</option>
											<option value="United Arab Emirates">United Arab Emirates</option>
											<option value="United Kingdom">United Kingdom</option>
											<option value="United States">United States</option>
											<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
											<option value="Uruguay">Uruguay</option>
											<option value="Uzbekistan">Uzbekistan</option>
											<option value="Vanuatu">Vanuatu</option>
											<option value="Venezuela">Venezuela</option>
											<option value="Viet Nam">Viet Nam</option>
											<option value="Virgin Islands, British">Virgin Islands, British</option>
											<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
											<option value="Wallis and Futuna">Wallis and Futuna</option>
											<option value="Western Sahara">Western Sahara</option>
											<option value="Yemen">Yemen</option>
											<option value="Zambia">Zambia</option>
											<option value="Zimbabwe">Zimbabwe</option>
										</select>
										
									</div>
									<span class="form-text text-muted"></span>
								</div>
								<div class="col-lg-1"></div>


								
								
							</div>




							<br>
							<input type="hidden" name="form_id_survey" id="form_id_survey" value="<?php echo $id_survey ?>" />
						
						
						<!--end::Form-->
					</div>
					<!--end::Card-->



				</div>
			</div>
		</div>
		<!--end::Container-->
	</div>

													<div class="row">

														<div class="col-lg-12">

															<h3 class="col-lg-12 text-align-centro padding-top-20-px">Legge per il rispetto della privacy</h3>
															<h4 class="col-lg-12">La compilazione di questo form autorizza il trattamento dei dati personali ai sensi dell’art. 13 del Regolamento Europeo 2016/679 in materia di protezione dei dati personali (“Regolamento” o “GDPR”) e del D.lgs. 30/06/2003 n. 196 (“Codice Privacy”), come modificato e integrato dal D.lgs. 101/2018, e delle successive modifiche ed integrazioni. <a href="https://www.tecnocapclosures.com/it/privacy/">Per maggiori informazioni si rimanda all’informativa sulla privacy della Tecnocap S.p.A.</a></h4>




														</div>
													</div>


													<div class="row">
														<div class="col-lg-12">
															<h3 class="col-lg-12 text-align-centro padding-top-20-px">Condizioni generali</h3>

															<h4><input type="checkbox"  onchange="document.getElementById('submit-survey-button').disabled = !this.checked;" /> Ho letto l'informativa e autorizzo l’utilizzo dei miei dati personali. <a href="https://www.tecnocapclosures.com/it/richieste-informazioni-generiche/">Informativa sulla protezione dei dati di Tecnocapclosures</a></h4>


														</div>
													</div>
													<br>
													<? include include ("initial-form.php"); ?>
													<div class="row">

														<div class="col-lg-12">

															<button type="submit" name="submit-survey-button" id="submit-survey-button" class="btn mr-2" style="color: white; background-color: #bb141a;" disabled="disabled">Invia il questionario</button>

														</div>
													</div>
													<!--end::SubmitButton-->

													
													<input type="hidden" name="survey_id_survey" value="<?php echo $_GET['survey']; ?>">
													<input type="hidden" name="survey_list" value="" id="survey_list">
												 	
												</form>

												

												
											</main>

											<div class="cd-panel cd-panel--from-right js-cd-panel-main" style="z-index:999">
												<header class="cd-panel__header">
													<h1>Algoritmica Lab</h1>
													<a href="#0" class="cd-panel__close js-cd-close">Close</a>
												</header>
												<div class="cd-panel__container">
													<div class="cd-panel__content">

													</div> <!-- cd-panel__content -->
												</div> <!-- cd-panel__container -->
											</div> <!-- cd-panel -->


										</div>
										<!--end::Container-->
									</div>
									<!--end::Entry-->
								</div>




								<!--end::Content-->
								<!--begin::Footer-->
								<!--doc: add "bg-white" class to have footer with solod background color-->

								<!--end::Footer-->
							</div>
							<!--end::Wrapper-->
						</div>
						<!--end::Page-->
					</div>
					<script>var HOST_URL = "<?php echo HOME_WEBAPP; ?>";</script>
					<!--begin::Global Config(global config for global JS scripts)-->
					<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#1BC5BD", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#6993FF", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#1BC5BD", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#E1E9FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
					<!--end::Global Config-->
					<!--begin::Global Theme Bundle(used by all pages)-->
					<script src="assets/plugins/global/plugins.bundle.js"></script>
					<script src="assets/js/scripts.bundle.js"></script>
					<!--end::Global Javascript Bundle-->
					<!--begin::Page Vendors Javascript(used by this page)-->
					<script src="assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
					<!--end::Page Vendors Javascript-->
					<!--begin::Page Custom Javascript(used by this page)-->
					<script src="assets/js/custom/documentation/documentation.js"></script>
					<script src="assets/js/custom/documentation/search.js"></script>

					<!--end::Page Scripts-->
				</body>
				<footer>
					<?php include ("pages/templates/footer.php"); ?>
				</footer>
				<!--end::Body-->
				</html>



				<?php



				if($_SERVER['REQUEST_METHOD'] == "POST"){


					if (empty($_POST["form_id_survey"])) {
						$survErr = "First name is required.";
					} else {
						$surv = test_input($_POST["survey_id_survey"]);
					}




					if (empty($_POST["form_role"])) {
						$roleErr = "Role is required.";
					} else {
						$role = test_input($_POST["survey_role"]);
					}

					if (empty($_POST["form_company"])) {
						$companyErr = "Company is required.";
					} else {
						$company = test_input($_POST["survey_company"]);
					}





					if (empty($_POST["form_email"])) {
						$emailErr = "Email is required.";
					} else {
						$email = test_input($_POST["survey_email"]);
					}

					if (empty($_POST["form_phone"])) {
						$phoneErr = "Phone is required.";
					} else {
						$phone = test_input($_POST["survey_phone"]);
					}

					if (empty($_POST["form_country"])) {
						$phoneErr = "Phone is required.";
					} else {
						$phone = test_input($_POST["survey_country"]);
					}


					if (empty($_POST["survey_list"])) {
						$listErr = "You need to answer the survey.";
					} else {
						$list = test_input($_POST["survey_list"]);
					}



				}

			?> 