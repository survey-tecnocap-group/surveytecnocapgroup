<?php 
header('Content-Type: text/html; charset=utf-8');
include "class/alg_function_class.php";
include "class/alg_mysql_class.php";
include "class/alg_connection_function.php";

//includo le variabili definite globali
include ("setting/define_variable.php");

//includo il SETTING per il DEFINE per i valori interessati alle pagine
include ("setting/define_page.php");


?>
<!DOCTYPE html>
<!--
Author: Keenthemes
Product Name: Metronic - Bootstrap 5 HTML, VueJS, React, Angular & Laravel Admin Dashboard Theme
Purchase: https://1.envato.market/EA4JP
Website: http://www.keenthemes.com
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
License: For each use you must have a valid license purchased only from above link in order to legally use the theme for your project.
-->
<html lang="en">
	<!--begin::Head-->
	<head><base href="">
		<title>Surveys Tecnocap Group - Statistics</title>
		<meta name="description" content="The most advanced Bootstrap Admin Theme on Themeforest trusted by 94,000 beginners and professionals. Multi-demo, Dark Mode, RTL support and complete React, Angular, Vue &amp; Laravel versions. Grab your copy now and get life-time updates for free." />
		<meta name="keywords" content="Metronic, bootstrap, bootstrap 5, Angular, VueJs, React, Laravel, admin themes, web design, figma, web development, free templates, free admin themes, bootstrap theme, bootstrap template, bootstrap dashboard, bootstrap dak mode, bootstrap button, bootstrap datepicker, bootstrap timepicker, fullcalendar, datatables, flaticon" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta charset="utf-8" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="Metronic - Bootstrap 5 HTML, VueJS, React, Angular &amp; Laravel Admin Dashboard Theme" />
		<meta property="og:url" content="https://keenthemes.com/metronic" />
		<meta property="og:site_name" content="Keenthemes | Metronic" />
		<link rel="canonical" href="Https://preview.keenthemes.com/metronic8" />
		<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
		<!--begin::Page Vendor Stylesheets(used by this page)-->
		<link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Page Vendor Stylesheets-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<!--end::Global Stylesheets Bundle-->
	</head>
	<!--end::Head-->
	<!--begin::Body-->
	<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed toolbar-tablet-and-mobile-fixed aside-enabled aside-fixed">
		<!--begin::Main-->
		<!--begin::Root-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="page d-flex flex-row flex-column-fluid">
				
				<!--begin::Wrapper-->
				<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
					<!--begin::Header-->
					<?php include ("pages/default_header.php"); 
								
								?>
					<!--end::Header-->
					<!--begin::Toolbar-->
					
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Container-->
						<div id="kt_content_container" class="container-xxl">
							<!--begin::Row-->
							
							<!--end::Row-->
							<!--begin::Row-->
							
							<!--end::Row-->
							<!--begin::Row-->
							<div class="row gy-5 g-xl-8">
								<!--begin::Col-->
								
								<!--end::Col-->
                                <!--begin::Statements-->
							<div class="card">
								<!--begin::Header-->
								<div class="card-header card-header-stretch">
									<!--begin::Title-->
									<div class="card-title">
										<h3 class="m-0 text-gray-800">Lista Utenti</h3>
									</div>
									<!--end::Title-->
									<!--begin::Toolbar-->
									<div class="card-toolbar m-0">
										<!--begin::Tab nav-->
										<ul class="nav nav-stretch fs-5 fw-bold nav-line-tabs border-transparent" role="tablist">
											<!--<li class="nav-item" role="presentation">
												<a id="kt_referrals_year_tab" class="nav-link text-active-gray-800 active" data-bs-toggle="tab" role="tab" href="#kt_referrals_1">This Year</a>
											</li>
											<li class="nav-item" role="presentation">
												<a id="kt_referrals_2019_tab" class="nav-link text-active-gray-800 me-4" data-bs-toggle="tab" role="tab" href="#kt_referrals_2">2019</a>
											</li>
											<li class="nav-item" role="presentation">
												<a id="kt_referrals_2018_tab" class="nav-link text-active-gray-800 me-4" data-bs-toggle="tab" role="tab" href="#kt_referrals_3">2018</a>
											</li>
											<li class="nav-item" role="presentation">
												<a id="kt_referrals_2017_tab" class="nav-link text-active-gray-800 ms-8" data-bs-toggle="tab" role="tab" href="#kt_referrals_4">2017</a>
											</li> -->
										</ul>
										<!--end::Tab nav-->
									</div>
									<!--end::Toolbar-->
								</div>
								<!--end::Header-->
								<!--begin::Tab Content-->
								<div id="kt_referred_users_tab_content" class="tab-content">
									<!--begin::Tab panel-->
									<div id="kt_referrals_1" class="card-body p-0 tab-pane fade show active" role="tabpanel">
										<div class="table-responsive">
											<!--begin::Table-->
											<table class="table table-flush align-middle table-row-bordered table-row-solid gy-4 gs-9">
												<!--begin::Thead-->
												<thead class="border-gray-200 fs-5 fw-bold bg-lighten">
													<tr>
														<th class="min-w-175px ps-9">Nome</th>
														<th class="min-w-150px px-0">Cognome</th>
														<th class="min-w-350px">E-mail</th>
														<th class="min-w-125px">Telefono</th>
														<th class="min-w-125px text-align">Ruolo</th> <!-- prima era text-center-->
													</tr>
												</thead>
												<!--end::Thead-->
												<!--begin::Tbody-->
												<tbody class="fs-6 fw-bold text-gray-600">
													<?php 
														

														$rowLoginList = $alg_class_myfunction->alg_fnt_getAllLogin();
														

														foreach ($rowLoginList as $login) {
															
														
														?>
													<tr>
													
														<td><?php echo $login['nome_login'];?></td>
														<td><?php echo $login['cognome_login'];?></td>
														<td><?php echo $login['user_login'];?></td>
														<td><?php echo $login['tel_login'];?></td>
														<?php if($login['role_login'] == 1)
														{ ?>
															<td> <?php echo 'Admin'; ?></td>	
														<?php }
														else { ?> <td><?php echo'Utente'; } ?></td>
															
														
														
													</tr>
													<?php } ?>
												</tbody>
												<!--end::Tbody-->
											</table>
											<!--end::Table-->
										</div>
									</div>
									<!--end::Tab panel-->
									<!--begin::Tab panel-->
									
									<!--<div id="kt_referrals_2" class="card-body p-0 tab-pane fade" role="tabpanel">
										<div class="table-responsive">
											
											<table class="table table-flush align-middle table-row-bordered table-row-solid gy-4 gs-9">
												
												<thead class="border-gray-200 fs-5 fw-bold bg-lighten">
													<tr>
														<th class="min-w-175px ps-9">Date</th>
														<th class="min-w-150px px-0">Order ID</th>
														<th class="min-w-350px">Details</th>
														<th class="min-w-125px">Amount</th>
														<th class="min-w-125px text-center">Invoice</th>
													</tr>
												</thead>
												
												<tbody class="fs-6 fw-bold text-gray-600">
													<tr>
														<td class="ps-9">May 30, 2020</td>
														<td class="ps-0">523445943</td>
														<td>Seller Fee</td>
														<td class="text-danger">$-1.30</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Apr 22, 2020</td>
														<td class="ps-0">231445943</td>
														<td>Parcel Shipping / Delivery Service App</td>
														<td class="text-success">$204.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Feb 09, 2020</td>
														<td class="ps-0">426445943</td>
														<td>Visual Design Illustration</td>
														<td class="text-success">$31.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Nov 01, 2020</td>
														<td class="ps-0">984445943</td>
														<td>Abstract Vusial Pack</td>
														<td class="text-success">$52.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Jan 04, 2020</td>
														<td class="ps-0">324442313</td>
														<td>Seller Fee</td>
														<td class="text-danger">$-0.80</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Nov 01, 2020</td>
														<td class="ps-0">102445788</td>
														<td>Darknight transparency 36 Icons Pack</td>
														<td class="text-success">$38.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Oct 24, 2020</td>
														<td class="ps-0">423445721</td>
														<td>Seller Fee</td>
														<td class="text-danger">$-2.60</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Oct 08, 2020</td>
														<td class="ps-0">312445984</td>
														<td>Cartoon Mobile Emoji Phone Pack</td>
														<td class="text-success">$76.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Sep 15, 2020</td>
														<td class="ps-0">312445984</td>
														<td>Iphone 12 Pro Mockup Mega Bundle</td>
														<td class="text-success">$5.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
												</tbody> 
												
											</table>
											
										</div>
									</div> -->
									<!--end::Tab panel-->
									<!--begin::Tab panel-->
									<!--<div id="kt_referrals_3" class="card-body p-0 tab-pane fade" role="tabpanel">
										<div class="table-responsive">
											
											<table class="table table-flush align-middle table-row-bordered table-row-solid gy-4 gs-9">
												
												<thead class="border-gray-200 fs-5 fw-bold bg-lighten">
													<tr>
														<th class="min-w-175px ps-9">Date</th>
														<th class="min-w-150px px-0">Order ID</th>
														<th class="min-w-350px">Details</th>
														<th class="min-w-125px">Amount</th>
														<th class="min-w-125px text-center">Invoice</th>
													</tr>
												</thead>
												
												<tbody class="fs-6 fw-bold text-gray-600">
													<tr>
														<td class="ps-9">Feb 09, 2020</td>
														<td class="ps-0">426445943</td>
														<td>Visual Design Illustration</td>
														<td class="text-success">$31.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Nov 01, 2020</td>
														<td class="ps-0">984445943</td>
														<td>Abstract Vusial Pack</td>
														<td class="text-success">$52.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Jan 04, 2020</td>
														<td class="ps-0">324442313</td>
														<td>Seller Fee</td>
														<td class="text-danger">$-0.80</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Sep 15, 2020</td>
														<td class="ps-0">312445984</td>
														<td>Iphone 12 Pro Mockup Mega Bundle</td>
														<td class="text-success">$5.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Nov 01, 2020</td>
														<td class="ps-0">102445788</td>
														<td>Darknight transparency 36 Icons Pack</td>
														<td class="text-success">$38.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Oct 24, 2020</td>
														<td class="ps-0">423445721</td>
														<td>Seller Fee</td>
														<td class="text-danger">$-2.60</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Oct 08, 2020</td>
														<td class="ps-0">312445984</td>
														<td>Cartoon Mobile Emoji Phone Pack</td>
														<td class="text-success">$76.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">May 30, 2020</td>
														<td class="ps-0">523445943</td>
														<td>Seller Fee</td>
														<td class="text-danger">$-1.30</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Apr 22, 2020</td>
														<td class="ps-0">231445943</td>
														<td>Parcel Shipping / Delivery Service App</td>
														<td class="text-success">$204.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
												</tbody>
												
											</table>
											
										</div>
									</div>
									-->
									<!--
									<div id="kt_referrals_4" class="card-body p-0 tab-pane fade" role="tabpanel">
										<div class="table-responsive">
											
											<table class="table table-flush align-middle table-row-bordered table-row-solid gy-4 gs-9">
												
												<thead class="border-gray-200 fs-5 fw-bold bg-lighten">
													<tr>
														<th class="min-w-175px ps-9">Date</th>
														<th class="min-w-150px px-0">Order ID</th>
														<th class="min-w-350px">Details</th>
														<th class="min-w-125px">Amount</th>
														<th class="min-w-125px text-center">Invoice</th>
													</tr>
												</thead>
												
												<tbody class="fs-6 fw-bold text-gray-600">
													<tr>
														<td class="ps-9">Nov 01, 2020</td>
														<td class="ps-0">102445788</td>
														<td>Darknight transparency 36 Icons Pack</td>
														<td class="text-success">$38.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Oct 24, 2020</td>
														<td class="ps-0">423445721</td>
														<td>Seller Fee</td>
														<td class="text-danger">$-2.60</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Nov 01, 2020</td>
														<td class="ps-0">102445788</td>
														<td>Darknight transparency 36 Icons Pack</td>
														<td class="text-success">$38.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Oct 24, 2020</td>
														<td class="ps-0">423445721</td>
														<td>Seller Fee</td>
														<td class="text-danger">$-2.60</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Feb 09, 2020</td>
														<td class="ps-0">426445943</td>
														<td>Visual Design Illustration</td>
														<td class="text-success">$31.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Nov 01, 2020</td>
														<td class="ps-0">984445943</td>
														<td>Abstract Vusial Pack</td>
														<td class="text-success">$52.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Jan 04, 2020</td>
														<td class="ps-0">324442313</td>
														<td>Seller Fee</td>
														<td class="text-danger">$-0.80</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Oct 08, 2020</td>
														<td class="ps-0">312445984</td>
														<td>Cartoon Mobile Emoji Phone Pack</td>
														<td class="text-success">$76.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
													<tr>
														<td class="ps-9">Oct 08, 2020</td>
														<td class="ps-0">312445984</td>
														<td>Cartoon Mobile Emoji Phone Pack</td>
														<td class="text-success">$76.00</td>
														<td class="text-center">
															<button class="btn btn-light btn-sm btn-active-light-primary">Download</button>
														</td>
													</tr>
												</tbody>
												
											</table>
											
										</div>
									</div>
														-->
								</div>
								
							</div>
							<!--end::Statements-->
								<!--end::Col-->
							</div>
							<!--end::Row-->
							<!--begin::Row-->
							
							<!--end::Row-->
							<!--begin::Row-->
							
						</div>
						<!--end::Container-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
						<!--begin::Container-->
						<div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
							<!--begin::Copyright-->
							<div class="text-dark order-2 order-md-1">
								<span class="text-muted fw-bold me-1">2021©</span>
								<a href="#" target="_blank" class="text-gray-800 text-hover-primary">Surveys Tecnocap Group</a>
							</div>
							<!--end::Copyright-->
							<!--begin::Menu-->
							<ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
								<li class="menu-item">
									<a href="#" target="_blank" class="menu-link px-2"></a>
								</li>
								<li class="menu-item">
									<a href="#" target="_blank" class="menu-link px-2"></a>
								</li>
								<li class="menu-item">
									<a href="#" target="_blank" class="menu-link px-2">Concept by Algoritmica</a>
								</li>
							</ul>
							<!--end::Menu-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->
			</div>
			<!--end::Page-->
		</div>
		<!--end::Root-->
		<!--begin::Drawers-->
		<!--begin::Activities drawer-->
		
		
		<!--end::Exolore drawer toggle-->
		<!--begin::Exolore drawer-->
		<div id="kt_explore" class="bg-white" data-kt-drawer="true" data-kt-drawer-name="explore" data-kt-drawer-activate="true" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'300px', 'lg': '375px'}" data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_explore_toggle" data-kt-drawer-close="#kt_explore_close">
			<!--begin::Card-->
			<div class="card shadow-none w-100">
				<!--begin::Header-->
				<div class="card-header" id="kt_explore_header">
					<h3 class="card-title fw-bolder text-gray-700">Explore Metronic</h3>
					<div class="card-toolbar">
						<button type="button" class="btn btn-sm btn-icon btn-active-light-primary me-n5" id="kt_explore_close">
							<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
							<span class="svg-icon svg-icon-2">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
									<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</button>
					</div>
				</div>
				<!--end::Header-->
				<!--begin::Body-->
				<div class="card-body" id="kt_explore_body">
					<!--begin::Content-->
					<div id="kt_explore_scroll" class="scroll-y me-n5 pe-5" data-kt-scroll="true" data-kt-scroll-height="auto" data-kt-scroll-wrappers="#kt_explore_body" data-kt-scroll-dependencies="#kt_explore_header, #kt_explore_footer" data-kt-scroll-offset="5px">
						<!--begin::Demos-->
						<div class="mb-0">
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo1</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper rounded">
										<img src="assets/media/demos/demo1.png" alt="demo" class="w-100 rounded" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<a href="https://preview.keenthemes.com/metronic8/demo1" class="btn btn-primary shadow">Preview</a>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo2</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper rounded">
										<img src="assets/media/demos/demo2.png" alt="demo" class="w-100 rounded" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<a href="https://preview.keenthemes.com/metronic8/demo2" class="btn btn-primary shadow">Preview</a>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo3</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper rounded">
										<img src="assets/media/demos/demo3.png" alt="demo" class="w-100 rounded" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<a href="https://preview.keenthemes.com/metronic8/demo3" class="btn btn-primary shadow">Preview</a>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo4</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper rounded">
										<img src="assets/media/demos/demo4.png" alt="demo" class="w-100 rounded" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<a href="https://preview.keenthemes.com/metronic8/demo4" class="btn btn-primary shadow">Preview</a>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo5</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper rounded">
										<img src="assets/media/demos/demo5.png" alt="demo" class="w-100 rounded" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<a href="https://preview.keenthemes.com/metronic8/demo5" class="btn btn-primary shadow">Preview</a>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo6</h3>
								<div class="overlay overflow-hidden border border-4 border-primary p-2 rounded">
									<div class="overlay-wrapper rounded">
										<img src="assets/media/demos/demo6.png" alt="demo" class="w-100 rounded" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<a href="https://preview.keenthemes.com/metronic8/demo6" class="btn btn-primary shadow">Preview</a>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo7</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper rounded">
										<img src="assets/media/demos/demo7.png" alt="demo" class="w-100 rounded" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<a href="https://preview.keenthemes.com/metronic8/demo7" class="btn btn-primary shadow">Preview</a>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo8</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper rounded">
										<img src="assets/media/demos/demo8.png" alt="demo" class="w-100 rounded" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<a href="https://preview.keenthemes.com/metronic8/demo8" class="btn btn-primary shadow">Preview</a>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo9</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper rounded">
										<img src="assets/media/demos/demo9.png" alt="demo" class="w-100 rounded" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<a href="https://preview.keenthemes.com/metronic8/demo9" class="btn btn-primary shadow">Preview</a>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo10</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper">
										<img src="assets/media/demos/demo10.png" alt="demo" class="w-100 rounded opacity-75" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<div class="badge badge-white px-6 py-4 fw-bold fs-base shadow">Coming soon</div>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo11</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper">
										<img src="assets/media/demos/demo11.png" alt="demo" class="w-100 rounded opacity-75" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<div class="badge badge-white px-6 py-4 fw-bold fs-base shadow">Coming soon</div>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo12</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper">
										<img src="assets/media/demos/demo12.png" alt="demo" class="w-100 rounded opacity-75" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<div class="badge badge-white px-6 py-4 fw-bold fs-base shadow">Coming soon</div>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo13</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper rounded">
										<img src="assets/media/demos/demo13.png" alt="demo" class="w-100 rounded" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<a href="https://preview.keenthemes.com/metronic8/demo13" class="btn btn-primary shadow">Preview</a>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo14</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper">
										<img src="assets/media/demos/demo14.png" alt="demo" class="w-100 rounded opacity-75" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<div class="badge badge-white px-6 py-4 fw-bold fs-base shadow">Coming soon</div>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo15</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper">
										<img src="assets/media/demos/demo15.png" alt="demo" class="w-100 rounded opacity-75" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<div class="badge badge-white px-6 py-4 fw-bold fs-base shadow">Coming soon</div>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo16</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper">
										<img src="assets/media/demos/demo16.png" alt="demo" class="w-100 rounded opacity-75" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<div class="badge badge-white px-6 py-4 fw-bold fs-base shadow">Coming soon</div>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo17</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper">
										<img src="assets/media/demos/demo17.png" alt="demo" class="w-100 rounded opacity-75" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<div class="badge badge-white px-6 py-4 fw-bold fs-base shadow">Coming soon</div>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo18</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper">
										<img src="assets/media/demos/demo18.png" alt="demo" class="w-100 rounded opacity-75" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<div class="badge badge-white px-6 py-4 fw-bold fs-base shadow">Coming soon</div>
									</div>
								</div>
							</div>
							<!--end::Demo-->
							<!--begin::Demo-->
							<div class="mb-7">
								<h3 class="fw-bold text-center mb-3">Demo19</h3>
								<div class="overlay overflow-hidden border border-4 p-2 rounded">
									<div class="overlay-wrapper">
										<img src="assets/media/demos/demo19.png" alt="demo" class="w-100 rounded opacity-75" />
									</div>
									<div class="overlay-layer bg-dark bg-opacity-10">
										<div class="badge badge-white px-6 py-4 fw-bold fs-base shadow">Coming soon</div>
									</div>
								</div>
							</div>
							<!--end::Demo-->
						</div>
						<!--end::Demos-->
					</div>
					<!--end::Content-->
				</div>
				<!--end::Body-->
				<!--begin::Footer-->
				<div class="card-footer py-5 text-center" id="kt_explore_footer">
					<a href="https://1.envato.market/EA4JP" class="btn btn-primary">Purchase Metronic</a>
				</div>
				<!--end::Footer-->
			</div>
			<!--end::Card-->
		</div>
		<!--end::Exolore drawer-->
		<!--begin::Chat drawer-->
		
		<!--end::Chat drawer-->
		<!--end::Drawers-->
		<!--begin::Modals-->
		<!--begin::Modal - Create App-->
		<div class="modal fade" id="kt_modal_create_app" tabindex="-1" aria-hidden="true">
			<!--begin::Modal dialog-->
			<div class="modal-dialog modal-dialog-centered mw-900px">
				<!--begin::Modal content-->
				<div class="modal-content">
					<!--begin::Modal header-->
					<div class="modal-header">
						<!--begin::Modal title-->
						<h2>Create App</h2>
						<!--end::Modal title-->
						<!--begin::Close-->
						<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
							<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
							<span class="svg-icon svg-icon-1">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
									<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
						<!--end::Close-->
					</div>
					<!--end::Modal header-->
					<!--begin::Modal body-->
					<div class="modal-body py-lg-10 px-lg-10">
						<!--begin::Stepper-->
						<div class="stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid" id="kt_modal_create_app_stepper">
							<!--begin::Aside-->
							<div class="d-flex justify-content-center justify-content-xl-start flex-row-auto w-100 w-xl-300px">
								<!--begin::Nav-->
								<div class="stepper-nav ps-lg-10">
									<!--begin::Step 1-->
									<div class="stepper-item current" data-kt-stepper-element="nav">
										<!--begin::Line-->
										<div class="stepper-line w-40px"></div>
										<!--end::Line-->
										<!--begin::Icon-->
										<div class="stepper-icon w-40px h-40px">
											<i class="stepper-check fas fa-check"></i>
											<span class="stepper-number">1</span>
										</div>
										<!--end::Icon-->
										<!--begin::Label-->
										<div class="stepper-label">
											<h3 class="stepper-title">Details</h3>
											<div class="stepper-desc">Name your App</div>
										</div>
										<!--end::Label-->
									</div>
									<!--end::Step 1-->
									<!--begin::Step 2-->
									<div class="stepper-item" data-kt-stepper-element="nav">
										<!--begin::Line-->
										<div class="stepper-line w-40px"></div>
										<!--end::Line-->
										<!--begin::Icon-->
										<div class="stepper-icon w-40px h-40px">
											<i class="stepper-check fas fa-check"></i>
											<span class="stepper-number">2</span>
										</div>
										<!--begin::Icon-->
										<!--begin::Label-->
										<div class="stepper-label">
											<h3 class="stepper-title">Frameworks</h3>
											<div class="stepper-desc">Define your app framework</div>
										</div>
										<!--begin::Label-->
									</div>
									<!--end::Step 2-->
									<!--begin::Step 3-->
									<div class="stepper-item" data-kt-stepper-element="nav">
										<!--begin::Line-->
										<div class="stepper-line w-40px"></div>
										<!--end::Line-->
										<!--begin::Icon-->
										<div class="stepper-icon w-40px h-40px">
											<i class="stepper-check fas fa-check"></i>
											<span class="stepper-number">3</span>
										</div>
										<!--end::Icon-->
										<!--begin::Label-->
										<div class="stepper-label">
											<h3 class="stepper-title">Database</h3>
											<div class="stepper-desc">Select the app database type</div>
										</div>
										<!--end::Label-->
									</div>
									<!--end::Step 3-->
									<!--begin::Step 4-->
									<div class="stepper-item" data-kt-stepper-element="nav">
										<!--begin::Line-->
										<div class="stepper-line w-40px"></div>
										<!--end::Line-->
										<!--begin::Icon-->
										<div class="stepper-icon w-40px h-40px">
											<i class="stepper-check fas fa-check"></i>
											<span class="stepper-number">4</span>
										</div>
										<!--end::Icon-->
										<!--begin::Label-->
										<div class="stepper-label">
											<h3 class="stepper-title">Billing</h3>
											<div class="stepper-desc">Provide payment details</div>
										</div>
										<!--end::Label-->
									</div>
									<!--end::Step 4-->
									<!--begin::Step 5-->
									<div class="stepper-item" data-kt-stepper-element="nav">
										<!--begin::Line-->
										<div class="stepper-line w-40px"></div>
										<!--end::Line-->
										<!--begin::Icon-->
										<div class="stepper-icon w-40px h-40px">
											<i class="stepper-check fas fa-check"></i>
											<span class="stepper-number">5</span>
										</div>
										<!--end::Icon-->
										<!--begin::Label-->
										<div class="stepper-label">
											<h3 class="stepper-title">Completed</h3>
											<div class="stepper-desc">Review and Submit</div>
										</div>
										<!--end::Label-->
									</div>
									<!--end::Step 5-->
								</div>
								<!--end::Nav-->
							</div>
							<!--begin::Aside-->
							<!--begin::Content-->
							<div class="flex-row-fluid py-lg-5 px-lg-15">
								<!--begin::Form-->
								<form class="form" novalidate="novalidate" id="kt_modal_create_app_form">
									<!--begin::Step 1-->
									<div class="current" data-kt-stepper-element="content">
										<div class="w-100">
											<!--begin::Input group-->
											<div class="fv-row mb-10">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-2">
													<span class="required">App Name</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify your unique app name"></i>
												</label>
												<!--end::Label-->
												<!--begin::Input-->
												<input type="text" class="form-control form-control-lg form-control-solid" name="name" placeholder="" value="" />
												<!--end::Input-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="fv-row">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-4">
													<span class="required">Category</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Select your app category"></i>
												</label>
												<!--end::Label-->
												<!--begin:Options-->
												<div class="fv-row">
													<!--begin:Option-->
													<label class="d-flex flex-stack mb-5 cursor-pointer">
														<!--begin:Label-->
														<span class="d-flex align-items-center me-2">
															<!--begin:Icon-->
															<span class="symbol symbol-50px me-6">
																<span class="symbol-label bg-light-primary">
																	<!--begin::Svg Icon | path: icons/duotune/maps/map004.svg-->
																	<span class="svg-icon svg-icon-1 svg-icon-primary">
																		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																			<path opacity="0.3" d="M18.4 5.59998C21.9 9.09998 21.9 14.8 18.4 18.3C14.9 21.8 9.2 21.8 5.7 18.3L18.4 5.59998Z" fill="black" />
																			<path d="M12 2C6.5 2 2 6.5 2 12C2 17.5 6.5 22 12 22C17.5 22 22 17.5 22 12C22 6.5 17.5 2 12 2ZM19.9 11H13V8.8999C14.9 8.6999 16.7 8.00005 18.1 6.80005C19.1 8.00005 19.7 9.4 19.9 11ZM11 19.8999C9.7 19.6999 8.39999 19.2 7.39999 18.5C8.49999 17.7 9.7 17.2001 11 17.1001V19.8999ZM5.89999 6.90002C7.39999 8.10002 9.2 8.8 11 9V11.1001H4.10001C4.30001 9.4001 4.89999 8.00002 5.89999 6.90002ZM7.39999 5.5C8.49999 4.7 9.7 4.19998 11 4.09998V7C9.7 6.8 8.39999 6.3 7.39999 5.5ZM13 17.1001C14.3 17.3001 15.6 17.8 16.6 18.5C15.5 19.3 14.3 19.7999 13 19.8999V17.1001ZM13 4.09998C14.3 4.29998 15.6 4.8 16.6 5.5C15.5 6.3 14.3 6.80002 13 6.90002V4.09998ZM4.10001 13H11V15.1001C9.1 15.3001 7.29999 16 5.89999 17.2C4.89999 16 4.30001 14.6 4.10001 13ZM18.1 17.1001C16.6 15.9001 14.8 15.2 13 15V12.8999H19.9C19.7 14.5999 19.1 16.0001 18.1 17.1001Z" fill="black" />
																		</svg>
																	</span>
																	<!--end::Svg Icon-->
																</span>
															</span>
															<!--end:Icon-->
															<!--begin:Info-->
															<span class="d-flex flex-column">
																<span class="fw-bolder fs-6">Quick Online Courses</span>
																<span class="fs-7 text-muted">Creating a clear text structure is just one SEO</span>
															</span>
															<!--end:Info-->
														</span>
														<!--end:Label-->
														<!--begin:Input-->
														<span class="form-check form-check-custom form-check-solid">
															<input class="form-check-input" type="radio" name="category" value="1" />
														</span>
														<!--end:Input-->
													</label>
													<!--end::Option-->
													<!--begin:Option-->
													<label class="d-flex flex-stack mb-5 cursor-pointer">
														<!--begin:Label-->
														<span class="d-flex align-items-center me-2">
															<!--begin:Icon-->
															<span class="symbol symbol-50px me-6">
																<span class="symbol-label bg-light-danger">
																	<!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
																	<span class="svg-icon svg-icon-1 svg-icon-danger">
																		<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
																			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																				<rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
																				<rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
																				<rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
																				<rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
																			</g>
																		</svg>
																	</span>
																	<!--end::Svg Icon-->
																</span>
															</span>
															<!--end:Icon-->
															<!--begin:Info-->
															<span class="d-flex flex-column">
																<span class="fw-bolder fs-6">Face to Face Discussions</span>
																<span class="fs-7 text-muted">Creating a clear text structure is just one aspect</span>
															</span>
															<!--end:Info-->
														</span>
														<!--end:Label-->
														<!--begin:Input-->
														<span class="form-check form-check-custom form-check-solid">
															<input class="form-check-input" type="radio" name="category" value="2" />
														</span>
														<!--end:Input-->
													</label>
													<!--end::Option-->
													<!--begin:Option-->
													<label class="d-flex flex-stack cursor-pointer">
														<!--begin:Label-->
														<span class="d-flex align-items-center me-2">
															<!--begin:Icon-->
															<span class="symbol symbol-50px me-6">
																<span class="symbol-label bg-light-success">
																	<!--begin::Svg Icon | path: icons/duotune/general/gen013.svg-->
																	<span class="svg-icon svg-icon-1 svg-icon-success">
																		<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																			<path opacity="0.3" d="M20.9 12.9C20.3 12.9 19.9 12.5 19.9 11.9C19.9 11.3 20.3 10.9 20.9 10.9H21.8C21.3 6.2 17.6 2.4 12.9 2V2.9C12.9 3.5 12.5 3.9 11.9 3.9C11.3 3.9 10.9 3.5 10.9 2.9V2C6.19999 2.5 2.4 6.2 2 10.9H2.89999C3.49999 10.9 3.89999 11.3 3.89999 11.9C3.89999 12.5 3.49999 12.9 2.89999 12.9H2C2.5 17.6 6.19999 21.4 10.9 21.8V20.9C10.9 20.3 11.3 19.9 11.9 19.9C12.5 19.9 12.9 20.3 12.9 20.9V21.8C17.6 21.3 21.4 17.6 21.8 12.9H20.9Z" fill="black" />
																			<path d="M16.9 10.9H13.6C13.4 10.6 13.2 10.4 12.9 10.2V5.90002C12.9 5.30002 12.5 4.90002 11.9 4.90002C11.3 4.90002 10.9 5.30002 10.9 5.90002V10.2C10.6 10.4 10.4 10.6 10.2 10.9H9.89999C9.29999 10.9 8.89999 11.3 8.89999 11.9C8.89999 12.5 9.29999 12.9 9.89999 12.9H10.2C10.4 13.2 10.6 13.4 10.9 13.6V13.9C10.9 14.5 11.3 14.9 11.9 14.9C12.5 14.9 12.9 14.5 12.9 13.9V13.6C13.2 13.4 13.4 13.2 13.6 12.9H16.9C17.5 12.9 17.9 12.5 17.9 11.9C17.9 11.3 17.5 10.9 16.9 10.9Z" fill="black" />
																		</svg>
																	</span>
																	<!--end::Svg Icon-->
																</span>
															</span>
															<!--end:Icon-->
															<!--begin:Info-->
															<span class="d-flex flex-column">
																<span class="fw-bolder fs-6">Full Intro Training</span>
																<span class="fs-7 text-muted">Creating a clear text structure copywriting</span>
															</span>
															<!--end:Info-->
														</span>
														<!--end:Label-->
														<!--begin:Input-->
														<span class="form-check form-check-custom form-check-solid">
															<input class="form-check-input" type="radio" name="category" value="3" />
														</span>
														<!--end:Input-->
													</label>
													<!--end::Option-->
												</div>
												<!--end:Options-->
											</div>
											<!--end::Input group-->
										</div>
									</div>
									<!--end::Step 1-->
									<!--begin::Step 2-->
									<div data-kt-stepper-element="content">
										<div class="w-100">
											<!--begin::Input group-->
											<div class="fv-row">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-4">
													<span class="required">Select Framework</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify your apps framework"></i>
												</label>
												<!--end::Label-->
												<!--begin:Option-->
												<label class="d-flex flex-stack cursor-pointer mb-5">
													<!--begin:Label-->
													<span class="d-flex align-items-center me-2">
														<!--begin:Icon-->
														<span class="symbol symbol-50px me-6">
															<span class="symbol-label bg-light-warning">
																<i class="fab fa-html5 text-warning fs-2x"></i>
															</span>
														</span>
														<!--end:Icon-->
														<!--begin:Info-->
														<span class="d-flex flex-column">
															<span class="fw-bolder fs-6">HTML5</span>
															<span class="fs-7 text-muted">Base Web Projec</span>
														</span>
														<!--end:Info-->
													</span>
													<!--end:Label-->
													<!--begin:Input-->
													<span class="form-check form-check-custom form-check-solid">
														<input class="form-check-input" type="radio" checked="checked" name="framework" value="1" />
													</span>
													<!--end:Input-->
												</label>
												<!--end::Option-->
												<!--begin:Option-->
												<label class="d-flex flex-stack cursor-pointer mb-5">
													<!--begin:Label-->
													<span class="d-flex align-items-center me-2">
														<!--begin:Icon-->
														<span class="symbol symbol-50px me-6">
															<span class="symbol-label bg-light-success">
																<i class="fab fa-react text-success fs-2x"></i>
															</span>
														</span>
														<!--end:Icon-->
														<!--begin:Info-->
														<span class="d-flex flex-column">
															<span class="fw-bolder fs-6">ReactJS</span>
															<span class="fs-7 text-muted">Robust and flexible app framework</span>
														</span>
														<!--end:Info-->
													</span>
													<!--end:Label-->
													<!--begin:Input-->
													<span class="form-check form-check-custom form-check-solid">
														<input class="form-check-input" type="radio" name="framework" value="2" />
													</span>
													<!--end:Input-->
												</label>
												<!--end::Option-->
												<!--begin:Option-->
												<label class="d-flex flex-stack cursor-pointer mb-5">
													<!--begin:Label-->
													<span class="d-flex align-items-center me-2">
														<!--begin:Icon-->
														<span class="symbol symbol-50px me-6">
															<span class="symbol-label bg-light-danger">
																<i class="fab fa-angular text-danger fs-2x"></i>
															</span>
														</span>
														<!--end:Icon-->
														<!--begin:Info-->
														<span class="d-flex flex-column">
															<span class="fw-bolder fs-6">Angular</span>
															<span class="fs-7 text-muted">Powerful data mangement</span>
														</span>
														<!--end:Info-->
													</span>
													<!--end:Label-->
													<!--begin:Input-->
													<span class="form-check form-check-custom form-check-solid">
														<input class="form-check-input" type="radio" name="framework" value="3" />
													</span>
													<!--end:Input-->
												</label>
												<!--end::Option-->
												<!--begin:Option-->
												<label class="d-flex flex-stack cursor-pointer">
													<!--begin:Label-->
													<span class="d-flex align-items-center me-2">
														<!--begin:Icon-->
														<span class="symbol symbol-50px me-6">
															<span class="symbol-label bg-light-primary">
																<i class="fab fa-vuejs text-primary fs-2x"></i>
															</span>
														</span>
														<!--end:Icon-->
														<!--begin:Info-->
														<span class="d-flex flex-column">
															<span class="fw-bolder fs-6">Vue</span>
															<span class="fs-7 text-muted">Lightweight and responsive framework</span>
														</span>
														<!--end:Info-->
													</span>
													<!--end:Label-->
													<!--begin:Input-->
													<span class="form-check form-check-custom form-check-solid">
														<input class="form-check-input" type="radio" name="framework" value="4" />
													</span>
													<!--end:Input-->
												</label>
												<!--end::Option-->
											</div>
											<!--end::Input group-->
										</div>
									</div>
									<!--end::Step 2-->
									<!--begin::Step 3-->
									<div data-kt-stepper-element="content">
										<div class="w-100">
											<!--begin::Input group-->
											<div class="fv-row mb-10">
												<!--begin::Label-->
												<label class="required fs-5 fw-bold mb-2">Database Name</label>
												<!--end::Label-->
												<!--begin::Input-->
												<input type="text" class="form-control form-control-lg form-control-solid" name="dbname" placeholder="" value="master_db" />
												<!--end::Input-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="fv-row">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-5 fw-bold mb-4">
													<span class="required">Select Database Engine</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Select your app database engine"></i>
												</label>
												<!--end::Label-->
												<!--begin:Option-->
												<label class="d-flex flex-stack cursor-pointer mb-5">
													<!--begin::Label-->
													<span class="d-flex align-items-center me-2">
														<!--begin::Icon-->
														<span class="symbol symbol-50px me-6">
															<span class="symbol-label bg-light-success">
																<i class="fas fa-database text-success fs-2x"></i>
															</span>
														</span>
														<!--end::Icon-->
														<!--begin::Info-->
														<span class="d-flex flex-column">
															<span class="fw-bolder fs-6">MySQL</span>
															<span class="fs-7 text-muted">Basic MySQL database</span>
														</span>
														<!--end::Info-->
													</span>
													<!--end::Label-->
													<!--begin::Input-->
													<span class="form-check form-check-custom form-check-solid">
														<input class="form-check-input" type="radio" name="dbengine" checked="checked" value="1" />
													</span>
													<!--end::Input-->
												</label>
												<!--end::Option-->
												<!--begin:Option-->
												<label class="d-flex flex-stack cursor-pointer mb-5">
													<!--begin::Label-->
													<span class="d-flex align-items-center me-2">
														<!--begin::Icon-->
														<span class="symbol symbol-50px me-6">
															<span class="symbol-label bg-light-danger">
																<i class="fab fa-google text-danger fs-2x"></i>
															</span>
														</span>
														<!--end::Icon-->
														<!--begin::Info-->
														<span class="d-flex flex-column">
															<span class="fw-bolder fs-6">Firebase</span>
															<span class="fs-7 text-muted">Google based app data management</span>
														</span>
														<!--end::Info-->
													</span>
													<!--end::Label-->
													<!--begin::Input-->
													<span class="form-check form-check-custom form-check-solid">
														<input class="form-check-input" type="radio" name="dbengine" value="2" />
													</span>
													<!--end::Input-->
												</label>
												<!--end::Option-->
												<!--begin:Option-->
												<label class="d-flex flex-stack cursor-pointer">
													<!--begin::Label-->
													<span class="d-flex align-items-center me-2">
														<!--begin::Icon-->
														<span class="symbol symbol-50px me-6">
															<span class="symbol-label bg-light-warning">
																<i class="fab fa-amazon text-warning fs-2x"></i>
															</span>
														</span>
														<!--end::Icon-->
														<!--begin::Info-->
														<span class="d-flex flex-column">
															<span class="fw-bolder fs-6">DynamoDB</span>
															<span class="fs-7 text-muted">Amazon Fast NoSQL Database</span>
														</span>
														<!--end::Info-->
													</span>
													<!--end::Label-->
													<!--begin::Input-->
													<span class="form-check form-check-custom form-check-solid">
														<input class="form-check-input" type="radio" name="dbengine" value="3" />
													</span>
													<!--end::Input-->
												</label>
												<!--end::Option-->
											</div>
											<!--end::Input group-->
										</div>
									</div>
									<!--end::Step 3-->
									<!--begin::Step 4-->
									<div data-kt-stepper-element="content">
										<div class="w-100">
											<!--begin::Input group-->
											<div class="d-flex flex-column mb-7 fv-row">
												<!--begin::Label-->
												<label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">
													<span class="required">Name On Card</span>
													<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Specify a card holder's name"></i>
												</label>
												<!--end::Label-->
												<input type="text" class="form-control form-control-solid" placeholder="" name="card_name" value="Max Doe" />
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="d-flex flex-column mb-7 fv-row">
												<!--begin::Label-->
												<label class="required fs-6 fw-bold form-label mb-2">Card Number</label>
												<!--end::Label-->
												<!--begin::Input wrapper-->
												<div class="position-relative">
													<!--begin::Input-->
													<input type="text" class="form-control form-control-solid" placeholder="Enter card number" name="card_number" value="4111 1111 1111 1111" />
													<!--end::Input-->
													<!--begin::Card logos-->
													<div class="position-absolute translate-middle-y top-50 end-0 me-5">
														<img src="assets/media/svg/card-logos/visa.svg" alt="" class="h-25px" />
														<img src="assets/media/svg/card-logos/mastercard.svg" alt="" class="h-25px" />
														<img src="assets/media/svg/card-logos/american-express.svg" alt="" class="h-25px" />
													</div>
													<!--end::Card logos-->
												</div>
												<!--end::Input wrapper-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="row mb-10">
												<!--begin::Col-->
												<div class="col-md-8 fv-row">
													<!--begin::Label-->
													<label class="required fs-6 fw-bold form-label mb-2">Expiration Date</label>
													<!--end::Label-->
													<!--begin::Row-->
													<div class="row fv-row">
														<!--begin::Col-->
														<div class="col-6">
															<select name="card_expiry_month" class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Month">
																<option></option>
																<option value="1">1</option>
																<option value="2">2</option>
																<option value="3">3</option>
																<option value="4">4</option>
																<option value="5">5</option>
																<option value="6">6</option>
																<option value="7">7</option>
																<option value="8">8</option>
																<option value="9">9</option>
																<option value="10">10</option>
																<option value="11">11</option>
																<option value="12">12</option>
															</select>
														</div>
														<!--end::Col-->
														<!--begin::Col-->
														<div class="col-6">
															<select name="card_expiry_year" class="form-select form-select-solid" data-control="select2" data-hide-search="true" data-placeholder="Year">
																<option></option>
																<option value="2021">2021</option>
																<option value="2022">2022</option>
																<option value="2023">2023</option>
																<option value="2024">2024</option>
																<option value="2025">2025</option>
																<option value="2026">2026</option>
																<option value="2027">2027</option>
																<option value="2028">2028</option>
																<option value="2029">2029</option>
																<option value="2030">2030</option>
																<option value="2031">2031</option>
															</select>
														</div>
														<!--end::Col-->
													</div>
													<!--end::Row-->
												</div>
												<!--end::Col-->
												<!--begin::Col-->
												<div class="col-md-4 fv-row">
													<!--begin::Label-->
													<label class="d-flex align-items-center fs-6 fw-bold form-label mb-2">
														<span class="required">CVV</span>
														<i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Enter a card CVV code"></i>
													</label>
													<!--end::Label-->
													<!--begin::Input wrapper-->
													<div class="position-relative">
														<!--begin::Input-->
														<input type="text" class="form-control form-control-solid" minlength="3" maxlength="4" placeholder="CVV" name="card_cvv" />
														<!--end::Input-->
														<!--begin::CVV icon-->
														<div class="position-absolute translate-middle-y top-50 end-0 me-3">
															<!--begin::Svg Icon | path: icons/duotune/finance/fin002.svg-->
															<span class="svg-icon svg-icon-2hx">
																<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																	<path d="M22 7H2V11H22V7Z" fill="black" />
																	<path opacity="0.3" d="M21 19H3C2.4 19 2 18.6 2 18V6C2 5.4 2.4 5 3 5H21C21.6 5 22 5.4 22 6V18C22 18.6 21.6 19 21 19ZM14 14C14 13.4 13.6 13 13 13H5C4.4 13 4 13.4 4 14C4 14.6 4.4 15 5 15H13C13.6 15 14 14.6 14 14ZM16 15.5C16 16.3 16.7 17 17.5 17H18.5C19.3 17 20 16.3 20 15.5C20 14.7 19.3 14 18.5 14H17.5C16.7 14 16 14.7 16 15.5Z" fill="black" />
																</svg>
															</span>
															<!--end::Svg Icon-->
														</div>
														<!--end::CVV icon-->
													</div>
													<!--end::Input wrapper-->
												</div>
												<!--end::Col-->
											</div>
											<!--end::Input group-->
											<!--begin::Input group-->
											<div class="d-flex flex-stack">
												<!--begin::Label-->
												<div class="me-5">
													<label class="fs-6 fw-bold form-label">Save Card for further billing?</label>
													<div class="fs-7 fw-bold text-muted">If you need more info, please check budget planning</div>
												</div>
												<!--end::Label-->
												<!--begin::Switch-->
												<label class="form-check form-switch form-check-custom form-check-solid">
													<input class="form-check-input" type="checkbox" value="1" checked="checked" />
													<span class="form-check-label fw-bold text-muted">Save Card</span>
												</label>
												<!--end::Switch-->
											</div>
											<!--end::Input group-->
										</div>
									</div>
									<!--end::Step 4-->
									<!--begin::Step 5-->
									<div data-kt-stepper-element="content">
										<div class="w-100 text-center">
											<!--begin::Heading-->
											<h1 class="fw-bolder text-dark mb-3">Release!</h1>
											<!--end::Heading-->
											<!--begin::Description-->
											<div class="text-muted fw-bold fs-3">Submit your app to kickstart your project.</div>
											<!--end::Description-->
											<!--begin::Illustration-->
											<div class="text-center px-4 py-15">
												<img src="assets/media/illustrations/sketchy-1/9.png" alt="" class="w-100 mh-300px" />
											</div>
											<!--end::Illustration-->
										</div>
									</div>
									<!--end::Step 5-->
									<!--begin::Actions-->
									<div class="d-flex flex-stack pt-10">
										<!--begin::Wrapper-->
										<div class="me-2">
											<button type="button" class="btn btn-lg btn-light-primary me-3" data-kt-stepper-action="previous">
											<!--begin::Svg Icon | path: icons/duotune/arrows/arr063.svg-->
											<span class="svg-icon svg-icon-3 me-1">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<rect opacity="0.5" x="6" y="11" width="13" height="2" rx="1" fill="black" />
													<path d="M8.56569 11.4343L12.75 7.25C13.1642 6.83579 13.1642 6.16421 12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75L5.70711 11.2929C5.31658 11.6834 5.31658 12.3166 5.70711 12.7071L11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25C13.1642 17.8358 13.1642 17.1642 12.75 16.75L8.56569 12.5657C8.25327 12.2533 8.25327 11.7467 8.56569 11.4343Z" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon-->Back</button>
										</div>
										<!--end::Wrapper-->
										<!--begin::Wrapper-->
										<div>
											<button type="button" class="btn btn-lg btn-primary" data-kt-stepper-action="submit">
												<span class="indicator-label">Submit
												<!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
												<span class="svg-icon svg-icon-3 ms-2 me-0">
													<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
														<rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="black" />
														<path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="black" />
													</svg>
												</span>
												<!--end::Svg Icon--></span>
												<span class="indicator-progress">Please wait...
												<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
											</button>
											<button type="button" class="btn btn-lg btn-primary" data-kt-stepper-action="next">Continue
											<!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
											<span class="svg-icon svg-icon-3 ms-1 me-0">
												<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
													<rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="black" />
													<path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="black" />
												</svg>
											</span>
											<!--end::Svg Icon--></button>
										</div>
										<!--end::Wrapper-->
									</div>
									<!--end::Actions-->
								</form>
								<!--end::Form-->
							</div>
							<!--end::Content-->
						</div>
						<!--end::Stepper-->
					</div>
					<!--end::Modal body-->
				</div>
				<!--end::Modal content-->
			</div>
			<!--end::Modal dialog-->
		</div>
		<!--end::Modal - Create App-->
		<!--begin::Modal - Select Location-->
		<div class="modal fade" id="kt_modal_select_location" tabindex="-1" aria-hidden="true">
			<!--begin::Modal dialog-->
			<div class="modal-dialog mw-1000px">
				<!--begin::Modal content-->
				<div class="modal-content">
					<!--begin::Modal header-->
					<div class="modal-header">
						<!--begin::Title-->
						<h2>Select Location</h2>
						<!--end::Title-->
						<!--begin::Close-->
						<div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
							<!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
							<span class="svg-icon svg-icon-1">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
									<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
									<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
								</svg>
							</span>
							<!--end::Svg Icon-->
						</div>
						<!--end::Close-->
					</div>
					<!--end::Modal header-->
					<!--begin::Modal body-->
					<div class="modal-body">
						<div id="kt_modal_select_location_map" class="w-100 rounded" style="height:450px"></div>
					</div>
					<!--end::Modal body-->
					<!--begin::Modal footer-->
					<div class="modal-footer d-flex justify-content-end">
						<a href="#" class="btn btn-active-light me-5" data-bs-dismiss="modal">Cancel</a>
						<button type="button" id="kt_modal_select_location_button" class="btn btn-primary" data-bs-dismiss="modal">Apply</button>
					</div>
					<!--end::Modal footer-->
				</div>
				<!--end::Modal content-->
			</div>
			<!--end::Modal dialog-->
		</div>
		<!--end::Modal - Select Location-->
		<!--end::Modals-->
		<!--begin::Scrolltop-->
		<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
			<!--begin::Svg Icon | path: icons/duotune/arrows/arr066.svg-->
			<span class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
					<rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
					<path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
				</svg>
			</span>
			<!--end::Svg Icon-->
		</div>
		<!--end::Scrolltop-->
		<!--end::Main-->
		<script>var hostUrl = "assets/";</script>
		<!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="assets/plugins/global/plugins.bundle.js"></script>
		<script src="assets/js/scripts.bundle.js"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->
		<script src="assets/plugins/custom/fullcalendar/fullcalendar.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->
		<script src="assets/plugins/custom/datatables/datatables.bundle.js"></script>
		<!--end::Page Vendors Javascript-->
		<!--begin::Page Custom Javascript(used by this page)-->
		<script src="assets/js/custom/apps/subscriptions/add/advanced.js"></script>

		<script src="assets/js/custom/modals/new-card.js"></script>
		<script src="assets/js/custom/widgets.js"></script>
		<!--end::Page Custom Javascript-->
		<!--end::Javascript-->
	</body>
	<!--end::Body-->
</html>