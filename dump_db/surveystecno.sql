-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Ott 20, 2021 alle 12:28
-- Versione del server: 10.4.21-MariaDB
-- Versione PHP: 7.3.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surveystecno`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `relation_category_question`
--

CREATE TABLE `relation_category_question` (
  `id_category_rcq` int(11) NOT NULL,
  `id_question_rcq` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `relation_category_question`
--

INSERT INTO `relation_category_question` (`id_category_rcq`, `id_question_rcq`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 4),
(2, 5),
(2, 6),
(3, 7),
(3, 8),
(3, 9),
(4, 10),
(4, 11),
(5, 12),
(5, 13),
(6, 14);

-- --------------------------------------------------------

--
-- Struttura della tabella `relation_form_survey`
--

CREATE TABLE `relation_form_survey` (
  `id_form_rfs` int(11) NOT NULL,
  `id_survey_rfs` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `relation_survey_category`
--

CREATE TABLE `relation_survey_category` (
  `id_survey_rsc` int(11) NOT NULL,
  `id_category_rsc` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `relation_survey_category`
--

INSERT INTO `relation_survey_category` (`id_survey_rsc`, `id_category_rsc`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 6);

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id_category` int(11) NOT NULL,
  `title_category` varchar(50) NOT NULL,
  `description_category` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `tbl_category`
--

INSERT INTO `tbl_category` (`id_category`, `title_category`, `description_category`) VALUES
(1, 'Area Commerciale', 'Come valuti il grado di soddisfazione rispetto ai seguenti aspetti?'),
(2, 'Qualità Prodotto', 'Come valuti il grado di soddisfazione rispetto agli aspetti legati alla qualità del prodotto:'),
(3, 'Assistenza Tecnica', 'Come valuti il grado di soddisfazione rispetto all\'assistenza cliente:'),
(4, 'Logistica', 'Come valuti il grado di soddisfazione rispetto agli aspetti legati logistici'),
(5, 'Valutazione Globale', NULL),
(6, 'Category Secondo Survey', 'Categoria di esempio di un altro survey');

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_form`
--

CREATE TABLE `tbl_form` (
  `id_form` int(11) NOT NULL,
  `firstname_form` varchar(100) NOT NULL,
  `lastname_form` varchar(50) NOT NULL,
  `role_form` int(11) NOT NULL,
  `company_form` varchar(75) NOT NULL,
  `nation_form` varchar(75) NOT NULL,
  `phone_form` varchar(50) DEFAULT NULL,
  `email_form` varchar(50) NOT NULL,
  `survey_related_form` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `tbl_form`
--

INSERT INTO `tbl_form` (`id_form`, `firstname_form`, `lastname_form`, `role_form`, `company_form`, `nation_form`, `phone_form`, `email_form`, `survey_related_form`) VALUES
(9, 'Riccardo', 'D\'Auria', 1, 'Algoritmica', 'Italy', '3317244489', 'riccardo.dauria@algoritmica.it', 1),
(10, 'Domenico', 'D\'Andretta', 2, 'Algoritmica Lab Cooperativa', 'Italy', '', 'dandretta@algoritmica.it', 1),
(11, 'dfsdf', 'sdfsd', 1, 'sdfsdf', 'Italy', '32423', 'sdfsdffss@dfsdfsd', 1),
(12, 'Martina', 'Test', 6, 'La Sveva Random', 'Iceland', '349666555', 'mart.buono@gmail.com', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_form_role`
--

CREATE TABLE `tbl_form_role` (
  `id_form_role` int(11) NOT NULL,
  `name_form_role` varchar(100) NOT NULL,
  `language_form_role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `tbl_form_role`
--

INSERT INTO `tbl_form_role` (`id_form_role`, `name_form_role`, `language_form_role`) VALUES
(1, 'Amministratore delegato', 'it'),
(2, 'Direttore finanziario', 'it'),
(3, 'Direttore tecnico', 'it'),
(4, 'Responsabile risorse umane', 'it'),
(5, 'Responsabile amministrazione', 'it'),
(6, 'Responsabile ambiente', 'it'),
(7, 'Responsabile sicurezza', 'it'),
(8, 'Altro', 'it');

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_history_login`
--

CREATE TABLE `tbl_history_login` (
  `id_history_login` int(11) NOT NULL,
  `user_history_login` int(11) NOT NULL,
  `access_date_history_login` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `tbl_history_login`
--

INSERT INTO `tbl_history_login` (`id_history_login`, `user_history_login`, `access_date_history_login`) VALUES
(1, 1, '2021-10-05 12:08:02'),
(2, 1, '2021-10-05 12:08:08'),
(3, 1, '2021-10-05 12:08:40'),
(4, 1, '2021-10-05 12:08:50'),
(5, 1, '2021-10-05 12:10:15'),
(6, 1, '2021-10-05 12:15:36'),
(7, 1, '2021-10-05 12:38:59'),
(8, 1, '2021-10-06 12:56:41'),
(9, 1, '2021-10-06 12:57:05'),
(10, 1, '2021-10-06 13:01:30'),
(11, 1, '2021-10-07 15:06:28'),
(12, 1, '2021-10-08 09:54:37'),
(13, 1, '2021-10-08 09:57:35'),
(14, 1, '2021-10-08 10:32:23'),
(15, 1, '2021-10-08 10:32:49'),
(16, 1, '2021-10-08 11:39:02'),
(17, 1, '2021-10-08 12:53:47'),
(18, 1, '2021-10-08 13:23:51'),
(19, 1, '2021-10-08 13:26:52'),
(20, 1, '2021-10-08 13:27:08'),
(21, 1, '2021-10-08 15:17:03'),
(22, 1, '2021-10-08 15:36:02'),
(23, 1, '2021-10-11 10:02:10'),
(24, 1, '2021-10-13 10:16:14'),
(25, 1, '2021-10-14 09:48:29');

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_language`
--

CREATE TABLE `tbl_language` (
  `id_language` int(11) NOT NULL,
  `name_language` varchar(50) NOT NULL,
  `shortname_language` varchar(10) NOT NULL,
  `icu_language` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `tbl_language`
--

INSERT INTO `tbl_language` (`id_language`, `name_language`, `shortname_language`, `icu_language`) VALUES
(1, 'Italiano', 'it', 'it_IT'),
(2, 'Inglese (USA)', 'en', 'en_US');

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_login`
--

CREATE TABLE `tbl_login` (
  `id_login` int(11) NOT NULL,
  `user_login` varchar(50) NOT NULL,
  `password_login` varchar(50) NOT NULL,
  `role_login` int(11) NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT 1,
  `nome_login` varchar(50) NOT NULL,
  `cognome_login` varchar(50) NOT NULL,
  `tel_login` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `tbl_login`
--

INSERT INTO `tbl_login` (`id_login`, `user_login`, `password_login`, `role_login`, `isActive`, `nome_login`, `cognome_login`, `tel_login`) VALUES
(1, 'admin@algoritmica.it', '25d55ad283aa400af464c76d713c07ad', 1, 1, 'Peppe', 'Bianchi', '33333333');

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_question`
--

CREATE TABLE `tbl_question` (
  `id_question` int(11) NOT NULL,
  `title_question` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `tbl_question`
--

INSERT INTO `tbl_question` (`id_question`, `title_question`) VALUES
(1, 'Chiarezza delle informazioni'),
(2, 'Flessibilità'),
(3, 'Rispetto tempi di consegna'),
(4, 'Performance del prodotto'),
(5, 'Chiarezza documentazione tecnica'),
(6, 'Efficacia del supporto tecnico'),
(7, 'Tempi di intervento'),
(8, 'Gestione delle problematiche'),
(9, 'Chiarezza delle informazioni'),
(10, 'Condizioni di ricevimento'),
(11, 'Conformità dei documenti di trasporto'),
(12, 'Prestazioni di Tecnocap rispetto le vostre aspettative'),
(13, 'Servizio offerto da Tecnocap rispetto ai concorrenti'),
(14, 'Domanda di prova');

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_result`
--

CREATE TABLE `tbl_result` (
  `id_result` int(11) NOT NULL,
  `id_survey_result` int(11) NOT NULL,
  `id_form_result` int(11) NOT NULL,
  `id_category_result` int(11) NOT NULL,
  `id_answer_result` int(11) NOT NULL,
  `value_answer_result` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `tbl_result`
--

INSERT INTO `tbl_result` (`id_result`, `id_survey_result`, `id_form_result`, `id_category_result`, `id_answer_result`, `value_answer_result`) VALUES
(108, 1, 9, 1, 1, 4),
(109, 1, 9, 1, 2, 5),
(110, 1, 9, 1, 3, 5),
(111, 1, 9, 2, 4, 5),
(112, 1, 9, 2, 5, -1),
(113, 1, 9, 2, 6, 3),
(114, 1, 9, 3, 7, 4),
(115, 1, 9, 3, 8, 3),
(116, 1, 9, 3, 9, 1),
(117, 1, 9, 4, 10, -1),
(118, 1, 9, 4, 11, -1),
(119, 1, 9, 5, 12, 3),
(120, 1, 9, 5, 13, 5),
(121, 1, 10, 1, 1, 5),
(122, 1, 10, 1, 2, 5),
(123, 1, 10, 1, 3, 5),
(124, 1, 10, 2, 4, 5),
(125, 1, 10, 2, 5, 5),
(126, 1, 10, 2, 6, 5),
(127, 1, 10, 3, 7, 5),
(128, 1, 10, 3, 8, 5),
(129, 1, 10, 3, 9, 5),
(130, 1, 10, 4, 10, 5),
(131, 1, 10, 4, 11, 5),
(132, 1, 10, 5, 12, 5),
(133, 1, 10, 5, 13, 5),
(134, 1, 11, 1, 1, 4),
(135, 1, 11, 1, 2, -1),
(136, 1, 11, 1, 3, 4),
(137, 1, 11, 2, 4, 4),
(138, 1, 11, 2, 5, 5),
(139, 1, 11, 2, 6, 4),
(140, 1, 11, 3, 7, 4),
(141, 1, 11, 3, 8, 5),
(142, 1, 11, 3, 9, -1),
(143, 1, 11, 4, 10, 5),
(144, 1, 11, 4, 11, -1),
(145, 1, 11, 5, 12, 5),
(146, 1, 11, 5, 13, -1),
(147, 1, 12, 1, 1, 4),
(148, 1, 12, 1, 2, 5),
(149, 1, 12, 1, 3, -1),
(150, 1, 12, 2, 4, 3),
(151, 1, 12, 2, 5, -1),
(152, 1, 12, 2, 6, 4),
(153, 1, 12, 3, 7, 3),
(154, 1, 12, 3, 8, -1),
(155, 1, 12, 3, 9, 4),
(156, 1, 12, 4, 10, 2),
(157, 1, 12, 4, 11, -1),
(158, 1, 12, 5, 12, 3),
(159, 1, 12, 5, 13, 5);

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_role`
--

CREATE TABLE `tbl_role` (
  `id_role` int(11) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  `role_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `tbl_role`
--

INSERT INTO `tbl_role` (`id_role`, `role_name`, `role_description`) VALUES
(1, 'admin', 'The admin has full access to all features');

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_survey`
--

CREATE TABLE `tbl_survey` (
  `id_survey` int(11) NOT NULL,
  `title_survey` varchar(100) NOT NULL,
  `description_survey` text NOT NULL,
  `data_create_survey` datetime NOT NULL DEFAULT current_timestamp(),
  `data_start_survey` datetime NOT NULL,
  `data_end_survey` datetime NOT NULL,
  `is_active_survey` tinyint(1) NOT NULL,
  `language_survey` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `tbl_survey`
--

INSERT INTO `tbl_survey` (`id_survey`, `title_survey`, `description_survey`, `data_create_survey`, `data_start_survey`, `data_end_survey`, `is_active_survey`, `language_survey`) VALUES
(1, 'Customer Satisfaction Form - October 2021', 'Inserire qui una breve descrizione dell\'utilità di questo singolo sondaggio specifico.', '2021-10-12 09:59:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '1'),
(2, 'Secondo Questionario di Prova', 'Un altro questionario utilizzato per verificare la correttezza dei valori ricavati dalla query', '2021-10-12 09:59:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '1');

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_text`
--

CREATE TABLE `tbl_text` (
  `id_text` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` text CHARACTER SET utf8 NOT NULL,
  `view` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `tbl_text`
--

INSERT INTO `tbl_text` (`id_text`, `name`, `value`, `view`) VALUES
(1, 'title_page_it', 'Tecnocap Questionario di Soddisfazione', 'global'),
(2, 'title_page_en', 'Tecnocap Satisfaction Questionnaire', 'global'),
(3, 'submit_it', 'Invia', 'global'),
(4, 'submit_en', 'Send', 'global'),
(5, 'cancel_it', 'Cancella', 'global'),
(6, 'cancel_en', 'Cancel', 'global');

-- --------------------------------------------------------

--
-- Struttura della tabella `tbl_valutation`
--

CREATE TABLE `tbl_valutation` (
  `id_valutation` int(3) NOT NULL,
  `label_valutation` varchar(30) NOT NULL,
  `value_valutation` int(3) NOT NULL,
  `symbol_value` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `tbl_valutation`
--

INSERT INTO `tbl_valutation` (`id_valutation`, `label_valutation`, `value_valutation`, `symbol_value`) VALUES
(1, 'Completamente insoddisfatto', 1, 'star'),
(2, 'Insoddisfatto', 2, 'star'),
(3, 'Nella media', 3, 'star'),
(4, 'Soddisfatto', 4, 'star'),
(5, 'Estremamente soddisfatto', 5, 'star');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `relation_category_question`
--
ALTER TABLE `relation_category_question`
  ADD KEY `id_category_rcq` (`id_category_rcq`),
  ADD KEY `id_question_rcq` (`id_question_rcq`);

--
-- Indici per le tabelle `relation_form_survey`
--
ALTER TABLE `relation_form_survey`
  ADD KEY `form_rfs` (`id_form_rfs`),
  ADD KEY `survey_rfs` (`id_survey_rfs`);

--
-- Indici per le tabelle `relation_survey_category`
--
ALTER TABLE `relation_survey_category`
  ADD KEY `survey_rsc` (`id_survey_rsc`),
  ADD KEY `category_src` (`id_category_rsc`);

--
-- Indici per le tabelle `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indici per le tabelle `tbl_form`
--
ALTER TABLE `tbl_form`
  ADD PRIMARY KEY (`id_form`);

--
-- Indici per le tabelle `tbl_form_role`
--
ALTER TABLE `tbl_form_role`
  ADD PRIMARY KEY (`id_form_role`);

--
-- Indici per le tabelle `tbl_history_login`
--
ALTER TABLE `tbl_history_login`
  ADD PRIMARY KEY (`id_history_login`);

--
-- Indici per le tabelle `tbl_language`
--
ALTER TABLE `tbl_language`
  ADD PRIMARY KEY (`id_language`);

--
-- Indici per le tabelle `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`id_login`);

--
-- Indici per le tabelle `tbl_question`
--
ALTER TABLE `tbl_question`
  ADD PRIMARY KEY (`id_question`);

--
-- Indici per le tabelle `tbl_result`
--
ALTER TABLE `tbl_result`
  ADD PRIMARY KEY (`id_result`);

--
-- Indici per le tabelle `tbl_role`
--
ALTER TABLE `tbl_role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indici per le tabelle `tbl_survey`
--
ALTER TABLE `tbl_survey`
  ADD PRIMARY KEY (`id_survey`);

--
-- Indici per le tabelle `tbl_text`
--
ALTER TABLE `tbl_text`
  ADD PRIMARY KEY (`id_text`);

--
-- Indici per le tabelle `tbl_valutation`
--
ALTER TABLE `tbl_valutation`
  ADD PRIMARY KEY (`id_valutation`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `tbl_form`
--
ALTER TABLE `tbl_form`
  MODIFY `id_form` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT per la tabella `tbl_form_role`
--
ALTER TABLE `tbl_form_role`
  MODIFY `id_form_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT per la tabella `tbl_history_login`
--
ALTER TABLE `tbl_history_login`
  MODIFY `id_history_login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT per la tabella `tbl_language`
--
ALTER TABLE `tbl_language`
  MODIFY `id_language` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `id_login` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `tbl_question`
--
ALTER TABLE `tbl_question`
  MODIFY `id_question` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT per la tabella `tbl_result`
--
ALTER TABLE `tbl_result`
  MODIFY `id_result` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT per la tabella `tbl_role`
--
ALTER TABLE `tbl_role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT per la tabella `tbl_survey`
--
ALTER TABLE `tbl_survey`
  MODIFY `id_survey` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `tbl_text`
--
ALTER TABLE `tbl_text`
  MODIFY `id_text` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `tbl_valutation`
--
ALTER TABLE `tbl_valutation`
  MODIFY `id_valutation` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `relation_category_question`
--
ALTER TABLE `relation_category_question`
  ADD CONSTRAINT `id_category_rcq` FOREIGN KEY (`id_category_rcq`) REFERENCES `tbl_category` (`id_category`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `id_question_rcq` FOREIGN KEY (`id_question_rcq`) REFERENCES `tbl_question` (`id_question`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `relation_form_survey`
--
ALTER TABLE `relation_form_survey`
  ADD CONSTRAINT `form_rfs` FOREIGN KEY (`id_form_rfs`) REFERENCES `tbl_form` (`id_form`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `survey_rfs` FOREIGN KEY (`id_survey_rfs`) REFERENCES `tbl_survey` (`id_survey`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limiti per la tabella `relation_survey_category`
--
ALTER TABLE `relation_survey_category`
  ADD CONSTRAINT `category_src` FOREIGN KEY (`id_category_rsc`) REFERENCES `tbl_category` (`id_category`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `survey_rsc` FOREIGN KEY (`id_survey_rsc`) REFERENCES `tbl_survey` (`id_survey`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
