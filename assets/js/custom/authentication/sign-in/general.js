"use strict";
var KTSigninGeneral = function() {
    var t, e, i;
  var form=$("#kt_sign_in_form");
    return {
        init: function() {
            t = document.querySelector("#kt_sign_in_form"), e = document.querySelector("#kt_sign_in_submit"), i = FormValidation.formValidation(t, {
                fields: {
                    email: {
                        validators: {
                            notEmpty: {
                                message: "Indirizzo email richiesto"
                            },
                            emailAddress: {
                                message: "L'email inserita non è valida"
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: "La password è obbligatoria"
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger,
                    bootstrap: new FormValidation.plugins.Bootstrap5({
                        rowSelector: ".fv-row"
                    })
                }
            }), e.addEventListener("click", (function(n) {
                n.preventDefault(), i.validate().then((function(i) {
                    if("Valid" == i){
                     var login_user_value = document.getElementById("login_email").value;
                     var login_password_value = CryptoJS.MD5(document.getElementById("login_password").value).toString();
                     //alert(login_user_value + ' ' + login_password_value);
                    var btnSpinner=$("#kt_sign_in_submit").addClass("spinner spinner-white spinner-right");
                    form.ajaxSubmit({


                        type: "POST",
                        url: './control/check_login_validation.php',
                        data: {"user_login" : login_user_value,
                        "password_login" : login_password_value,},
                        success: function(response, status, xhr, $form) {
                            setTimeout(function(){
                                
                                btnSpinner.removeClass("spinner spinner-white spinner-right");
                                if(response==1) {
                                   
                                    window.location.href = "home-statistics.php";
                                }
                                
                                //account not present
                                if(response==0) {
                                    Swal.fire({
                        text: "Errore: l'account non esiste. Controllare l'email e la passoword e riprovare.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok, chiaro",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                        })
                                }

                                //account disabled
                                if(response<0) {
                                    Swal.fire({
                        text: "L'account è stato disabilitato, contattare gli amministratori per maggiori informazioni.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok, chiaro",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    })
                                }
                             }, 500);
                        }
                    });
                    


                    /*(e.setAttribute("data-kt-indicator", "on"), e.disabled = !0, setTimeout((function() {
                        e.removeAttribute("data-kt-indicator"), e.disabled = !1, Swal.fire({
                            text: "Hai effettuato l'accesso con successo!",
                            icon: "success",
                            buttonsStyling: !1,
                            confirmButtonText: "Ok, ricevuto!",
                            customClass: {
                                confirmButton: "btn btn-primary"
                            }
                        }).then((function(e) {
                            e.isConfirmed && (t.querySelector('[name="email"]').value = "", t.querySelector('[name="password"]').value = "")
                        }))
                    }), 2e3))*/}

                    else { 
                        Swal.fire({
                        text: "E' stato riscontrato un errore durante la sezione di login, si prega di riprovare più tardi.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "Ok, chiaro",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    })}
                }))
            }))
        }
    }
}();
KTUtil.onDOMContentLoaded((function() {
    KTSigninGeneral.init()
}));