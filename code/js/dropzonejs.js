"use strict";
// Class definition

var KTDropzoneDemo = function () {
    // Private functions
    var demo1 = function () {
        // file type validation
        $('#kt_dropzone_cover').dropzone({
            url: HOST_URL + 'code/api/api_upload.php?type=file_cover',
            paramName: "file",
            maxFiles: 1,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            acceptedFiles: "image/*,application/pdf,.doc",
            init: function() {
                this.on("success", function(file, responseText) {
                    $("#file_cover").val(responseText);
                });
                this.on("removedfile", function (file) {
                    $("#file_cover").val('');
                });
            },
            accept: function(file, done) {
                console.log(done);
                if (file.name == "justinbieber.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            }           
        });

        $('#kt_dropzone_cv').dropzone({
            url: HOST_URL + 'code/api/api_upload.php?type=file_cv',
            paramName: "file", 
            maxFiles: 1,
            maxFilesize: 10, // MB
            addRemoveLinks: true,
            acceptedFiles: "image/*,application/pdf,.doc",
            init: function() {
                this.on("success", function(file, responseText) {
                    $("#file_cv").val(responseText);
                    //$("#tell_experience").css("display", "none");
                });
                this.on("removedfile", function (file) {
                    $("#file_cv").val('');
                    //$("#tell_experience").css("display", "");
                });
            },
            accept: function(file, done) {
                if (file.name == "justinbieber.jpg") {
                    done("Naha, you don't.");
                } else {
                    done();
                }
            }
        });
    }    

    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();

KTUtil.ready(function() {
    KTDropzoneDemo.init();
});
