
// Definisco la mia funzione ReplaceAll, mi cambio tutto
function replaceAll(str, cerca, sostituisci) {
    return str.split(cerca).join(sostituisci);
}

//mi prendo solo il numero
function getOnlyNumericFloat(numero) {
    numero=replaceAll(numero, "_","");
    numero=replaceAll(numero, "€","");
    numero=replaceAll(numero, "$","");
    numero=replaceAll(numero, ".","");
    numero=replaceAll(numero, ",",".");
    if(isNaN(parseFloat(numero)))
        numero=0;
    return parseFloat(numero);
}

function dateToYMD(date) {
    var strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var d = date.getDate();
    var m = strArray[date.getMonth()];
    var y = date.getFullYear();
    return '' + (d <= 9 ? '0' + d : d) + '-' + m + '-' + y;
}  