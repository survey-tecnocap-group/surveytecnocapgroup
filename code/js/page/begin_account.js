var idUserSelected="";
var idRoleSelected="";

//mi salvo il file image_profile online
var image_profile="";

$(document).ready(function() {
    $('#kt_select_role_edit, #kt_select_role_validate').select2({
        placeholder: "Select",
        minimumResultsForSearch: Infinity
    });   

    $('#kt_select_area_edit, #kt_formation_area_validate').select2({
        placeholder: "Select",
        maximumSelectionLength: 3
    });    

    $('#kt_select_nation_edit, #kt_select_nation_edit_validate').select2({
        placeholder: "Select",
        minimumResultsForSearch: Infinity
    });

    $('#kt_select_sex_form, #kt_formation_sex_form_validate').select2({
        placeholder: "Select sex",
        minimumResultsForSearch: Infinity
    });

    $('#kt_select_edit_form, #kt_formation_edit_form_validate').select2({
        placeholder: "Select"
    });

    $('#kt_select_nation_form, #kt_formation_nation_form_validate').select2({
        placeholder: "Select country",
        language: {
            noResults: function(term) {
              var txt="\""+event.target.value+"\"";
              let msgNotFindCountry=$("#notFindCountry").val();
              let msgBtnCountry=$("#addCountry").val();
              let type="\"country\"";
              return "<div>"+msgNotFindCountry+"<a href='javascript:addQuickValue("+type+","+txt+");' class='btn btn-light-primary font-weight-bold ml-2'>"+msgBtnCountry+"</a></div>";
            },
        },
        escapeMarkup: function(markup) {	
            return markup;
        },    
    }); 
    
    $('#kt_select_role_area, #kt_formation_role_area_validate').select2({
        placeholder: "Select",
        language: {
            noResults: function(term) {
              var txt="\""+event.target.value+"\"";
              let msgNotFindCountry=$("#notFindCountry").val();
              let msgBtnCountry=$("#addRole").val();
              let type="\"role_area\"";
              return "<div>"+msgNotFindCountry+"<a href='javascript:addQuickValue("+type+","+txt+");' class='btn btn-light-primary font-weight-bold ml-2'>"+msgBtnCountry+"</a></div>";
            },
        },
        escapeMarkup: function(markup) {	
            return markup;
        },    
    }); 

    $('#kt_select_area_form, #kt_formation_area_form_validate').select2({
        placeholder: "Select",
        language: {
            noResults: function(term) {
              var txt="\""+event.target.value+"\"";
              let msgNotFindCountry=$("#notFindCountry").val();
              let msgBtnCountry=$("#addArea").val();
              let type="\"area\"";
              return "<div>"+msgNotFindCountry+"<a href='javascript:addQuickValue("+type+","+txt+");' class='btn btn-light-primary font-weight-bold ml-2'>"+msgBtnCountry+"</a></div>";
            },
        },
        escapeMarkup: function(markup) {	
            return markup;
        }, 
    });

    $("#profile-img").change(function(){
        setTimeout(function(){ 
            getFileCover();    
        }, 500);
    });

    $('.toggle-password').click(function(){
        $('#classEye').toggleClass('fa-eye fa-eye-slash');
        let input = $(this).prev();
        input.attr('type', input.attr('type') === 'password' ? 'text' : 'password');
    });
});

function loadUserDetail(idUser) {
    idRoleSelected="";
    idUserSelected=idUser;

    $.post('./code/function/alg_function_call.php', {
        type: 'get_UserExtraDetail',
        idUser: idUser, 
        }, function(data) {
            let json = JSON.parse(data);
            $("#exampleModalLabelUser").html(json.last_name+" "+json.first_name);
            $("#kt_select_sex").val(json.id_sex).trigger('change');
            $("#kt_select_nation_edit").val(json.id_nazione).trigger('change');
            $("#user_FirstName").val(json.first_name);
            $("#user_LastName").val(json.last_name);
            $("#user_Email").val(json.email);
            $("#user_Phone").val(json.phone);
            $("#modalUserDetails").modal('show');
        }
    );
}

function loadTestimonialDetail(idUser) {
    idUserSelected=idUser;

    $.post('./code/function/alg_function_call.php', {
        type: 'get_TestimonailExtraDetail',
        idUser: idUser, 
        }, function(data) {
            let json = JSON.parse(data);
            json=json[0];

            image_profile=json.image;
            if(json.image)
                $("#kt_user_edit_avatar_edit").css("background-image", "url('assets/media/testimonial/"+json.image+"')");
            $("#exampleModalLabelTestimonial").html(json.nominativo);
            $("#testimonial_fullname").val(json.nominativo);
            $("#testimonial_ruolo").val(json.ruolo);
            $("#testimonial_messaggio").val(json.messaggio);
            $("#testimonial_area").val(json.area);           
            $("#modalTestimonialDetails").modal('show');
        }
    );
}

function loadExtraDetailAccount(idUser, idRole) {
    idUserSelected=idUser;
    idRoleSelected=idRole;

    $.post('./code/function/alg_function_call.php', {
        type: 'get_UserExtraDetail',
        idUser: idUser, 
        }, function(data) {
            let json = JSON.parse(data);
            $("#exampleModalLabel").html(json.last_name+" "+json.first_name);
            $("#kt_select_role_edit").val(json.name).trigger('change');
            
            var arrayArea=json.id_area.split(";;");   
            $("#kt_select_area_edit").val(arrayArea).trigger('change');           
           
            $("#accountPermission").prop('checked', false);
            if(json.write_role==1)
                    $("#accountPermission").prop('checked', true);
            $('#modalExtraDetails').modal('show');
        }
    );
}


function activeAccount(idUser) {   
    var isActive=$("#switch_active_"+idUser);
    var activeUser=0;
    if(isActive.val()!=0) {
        isActive.prop("checked", false);
        isActive.val(0);
        activeUser=0;
    }
    else {
        isActive.prop("checked", true);
        isActive.val(1);
        activeUser=1;
    }
        
    $.post('./code/function/alg_function_call.php', {
    type: 'active_account',
    idUser: idUser, 
    active: activeUser
    }, function(data) {
        if(data==1) {
        }        
    }); 
}

function deleteAccount(idUser, language) {
    idUserSelected=idUser;
    language_Selected=language;
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'delete_account',
        language: language, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, confirm_DeleteJob);   
    });
}

function deleteTestimonial(idUser, language) {
    idUserSelected=idUser;
    language_Selected=language;
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'delete_testimonial',
        language: language, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, confirm_DeleteTestimonial);   
    });
}

function confirm_DeleteTestimonial() {
    $.post('./code/function/alg_function_call.php', {
        type: 'delete_testimonial',
        id_user: idUserSelected 
        }, function(data) {
            if(data==1) {
                $("#row_"+idUserSelected).fadeOut( "slow", function() {
                });
            }        
        }
    ); 
}

function confirm_DeleteJob() {
    $.post('./code/function/alg_function_call.php', {
        type: 'delete_account',
        id_user: idUserSelected 
        }, function(data) {
            if(data==1) {
                $("#row_"+idUserSelected).fadeOut( "slow", function() {
                });
            }        
        }
    ); 
}


function saveExtra() {
    idUser=idUserSelected;
    idRole=idRoleSelected;

    let role=$("#kt_select_role_edit").val();
    let area=$("#kt_select_area_edit").val();
    let permission_write=$("#accountPermission").is(":checked");

    $.post('./code/function/alg_function_call.php', {
        type: 'update_UserExtraDetail',
        id_user: idUser, 
        id_role: idRole,
        detail: {'role':role, 'area':area, 'permission_write':permission_write}
        }, function(data) {
            if(data==1) {
                var txtArea="";
                var data = $('#kt_select_area_edit').select2('data'); 
                data.forEach(function (item, index) { 
                    txtArea+=item.text; 
                    if((data.length>1) && ((index+1)<data.length))
                        txtArea+=" &divide; ";
                })
                $("#txtArea_"+idUser).html(txtArea);
                $("#txtRole_"+idUser).html($("#kt_select_role_edit").select2('data')[0]['text'].toUpperCase());
                $('#modalExtraDetails').modal('hide');
                $("#imageProfile_"+idUser).effect("pulsate", {times:5}, 3000);
            }        
        }
    ); 
}

function saveUser() {    
    idUser=idUserSelected;

    let firstName=$("#user_FirstName").val();
    let lastName=$("#user_LastName").val();
    let email=$("#user_Email").val();
    let phone=$("#user_Phone").val();
    let sex= $("#kt_select_sex").val();
    let nazione=$("#kt_select_nation_edit").val();

    $.post('./code/function/alg_function_call.php', {
        type: 'update_UserDetail',
        id_user: idUser, 
        detail: {'first_name':firstName, 'last_name':lastName, 'email':email, 'phone':phone, 'sex':sex, 'nazione':nazione}
        }, function(data) {
            if(data==1) {
                $("#txtName_"+idUser).html(lastName+" "+firstName);
                $("#txtEmail_"+idUser).html(email);
                $("#txtNazione_"+idUser).html($("#kt_select_nation_edit").select2('data')[0]['text']);
                $("#txtPhone_"+idUser).html(phone);

                $('#modalUserDetails').modal('hide');
                $("#imageProfile_"+idUser).effect("pulsate", {times:5}, 3000);
            }        
        }
    ); 
}

function saveTestimonial() {   
    idUser=idUserSelected;

    //mi controllo se l'immagine è stata cambiata 
    let imageTestimonial=image_profile;
    let nominativo=$("#testimonial_fullname").val();
    let ruolo=$("#testimonial_ruolo").val();
    let messaggio=$("#testimonial_messaggio").val();
    let area=$("#testimonial_area").val();
    
    $.post('./code/function/alg_function_call.php', {
        type: 'update_Testimonial',
        id_user: idUser, 
        detail: {'nominativo':nominativo, 'ruolo':ruolo, 'messaggio':messaggio, 'area':area, 'image':imageTestimonial}
        }, function(data) {
            if(data==1) {
                $("#imageProfile_"+idUser).attr("src", "assets/media/testimonial/"+imageTestimonial);
                $("#imageProfile_"+idUser).attr("style", "width: 100%; height: 100%!important; border-radius: 0.42rem; object-fit:cover; object-position: 50% 50%");

                $("#txtName_"+idUser).html(nominativo);
                $("#txtRuolo_"+idUser).html(ruolo);
                $("#txtArea_"+idUser).html(area);

                $('#modalTestimonialDetails').modal('hide');
                $("#imageProfile_"+idUser).effect("pulsate", {times:5}, 3000);
            }        
        }
    ); 
}

function saveForm() {
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'confirm_new_member',
        language: language, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, confirm_saveForm);   
    });
}

function saveFormTestimonial() {
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'confirm_new_testimonial',
        language: language, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, confirm_saveFormTestimonial);   
    });
}

function confirm_saveFormTestimonial() {
    let nominativo=$("#testimonial_add_nominativo").val();
    if(!(nominativo)) { formNoComplete(); return; }

    let area=$("#testimonial_add_area").val();
    if(!(area)) { formNoComplete(); return; }

    let ruolo=$("#testimonial_add_ruolo").val();
    if(!(ruolo)) { formNoComplete(); return; }

    let messaggio=$("#testimonial_add_messaggio").val();
    if(!(messaggio)) { formNoComplete(); return; }

    var arrayDetail={'nominativo':nominativo,
                     'area':area,
                     'ruolo':ruolo,
                     'messaggio':messaggio,
                     'image':image_profile};
    
    $.post('./code/function/alg_function_call.php', {
        type: 'add_testimonial',
        detail: arrayDetail
        }, function(data) {
                if(data==1) {  
                    claerFormTestimonial();                    
                }        
           }
    ); 
}

function confirm_saveForm(language) {
    let firstName=$("#userForm_firstname").val();
    if(!(firstName)) { formNoComplete(); return; }

    let lastName=$("#userForm_lastname").val();
    if(!(lastName)) { formNoComplete(); return; }

    let id_sex=$("#kt_select_sex_form").val();
    let id_nazione=$("#kt_select_nation_form").val();
    let email=$("#userForm_email").val();
    if(!(email)) { formNoComplete(); return; }

    let phone=$("#userForm_phone").val();
    if(!(phone)) { formNoComplete(); return; }

    let role_area=$("#kt_select_role_area").select2('data')[0]['text'];
    if(!(role_area)) { formNoComplete(); return; }

    let area=$("#kt_select_area_form").val();
    let write_permission=$("#userForm_accountPermission").is(":checked");

    let username=$("#userForm_username").val();
    if(!(username)) { formNoComplete(); return; }

    let password=$("#userForm_password").val();
    if(!(password)) { formNoComplete(); return; }

    let isEnabled=1;

    var arrayDetail={'first_name':firstName,
                     'last_name':lastName,
                     'image_profile':image_profile,
                     'id_sex':id_sex,
                     'id_nazione':id_nazione,
                     'email':email,
                     'phone':phone,
                     'role_area': role_area,
                     'id_area':area,
                     'write_permission':write_permission,
                     'username':username,
                     'password':password,
                     'isEnabled':isEnabled};
    
    $.post('./code/function/alg_function_call.php', {
        type: 'add_user',
        detail: arrayDetail
        }, function(data) {
                if(data==1) {  
                    claerForm();                    
                }        
           }
    ); 
}

function claerFormTestimonial() {
    idUserSelected="";
    idRoleSelected="";
    image_profile="";
    $("#testimonial_add_nominativo").val('');
    $("#testimonial_add_area").val('');
    $("#testimonial_add_ruolo").val('');
    $("#testimonial_add_messaggio").val('');

    $("#divMember").css("display", "none");
    $("#divFilter").css("display", "block");
    $("#divListMember").css("display", "block");

    setTimeout(function(){ location.reload(); }, 250);
}

function claerForm() {
    idUserSelected="";
    idRoleSelected="";
    image_profile="";
    $("#userForm_firstname").val('');
    $("#userForm_lastname").val('');
    $("#userForm_email").val('');
    $("#userForm_phone").val('');
    $("#userForm_accountPermission").prop("checked", false);
    $("#userForm_username").val('');
    $("#userForm_password").val('');

    $("#divMember").css("display", "none");
    $("#divFilter").css("display", "block");
    $("#divListMember").css("display", "block");

    setTimeout(function(){ location.reload(); }, 250);
}

function formNoComplete() {
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'form_no_complete',
        language: language, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, '');   
    });
}

function resetImageProfile() {
    image_profile="";
}

function getFileCover(){
    var time_unique=Date.now();
    var dataURL=($(".image-input-wrapper").css('background-image'));
        
    $.post('./code/save_image.php', {
        imgBase64: dataURL, 
        type: 'add_careers',
        time_unique: time_unique
    }, function(data) {
        var filename=data.split(";;");
        image_profile=filename[1];
   });
}

function findMember() {
    var find="";
    let id_area=$("#kt_select_area").val().trim();
    if(id_area)
        find="?area="+id_area;

    let id_country=$("#kt_select_nation").val().trim(); 
    if(id_country) {
        if(find)
            find+="&country="+id_country;
        else
            find="?country="+id_country;
    }
    window.location.href="account.php"+find;
}

function addNewMember() {
    $("#divFilter").css("display", "none");
    $("#divListMember").css("display", "none");
    $("#divMember").css("display", "block");
}

