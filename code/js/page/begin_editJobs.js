

var idJob_Selected;
var language_Selected;
var typeQuickSelection;
var itemSelect="";

"use strict";

// Class definition
var KTUserEdit = function () {
	// Base elements
	var avatar;

	var initUserForm = function() {
		avatar = new KTImageInput('kt_job_edit_avatar_edit');
	}

	return {
		// public functions
		init: function() {
			initUserForm();
		}
	};
}();

jQuery(document).ready(function() {
	KTUserEdit.init();
});

function setCookieLanguage(language) {
    document.cookie = "tecnocap-careers="+language;
    location.reload();
}

function resetImageJob() {
    image_profile="";
    let resetImage="assets/media/users/blank_job_hd.jpg";
    $("#kt_job_edit_avatar_edit").css("background-image", "url('"+resetImage+"')");
    $("#removeIconEditJob").css("display", "none");
}

function previewJobs() {
    let jobTitle=$("#job_title").val();
    let time_unique="preview_"+$("#time_unique").val();
    let jobContent=document.querySelector(".ql-editor").innerHTML;

    sessionStorage.setItem("preview_name", time_unique);
    sessionStorage.setItem("preview_title", jobTitle);
    sessionStorage.setItem("preview_content", jobContent);

    window.open("preview.php");
}

function getHTMLFromContent() {
    let idJob=$('#job_idJob').val();
    let language=$("#job_language").val();
    var time_unique=$("#time_unique").val();
    var contentHTML=document.querySelector(".ql-editor").innerHTML;
    $.post('./code/writeContentToFile.php', {
        get_contentHTML: contentHTML,
        id_job: time_unique,
        get_language: language, 
        type: "write"
    }, function( data ) {
        //alert( "Data Loaded: " + data );
    });
}

function getContentFromFile(idJob, language) {
    var time_unique=$("#time_unique").val();
    $.post('./code/writeContentToFile.php', {
        id_job: time_unique,
        get_language: language, 
        type: "read"
    }, function( data ) {
        document.querySelector(".ql-editor").innerHTML=data;
    });
}

function editJob(idJob, language) {
    idJob_Selected=idJob;
    language_Selected=language;

    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'edit_job',
        language: language, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, confirmEditJob);   
    });
}

function addQuickValue(type, txt) {
    typeQuickSelection=type;

    switch(type) {
        case "country":
            typeCountry();
            break;
        
        case "role":
            typeRole();
            break;

        case "role_area":
            typeRoleArea();
            break;
        
        case "formation":
            typeFormation();
            break;
        
        case "contract":
            typeContract();
            break;
        
        case "area":
            typeArea();
            break;
    }

    getValueForType(type);
   
    $("#inputAdded").val(txt);
    $('#exampleModalSizeDefault').modal('show');
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/*
function createDynamicMenu (state) {
    if (!state.id) {
      return state.text;
    }
    var $state = $(
      '<div onmouseover=showEditMenu("'+state.id+'")>' +
      '<span>' + state.text + '</span>' + 
      '<div id="menu_'+state.id+'" class="btn-group ml-3" role="group" aria-label="First group" style="display:none">' +
	        '<button type="button" class="btn btn-light-primary btn-sm btn-icon">' +
				'<i class="la la-edit"></i>' +
			'</button>' +			
       '</div>' +
       '</div>'
    );

    return $state;
};

//mi registro il menù precedente visualizzato lo nascondo per il prossimo HOVER
var beforeMenu="";
function showEditMenu(id) {
    if(beforeMenu)
        $("#menu_"+beforeMenu).css("display","none");
    
    beforeMenu=id;
    $("#menu_"+id).css("display","");
}
*/

//controllo ad ogni input se il valore è NULL
if (typeof inputAdded !== 'undefined') {
    inputAdded.oninput = function() {
    if(inputAdded.value=="")
        cancelEdit()
    };
}

function unselectOption() {
    if($("#inputAdded").val()=="") {
        $("#kt_select_edit_form").val(null).trigger("change");
        $("#inputAdded").val('');
        itemSelect="";
    }
}

var idSelectedDelete="";
$('#kt_select_edit_form').on('select2:select', function (e) {
    var data = e.params.data;
    idSelectedDelete=data.id;
    

    $("#modalBtnEditDelete").css("display", "none");
    canDeleteSelected();

    showEdit(1);
    $("#inputAdded").val(data.text);
    itemSelect=data.id;
});

function deleteEditSelected() {    
    $.post('./code/function/alg_function_call.php', {
        type: "delete_SettingSelected",
        id: idSelectedDelete,
        choose: typeQuickSelection
    }, function(data) {
        $('#exampleModalSizeDefault').modal('hide');
    });

}

function canDeleteSelected() {
    $.post('./code/function/alg_function_call.php', {
        type: "checkCanDelete_SettingSelected",
        id: idSelectedDelete,
        choose: typeQuickSelection
    }, function(data) {        
        if(data.trim()==0)
            $("#modalBtnEditDelete").css("display", "");
    });

}

function showEdit(isEdit) {
    var txtTitle=($("#modalTitle").html());

    if(isEdit) {
        $("#divSave").hide();
        $("#divEdit").show();
        $("#divSelect2").hide();
        txtTitle=txtTitle.replace($("#title_add_new").val(),$("#msgEdit").val());
        $("#modalTitle").html(txtTitle);
    }
    else {
        $("#divSave").show();
        $("#divEdit").hide();
        $("#divSelect2").show();
        $("#inputAdded").val('');
        $("#modalTitle").html();
        txtTitle=txtTitle.replace($("#msgEdit").val(),$("#title_add_new").val());
        $("#modalTitle").html(txtTitle);
        unselectOption();
    }
}

function cancelEdit() {
    showEdit(0);
}

function getValueForType(type) {
    showEdit(0);
    $("#kt_select_edit_form").empty();    

    $.post('./code/function/alg_function_call.php', {
        type: "get_ListTypeForm",
        choose:type
    }, function( data ) {
        var json=JSON.parse(data);
        //$("#kt_select_edit_form").append("<option value='&nbsp;'>&nbsp;</option>");
        if(type=="area")
            $("#kt_select_edit_form").append("<option value='-1'>- all -</option>");

        json.forEach(element => {
            if(type=="country")
                $("#kt_select_edit_form").append("<option value="+element.id_nazione+">"+capitalizeFirstLetter(element.name)+"</option>");

            if(type=="role")
                $("#kt_select_edit_form").append("<option value="+element.id_job_type+">"+capitalizeFirstLetter(element.name)+"</option>");

            if(type=="formation")
                $("#kt_select_edit_form").append("<option value="+element.id_formazione+">"+capitalizeFirstLetter(element.name)+"</option>");
            
            if(type=="contract")
                $("#kt_select_edit_form").append("<option value="+element.id_contract+">"+capitalizeFirstLetter(element.name)+"</option>");
            
            if(type=="area") {
                $("#kt_select_edit_form").append("<option value="+element.id_area+">"+capitalizeFirstLetter(element.name)+"</option>");
            }
        });
        unselectOption();
    });
}

function typeCountry() {
    let titleMsg=$("#title_add_new").val()+" "+$("#nation").val();
    $("#txtType").html($("#nation").val());
    $("#modalBtnSave").html($("#msgAdd").val());
    $("#modalTitle").html(titleMsg);
    $("#kt_select_nation").select2('close');
    $("#kt_select_nation_form").select2('close');
}

function typeRoleArea() {
    let titleMsg=$("#title_add_new").val()+" "+$("#roleArea").val();
    $("#txtType").html($("#roleArea").val());
    $("#modalBtnSave").html($("#msgAdd").val());
    $("#modalTitle").html(titleMsg);
    $("#kt_select_role_area").select2('close');
}

function typeRole() {
    let titleMsg=$("#title_add_new").val()+" "+$("#role").val();
    $("#txtType").html($("#role").val());
    $("#modalBtnSave").html($("#msgAdd").val());
    $("#modalTitle").html(titleMsg);
    $("#kt_select_role").select2('close');
}

function typeFormation() {
    let titleMsg=$("#title_add_new").val()+" "+$("#formation").val();
    $("#txtType").html($("#formation").val());
    $("#modalBtnSave").html($("#msgAdd").val());
    $("#modalTitle").html(titleMsg);
    $("#kt_select_formation").select2('close');
}

function typeArea() {
    let titleMsg=$("#title_add_new").val()+" "+$("#area").val();
    $("#txtType").html($("#area").val());
    $("#modalBtnSave").html($("#msgAdd").val());
    $("#modalTitle").html(titleMsg);
    $("#kt_select_formation").select2('close');
    $("#kt_select_area_form").select2('close');
}

function typeContract() {
    let titleMsg=$("#title_add_new").val()+" "+$("#contract").val();
    $("#txtType").html($("#contract").val());
    $("#modalBtnSave").html($("#msgAdd").val());
    $("#modalTitle").html(titleMsg);
    $("#kt_select_contract").select2('close');
}

function updateEdit() {
    let inputValue=$("#inputAdded").val();    
    var arrayDetail={'id_selected':itemSelect, 'type':typeQuickSelection, 'value':inputValue};
    updateConfirm(arrayDetail);    
}

function updateConfirm(arrayDetail) {    
    $.post('./code/function/alg_function_call.php', {
        type: 'update_ListTypeForm',
        detail: arrayDetail
    }, function( data ) {
        var idValue=data.trim();  
        if(idValue==1) {
            //mi aggiorno l'elenco nel Select2
            getValueForType(arrayDetail['type']);

            $.post('./code/function/alg_function_sweetalert.php', {
                type: 'update_quick_edit',
                language: language, 
            }, function( data ) {
                $('#exampleModalSizeDefault').modal('hide');

                detailMessage=JSON.parse(data);
                msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, confirm_DeleteJob);   
            });
        }
    });
}

function saveNewAdd() {
    let inputAdded=$("#inputAdded").val();
    switch(typeQuickSelection) {
        case "country":
            saveConfirm('add_country', inputAdded);           
            break;
        
        case "role":
            saveConfirm('add_role', inputAdded);
            break;
        
        case "role_area":
            saveConfirm('add_role_area', inputAdded);
            break;
        
        case "formation":
            saveConfirm('add_formation', inputAdded);
            break;
        
        case "contract":
            saveConfirm('add_contract', inputAdded);
            break;
        
        case "area":
            saveConfirm('add_area', inputAdded);
            break;
    }       
}

function saveConfirm(type, inputAdded) {
    if(type=='add_role_area') {
        let valueInput=inputAdded.replace(" ", "-");
        $("#kt_select_role_area").append("<option value="+valueInput+">"+inputAdded+"</option>");
        $("#kt_select_role_area").val(valueInput);
        $('#exampleModalSizeDefault').modal('hide');
        return;
    }

    $.post('./code/function/alg_function_call.php', {
        type: type,
        value: inputAdded,
    }, function( data ) {
        var idValue=data.trim();
        if(type=='add_country') {
            $("#kt_select_nation").append("<option value="+idValue+">"+inputAdded+"</option>");
            $("#kt_select_nation").val(idValue);

            $("#kt_select_nation_form").append("<option value="+idValue+">"+inputAdded+"</option>");
            $("#kt_select_nation_form").val(idValue);
        }
        if(type=='add_role') {
            $("#kt_select_role").append("<option value="+idValue+">"+inputAdded+"</option>");
            $("#kt_select_role").val(idValue);
        }        
        if(type=='add_formation') {
            $("#kt_select_formation").append("<option value="+idValue+">"+inputAdded+"</option>");
            $("#kt_select_formation").val(idValue);
        }

        if(type=='add_contract') {
            $("#kt_select_contract").append("<option value="+idValue+">"+inputAdded+"</option>");
            $("#kt_select_contract").val(idValue);
        }

        if(type=='add_area') {
            $("#kt_select_area").append("<option value="+idValue+">"+inputAdded+"</option>");
            $("#kt_select_area").val(idValue);

            $("#kt_select_area_form").append("<option value="+idValue+">"+inputAdded+"</option>");
            $("#kt_select_area_form").val(idValue);
        }
        $('#exampleModalSizeDefault').modal('hide');
        return idValue;
    });
}
   
function confirmEditJob() {
    //mi carico l'immagine iniziale del job
    image_profile=($("#kt_job_edit_avatar_edit").attr("load_image"));
    $("#image_cover_edit").css("display", "");

    $("#div_ListaCareersForJob").fadeOut('fast', function(){
        $('#formEditJob').load("./edit-jobs-form.php", {'idJob':idJob_Selected}, function(response, status, xhr) {
            if (status == "success") { 
                $("#job_btnEdit").css("display","none");
                $("#job_btnDelete").css("display","none");
                $("#job_btnClose").css("display", "block");
                $("#job_btnUpdate").css("display", "block");
                $("#formEditJob").fadeIn();
                getContentFromFile(idJob_Selected, language_Selected);
                /*
                setTimeout(function(){ 
                    //$("#div_innerFormModule").remove(); 
                }, 5000);
                */
            }			
        });
    });
}

//mi salvo il file image_profile online
var image_profile="";

$("#profile-img_edit").change(function(){
    setTimeout(function(){ 
        $("#removeIconEditJob").css("display", "");
        getFileCoverEditJob();    
    }, 500);
});


function getFileCoverEditJob(){
    var time_unique=Date.now();
    var dataURL=($("#image_edit_cover_job").css('background-image'));
    
    $.post('./code/save_image.php', {
        imgBase64: dataURL, 
        type: 'job',
        time_unique: time_unique
    }, function(data) {
        console.log(data);
        var filename=data.split(";;");
        image_profile=filename[1];
    });
}

function showUserDetails(idCareers, idJob) {
    $("#btn_details_"+idCareers).addClass('spinner spinner-white spinner-right');
    
    $("#div_ListaCareersForJob").fadeOut('fast', function(){
        $("#formShowUserDetails").load( "./user-details.php", { id_careers: idCareers, id_job: idJob }, function(response, status, xhr) {
            $("#formShowUserDetails").fadeIn('slow');
            $("#btn_details_"+idCareers).removeClass('spinner spinner-white spinner-right');
          });
    });
}

function closeUserDetails(idCareers) {   
    $("#formShowUserDetails").fadeOut('fast', function(){
        $("#div_ListaCareersForJob").fadeIn('slow');
    });
}


function closeEditJob() {
    $("#job_btnEdit").css("display","block");
    $("#job_btnDelete").css("display","block");
    $("#job_btnUpdate").css("display", "none");
    $("#job_btnClose").css("display", "none");
    $("#formEditJob").fadeOut('fast', function(){
        $("#div_ListaCareersForJob").fadeIn('slow');
    });
}

function refreshJobInformation() {
    KTApp.block('#kt_blockui_card', {
        overlayColor: '#000000',
        state: 'primary',
        message: $("#job_update_progress").val()
    });
}

function formatForEuro(value) {
    const formatter = new Intl.NumberFormat('it-IT', {
        style: 'currency',
        currency: 'EUR'
      })
      return formatter.format(value).replace("€","");
}

function formatForDollar(value) {
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 2
      });

      return formatter.format(value).replace("$","");
}

function closeRefresJobInformation(arrayDetail) {    
    $("#txt_JobTitle").html(arrayDetail['job_title']);
    $("#txt_JobCity").html(arrayDetail['job_city']);
    $("#txt_JobShortDescription").html(arrayDetail['job_short_description'].substring(0,60)+"<br>"+arrayDetail['job_short_description'].substring(60,120)+"...");
    $("#txt_JobContract").html(arrayDetail['job_contract_name']);
    $("#txt_JobBudgetValuta").html(arrayDetail['job_valuta_name']);
    if(arrayDetail['job_valuta_name']=="$")
        $("#txt_JobBudget").html(formatForDollar(arrayDetail['job_budget']));
    else if(arrayDetail['job_valuta_name']=="€")
        $("#txt_JobBudget").html(formatForEuro(arrayDetail['job_budget']));
    else {
        $("#txt_JobBudgetValuta").html('');
        $("#txt_JobBudget").html(arrayDetail['job_budget']);
    }

    $("#txt_JobDataStart").html(moment(arrayDetail['job_dataStart']).format("DD MMM, YY"));
    $("#txt_JobDataEnd").html(moment(arrayDetail['job_dataEnd']).format("DD MMM, YY"));

    setTimeout(function() {
        KTApp.unblock('#kt_blockui_card');
    }, 2000);
}

function deleteJob(idJob, language) {
    idJob_Selected=idJob;
    language_Selected=language;
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'delete_job',
        language: language 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, confirm_DeleteJob_Selected); 
    });
}

function confirm_DeleteJob_Selected() {
    $.post('./code/function/alg_function_call.php', {
        type: 'delete_job',
        id_job: idJob_Selected
    }, function( data ) {
        window.location.href = "list-jobs.php";
    });
}

function confirm_UpdateJob() {
    refreshJobInformation();

    let job_title=$("#job_title").val();
    let job_short_description=$("#job_short_description").val();
    let job_filename=$("#time_unique").val();
    let job_area=$("#kt_select_area").val();
    let job_city=$("#job_city").val();
    let job_nazione=$("#kt_select_nation").val();
    let job_role=$("#kt_select_role").val();
    let job_formazione=$("#kt_select_formation").val();
    let job_contract=$("#kt_select_contract").val();
    let job_contract_name=$("#kt_select_contract").text().trim();
    let job_valuta= $('#job_valuta').attr("value");
    let job_valuta_name= $('#job_valuta').text();
    let job_budget=$("#kt_input_budget").val();    
    if(job_valuta_name.toLowerCase()!="alphanumeric")
        job_budget=getOnlyNumericFloat(job_budget);

    let job_dataStart=$("#job_start_date").val();
    let job_dataEnd=$("#job_end_date").val();
    let job_notify_all_area=$("#notify_all_area").is(':checked');
    let job_notify_admin=$("#notify_admin").is(':checked');
    let job_note=$("#exampleTextarea").val();
    let job_status=$("#get_StatusJob").val();
    //alert(job_status);
   
    var arrayDetail={'job_image': image_profile,
                     'job_status':job_status,
                     'job_title':job_title,
                     'job_short_description':job_short_description,
                     'job_filename':job_filename,
                     'job_area':job_area,
                     'job_city':job_city,
                     'job_nazione':job_nazione,
                     'job_role':job_role,
                     'job_formazione':job_formazione,
                     'job_contract':job_contract,
                     'job_contract_name':job_contract_name,
                     'job_valuta':job_valuta,
                     'job_valuta_name':job_valuta_name,
                     'job_budget':job_budget,
                     'job_dataStart':job_dataStart,
                     'job_dataEnd':job_dataEnd,
                     'job_notify_all_area':job_notify_all_area,
                     'job_notify_admin':job_notify_admin,
                     'job_note':job_note};

    $.post('./code/function/alg_function_call.php', {
        type: 'update_job',
        idJob: idJob_Selected,
        language: language_Selected,
        detail: arrayDetail 
        }, function( data ) {
            var returnArray=data.split(";;");
            if(returnArray[0].trim()==1) {
                $("#image_cover_edit").css("display", "none");
                $("#preview_image_job").attr("src", "assets/media/project-logos/"+image_profile);
                updateJobSuccess();

                //mi aggiorno la progress
                if(returnArray[1]>0) {
                    $("#progressBar_date").css("display","block");
                    $("#progressBar_message_date").css("display","none");
                }
                $("#job_progressbar").css("width", returnArray[1]+"%");
                $("#job_txt_progressbar").html(returnArray[1]+"%");

                $("#job_btnEdit").css("display","block");
                $("#job_btnDelete").css("display","block");
                $("#job_btnUpdate").css("display", "none");
                $("#job_btnClose").css("display", "none");
                $("#formEditJob").fadeOut('fast', function(){
                    $("#div_ListaCareersForJob").fadeIn('slow');
                        closeRefresJobInformation(arrayDetail);
                });
            }
            else {
                setTimeout(function() {
                    KTApp.unblock('#kt_blockui_card');
                }, 500);            
                errorUpdateJob();
            }
    });
}

function updateJobSuccess() {
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'update_job_success',
        language: language_Selected, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, '');   
    });
}

function errorUpdateJob() {
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'error_update_job',
        language: language_Selected, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, '');   
    });
}

function updateJob(idJob, language) {   
    idJob_Selected=idJob;
    language_Selected=language;

    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'update_job',
        language: language, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, confirm_UpdateJob);   
    });
}

function changeStateCarrers(idCareers, idJob, idUser){
    var checkbox_careers=$("#state_careers_"+idCareers);
    if(checkbox_careers.val()!="false") {
        checkbox_careers.prop("checked", false);
        checkbox_careers.val(false);
        $("#ribbon_careers_"+idCareers).css("display","none");
        activeCarrers(false, idCareers, idJob, idUser);
        return;
    }
    checkbox_careers.prop("checked", true);
    checkbox_careers.val(true);        
    $("#ribbon_careers_"+idCareers).css("display","block");
    activeCarrers(true, idCareers, idJob, idUser);
    return;
}

function showDataNascita(dataNascita, idSpan) {
    var txtSpanOld=$("#"+idSpan).html();
    $("#"+idSpan).html(dataNascita);
    $("#"+idSpan).effect( "pulsate", {times:3}, 3000, function(){
        setTimeout(function(){ 
            $("#"+idSpan).fadeOut('slow', function() {
                $("#"+idSpan).html(txtSpanOld);
                $("#"+idSpan).fadeIn('slow');
            });           
         }, 3000);
    });
}


function activeCarrers(isActive, idCareers, idJob, idUser) {
    if(isActive==true) {
        isActive=1;
        approvedCareers(idJob, idCareers, idUser);
    }
    else {
        isActive=0;
        removeApprovedCareers(idJob, idCareers);
    }
}

function approvedCareers(idJob, idCareers, idUser) {
    $('#bodyHidden').load("./code/function/alg_function_call.php", {'type':'approvedCareers', 'idJob':idJob, 'idCareers':idCareers, 'idUser':idUser}, function(response, status, xhr) {
        if (status == "success") { 
        }			
    });
}

function removeApprovedCareers(idJob, idCareers) {
    $('#bodyHidden').load("./code/function/alg_function_call.php", {'type':'removeApprovedCareers', 'idJob':idJob, 'idCareers':idCareers}, function(response, status, xhr) {
        if (status == "success") { 
        }			
    });
}