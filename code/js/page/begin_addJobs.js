
let language=$("#job_language").val();
let time_unique=$("#time_unique").val();
let idUser=$("#job_id_user").val();

$(document).ready(function() {
    loadEditForm();
});

function loadEditForm() {
    $('#formEditJob').load("./edit-jobs-form.php", {'isAdd':1}, function(response, status, xhr) {
        if (status == "success") { 
        }			
    });
}

//mi salvo il file online
function getFileCoverAddJob(){
    var dataURL=($(".image-input-wrapper").css('background-image'));
    
    $.post('./code/save_image.php', {
        imgBase64: dataURL, 
        type: 'job',
        time_unique: time_unique
    }, function(data) {
    });
}

function checkIfAddedImage(value) {
    $("#check_image_profile").val(value);
}

function getDetailInputJob() {
    if((!(document.querySelector(".ql-editor").innerHTML)) || (document.querySelector(".ql-editor").innerHTML=="<p><br></p>")) { formNoComplete(); return; }

    let job_title=$("#job_title").val();
    if(!(job_title)) { formNoComplete(); return; }

    let job_short_description=$("#job_short_description").val();
    if(!(job_short_description)) { formNoComplete(); return; }

    let job_area=$("#kt_select_area").val();
    let job_city=$("#job_city").val();
    if(!(job_city)) { formNoComplete(); return; }
    

    let job_nazione=$("#kt_select_nation").val();
    let job_role=$("#kt_select_role").val();
    let job_formazione=$("#kt_select_formation").val();
    let job_contract=$("#kt_select_contract").val();
    let job_contract_name=$("#kt_select_contract").text().trim();
    let job_valuta= $('#job_valuta').attr("value");
    let job_valuta_name= $('#job_valuta').text();
    let job_budget=$("#kt_input_budget").val();    
    if(job_valuta_name.toLowerCase()!="alphanumeric")
        job_budget=getOnlyNumericFloat(job_budget);
    let job_dataStart=$("#job_start_date").val();
    let job_dataEnd=$("#job_end_date").val();
    let job_notify_all_area=$("#notify_all_area").is(':checked');
    let job_notify_admin=$("#notify_admin").is(':checked');
    let job_note=$("#exampleTextarea").val();
    let job_status=$("#get_StatusJob").val();

    let check_image_profile=$("#check_image_profile").val();
   
    var arrayDetail={'time_unique':time_unique,
                     'check_image_profile': check_image_profile,
                     'job_id_user': idUser,
                     'job_status': job_status,
                     'job_title':job_title,
                     'job_short_description':job_short_description,
                     'job_area':job_area,
                     'job_city':job_city,
                     'job_nazione':job_nazione,
                     'job_role':job_role,
                     'job_formazione':job_formazione,
                     'job_contract':job_contract,
                     'job_contract_name':job_contract_name,
                     'job_valuta':job_valuta,
                     'job_valuta_name':job_valuta_name,
                     'job_budget':job_budget,
                     'job_dataStart':job_dataStart,
                     'job_dataEnd':job_dataEnd,
                     'job_notify_all_area':job_notify_all_area,
                     'job_notify_admin':job_notify_admin,
                     'job_note':job_note};
    
    addJob(arrayDetail);
}

function addJob(arrayDetail) {
    $.post('./code/function/alg_function_call.php', {
        type: 'add_job',
        language: language,
        detail: arrayDetail 
        }, function(data) {
            console.log(data);
            if(data==1) {
                getHTMLFromContent();
                getFileCoverAddJob();
                addJobSuccess();
                confirmResetInput();
            }        
        }
    );   
}

function addJobSuccess() {
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'add_job_success',
        language: language, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, '');   
    });
}

function formNoComplete() {
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'form_no_complete',
        language: language, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, '');   
    });
}

function resetInput() {
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'confirm_cancel',
        language: language, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, confirmResetInput);   
    });
}

function confirmResetInput() {
   $("#btn_remove_image").click();

    $("#job_title").val('');
    $("#job_short_description").val('');
    $("#job_city").val('');
    $("#kt_input_budget").val('');
    $("#job_start_date").val('');
    $("#job_end_date").val('');
    $("#notify_all_area").prop('checked',false);
    $("#notify_admin").prop("checked",false);
    $("#exampleTextarea").val('');
    document.querySelector(".ql-editor").innerHTML="";
}
