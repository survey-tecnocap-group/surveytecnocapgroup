
let language=$("#language").val();

function changeStateJobs(idJob){
    var checkbox_job=$("#state_jobs_"+idJob);
    if(checkbox_job.val()!="false") {
        checkbox_job.prop("checked", false);
        checkbox_job.val(false);
        $("#overlay_disable_"+idJob).fadeIn();
        $("#ribbon_"+idJob).css("display","block");
        activeJob(idJob, false);
        return;
    }
    checkbox_job.prop("checked", true);
    checkbox_job.val(true);        
    $("#overlay_disable_"+idJob).fadeOut();
    $("#ribbon_"+idJob).css("display","none");
    activeJob(idJob,true);
    return;
}


function activeJob(idJob, isActive) {
    if(isActive==true)
        isActive=1;
    else
        isActive=0;

    $('#bodyHidden').load("./code/function/alg_function_call.php", {'type':'activeJob', 'idJob':idJob, 'is_active':isActive}, function(response, status, xhr) {
        if (status == "success") { 
        }			
    });	
}

function loadPageNumber(pagNumber) {
    $("#numberPage").val(pagNumber);
    $("#loading_page").fadeIn();
    setTimeout(function(){ 
        $("#reload_form").submit();
     }, 500);
}

function loadItemForPage() {
    itemForPage=$("#selectItemForPage").val();
    
    $("#numberPage").val('1');
    $("#numberItemForPage").val(itemForPage);
    $("#loading_page").fadeIn();
    setTimeout(function(){ 
        $("#reload_form").submit();
     }, 500);
     
}

function showFilter() {
    if($("#divFilter").is(":visible"))
        $("#divFilter").hide();
    else
        $("#divFilter").show();
}

function findListJob() {
    $("#findSpinner").addClass('spinner spinner-white spinner-right');
    setTimeout(function(){ 
        queryFind();
    }, 250);
}

function queryFind() {
    var find="";
    let id_area=$("#kt_select_area").val().trim();
    if(id_area)
        find="?area="+id_area;

    let id_country=$("#kt_select_nation").val().trim(); 
    if(id_country) {
        if(find)
            find+="&country="+id_country;
        else
            find="?country="+id_country;
    }
    window.location.href="list-jobs.php"+find;
}

function searchLocationRole(pagNumber) {
    let numberItemForPage=$("#selectItemForPage").val();        
    let searchArea=$("#kt_select_area").val().trim();
    let searchRole=$("#kt_select_role").val().trim();
    $("#btn_SearchNow").addClass('spinner spinner-white spinner-right');
    setTimeout(function(){ 
        var newURL = window.location.protocol + "//" + window.location.host + window.location.pathname;
        var haveInitSearch=0;
        if(searchArea) {
            newURL+="?location="+searchArea;
            haveInitSearch=1;
        }
        if(searchRole) {
            if(haveInitSearch)
                newURL+="&";
            else {
                haveInitSearch=1;
                newURL+="?";
            }
            newURL+="role="+searchRole;
        }

        if(haveInitSearch)
            newURL+="&";
        else {
            haveInitSearch=1;
            newURL+="?";
        }

        newURL+="page="+pagNumber;

        if(numberItemForPage>0)
            newURL+="&item="+numberItemForPage;
        
        window.location.href=(newURL);
    }, 250);

}