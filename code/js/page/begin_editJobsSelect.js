
$(document).ready(function() {
    $('#kt_select_role, #kt_select_role_validate').select2({
        placeholder: "Select role",
        language: {
            noResults: function(term) {
              var txt="\""+event.target.value+"\"";
              let msgNotFindCountry=$("#notFindCountry").val();
              let msgBtnCountry=$("#addRole").val();
              let type="\"role\"";
              return "<div>"+msgNotFindCountry+"<a href='javascript:addQuickValue("+type+","+txt+");' class='btn btn-light-primary font-weight-bold ml-2'>"+msgBtnCountry+"</a></div>";
            },
        },
        escapeMarkup: function(markup) {	
            return markup;
        },    
    });

    $('#kt_select_formation, #kt_formation_formation_validate').select2({
        placeholder: "Select formation",
        language: {
            noResults: function(term) {
              var txt="\""+event.target.value+"\"";
              let msgNotFindCountry=$("#notFindCountry").val();
              let msgBtnCountry=$("#addFormation").val();
              let type="\"formation\"";
              return "<div>"+msgNotFindCountry+"<a href='javascript:addQuickValue("+type+","+txt+");' class='btn btn-light-primary font-weight-bold ml-2'>"+msgBtnCountry+"</a></div>";
            },
        },
        escapeMarkup: function(markup) {	
            return markup;
        },    
    });

    $('#kt_select_contract, #kt_formation_contract_validate').select2({
        placeholder: "Select contract",
        language: {
            noResults: function(term) {
              var txt="\""+event.target.value+"\"";
              let msgNotFindCountry=$("#notFindCountry").val();
              let msgBtnCountry=$("#addContract").val();
              let type="\"contract\"";
              return "<div>"+msgNotFindCountry+"<a href='javascript:addQuickValue("+type+","+txt+");' class='btn btn-light-primary font-weight-bold ml-2'>"+msgBtnCountry+"</a></div>";
            },
        },
        escapeMarkup: function(markup) {	
            return markup;
        },  
    });

    $('#kt_select_area, #kt_formation_area_validate').select2({
        placeholder: "Select area",
        language: {
            noResults: function(term) {
              var txt="\""+event.target.value+"\"";
              let msgNotFindCountry=$("#notFindCountry").val();
              let msgBtnCountry=$("#addArea").val();
              let type="\"area\"";
              return "<div>"+msgNotFindCountry+"<a href='javascript:addQuickValue("+type+","+txt+");' class='btn btn-light-primary font-weight-bold ml-2'>"+msgBtnCountry+"</a></div>";
            },
        },
        escapeMarkup: function(markup) {	
            return markup;
        }, 
    });

    $('#kt_select_nation, #kt_formation_nation_validate').select2({
        placeholder: "Select country",
        language: {
            noResults: function(term) {
              var txt="\""+event.target.value+"\"";
              let msgNotFindCountry=$("#notFindCountry").val();
              let msgBtnCountry=$("#addCountry").val();
              let type="\"country\"";
              return "<div>"+msgNotFindCountry+"<a href='javascript:addQuickValue("+type+","+txt+");' class='btn btn-light-primary font-weight-bold ml-2'>"+msgBtnCountry+"</a></div>";
            },
        },
        escapeMarkup: function(markup) {	
            return markup;
        },    
    });    

    $('#kt_select_sex, #kt_formation_sex_validate').select2({
        placeholder: "Select sex",
        minimumResultsForSearch: Infinity
    });

    // currency format
    $("#kt_input_budget").inputmask('999.999', {
        numericInput: true
    }); //123456  =>  € ___.__1.234,56

    //mi rimuovo il pulsante formattin text dell'editor
    $(".ql-clean").remove();
});

function selectValuta(valuta, idValuta) {
    $('#job_valuta').attr("value", idValuta);
    $('#job_valuta').html(valuta);
    let strValuta=valuta;
    if(strValuta.toLowerCase()=="alphanumeric")
        formatValutaAlphaNumeric('');
    else
        formatValutaNumeric('');
}

function formatValutaAlphaNumeric(initValue) {
    $("#kt_input_budget").inputmask('', {
        numericInput: false
    });
    $("#kt_input_budget").val(initValue);
}

function formatValutaNumeric(initValue) {
    $("#kt_input_budget").inputmask('999.999', {
        numericInput: true
    });
    $("#kt_input_budget").val(initValue);

}