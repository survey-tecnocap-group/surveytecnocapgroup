
$(document).ready(function() {
    $('#kt_select_role, #kt_select_role_validate').select2({
        placeholder: "Select role",
        minimumResultsForSearch: Infinity
    });

    $('#kt_select_formation, #kt_formation_formation_validate').select2({
        placeholder: "Select formation",
        minimumResultsForSearch: Infinity
    });

    $('#kt_select_contract, #kt_formation_contract_validate').select2({
        placeholder: "Select contract",
        minimumResultsForSearch: Infinity
    });

    $('#kt_select_area, #kt_formation_area_validate').select2({
        placeholder: "Select area",
        minimumResultsForSearch: Infinity
    });

    $('#kt_select_nation, #kt_formation_nation_validate').select2({
        placeholder: "Select country",
        minimumResultsForSearch: Infinity
    });    

    $('#kt_select_sex, #kt_formation_sex_validate').select2({
        placeholder: "Select sex",
        minimumResultsForSearch: Infinity
    });

    // currency format
    $("#kt_input_budget").inputmask('999.999', {
        numericInput: true
    }); //123456  =>  € ___.__1.234,56

    //mi rimuovo il pulsante formattin text dell'editor
    $(".ql-clean").remove();
});

function selectValuta(valuta, idValuta) {
    $('#job_valuta').attr("value", idValuta);
    $('#job_valuta').html(valuta);
}