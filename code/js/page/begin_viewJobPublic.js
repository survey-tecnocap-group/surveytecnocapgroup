
$(document).ready(function() {
    getContentFromFilePublic();
});


function getContentFromFilePublic() {
    var filename=$("#filename").val();
    var language=$("#language").val();

    $.post('./code/writeContentToFile.php', {
        id_job: filename,
        get_language: language, 
        type: "read"
    }, function( data ) {
        $("#content_html").html(data);
    });
}