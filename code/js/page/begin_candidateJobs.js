let language=$("#language").val();

function getValueUser(idJob, language) {    
    var arrayExperience=getRepeaterExperience();
    var arrayEducation=getRepeaterEducation();
    var arrayInputForm=getInputForm();
        
    $.post('./code/function/alg_function_call.php', {
    type: 'add_careers',
    idJob:idJob,
    language: language,
    arrayUser: arrayInputForm,
    arrayExperience: arrayExperience,
    arrayEducation: arrayEducation 
    }, function(data) {
        //console.log(data);
        var json=JSON.parse(data);

        if(json.response==1) {
            $.post('./code/api/api_email.php', {
                idJob:idJob,
                detailCareers: json.detailCareers,
                detailUser: arrayInputForm,
                }, function(dataCareers) {
                    //window.location.href = "thanks.php";
                }
            );
            successAddCareers();
        }        
    }
);   
}

//mi salvo il file image_profile online
var image_profile="";

function getFileCover(){
    var time_unique=Date.now();
    var dataURL=($(".image-input-wrapper").css('background-image'));
    
    $.post('./code/save_image.php', {
        imgBase64: dataURL, 
        type: 'add_careers',
        time_unique: time_unique
    }, function(data) {
        var filename=data.split(";;");
        image_profile=filename[1];
    });
}

$("#profile-img").change(function(){
    setTimeout(function(){ 
        getFileCover();    
    }, 500);
});

function resetImageProfile() {
    image_profile="";
}

function getInputForm() {
    var time_unique=Date.now();

    var firstname=$("#user_firstname").val();
    if(!(firstname)) { formNoComplete(); return; }

    var lastname=$("#user_lastname").val();
    if(!(lastname)) { formNoComplete(); return; }

    var location=$("#user_location").val();
    if(!(location)) { formNoComplete(); return; }

    var nazione=$("#kt_select_nation").val();
    if(!(nazione)) { formNoComplete(); return; }

    var sex=$("#kt_select_sex").val();
    //if(!(sex)) { formNoComplete(); exit(); }

    var datebirth=$("#user_datebirth").val();
    //if(!(datebirth)) { formNoComplete(); exit(); }

    var phone=$("#user_phone").val();
    if(!(phone)) { formNoComplete(); return; }

    var email=$("#user_email").val();
    if(!(email)) { formNoComplete(); return; }

    var linkedin=$("#user_linkedin").val();
    var facebook=$("#user_facebook").val();
    var twitter=$("#user_twitter").val();
    var website=$("#user_website").val();
    var message=$("#user_messagehiring").val();
    
    var id_resume=$("#file_cv").val();
    
    var id_cover=$("#file_cover").val();
    var user_experience=$("#user_experience").val();
    //if((!(id_resume)) && (!(user_experience))) { formNoComplete(); exit(); }

    return {'firstname':firstname,
            'lastname':lastname,
            'image_profile':image_profile,
            'id_resume':id_resume,
            'id_cover':id_cover,
            'location':location,
            'nazione':nazione,
            'sex':sex,
            'datebirth':datebirth,
            'phone':phone,
            'email':email,
            'linkedin':linkedin,
            'facebook':facebook,
            'twitter':twitter,
            'website':website,
            'messagehiring':message };

}

function getRepeaterExperience() {
    //experience input
    var arrayExperienceTitle=new Array();
    var arrayExperienceCompany=new Array();
    var arrayExperienceLocation=new Array();
    var arrayExperienceNation=new Array();
    var arrayExperienceDescription=new Array();
    var arrayExperienceStartDate=new Array();
    var arrayExperienceEndDate=new Array();
    var arrayExperienceCurrentlyWork=new Array();

    jQuery('input[name*="user_experience_title"]').each(function(e) { 
        if($(this).val()) {
            arrayExperienceTitle.push($(this).val()); 
        }
    });

    if(arrayExperienceTitle.length) {
        jQuery('input[name*="user_experience_company"]').each(function(e) { arrayExperienceCompany.push($(this).val()); });
        jQuery('input[name*="user_experience_location"]').each(function(e) { arrayExperienceLocation.push($(this).val()); });
        jQuery('input[name*="user_experience_nation"]').each(function(e) { arrayExperienceNation.push($(this).val()); });
        jQuery('textarea[name*="user_experience_description"]').each(function(e) { arrayExperienceDescription.push($(this).val()); });
        jQuery('input[name*="user_experience_startDate"]').each(function(e) { arrayExperienceStartDate.push($(this).val()); });
        jQuery('input[name*="user_experience_endDate"]').each(function(e) { arrayExperienceEndDate.push($(this).val()); });
        jQuery('input[name*="user_experience_currentlywork"]').each(function(e) { arrayExperienceCurrentlyWork.push($(this).is(':checked')); });
    
        return {'title':arrayExperienceTitle,
                'company':arrayExperienceCompany,
                'location':arrayExperienceLocation,
                'nation':arrayExperienceNation,
                'description':arrayExperienceDescription,
                'startDate':arrayExperienceStartDate,
                'endDate':arrayExperienceEndDate,
                'currently_work':arrayExperienceCurrentlyWork};
    }   
    return 0;
}

function getRepeaterEducation() {
    //education input
    var arrayEducationIstituion=new Array();
    var arrayEducationDegree=new Array();
    var arrayEducationLocation=new Array();
    var arrayEducationNation=new Array();
    var arrayEducationDescription=new Array();
    var arrayEducationStartDate=new Array();
    var arrayEducationEndDate=new Array();
    var arrayEducationCurrentlyAttend=new Array();

    if(arrayEducationIstituion.length) {
        jQuery('input[name*="user_education_istitution"]').each(function(e) { arrayEducationIstituion.push($(this).val()); });
        jQuery('input[name*="user_education_degree"]').each(function(e) { arrayEducationDegree.push($(this).val()); });
        jQuery('input[name*="user_education_location"]').each(function(e) { arrayEducationLocation.push($(this).val()); });
        jQuery('input[name*="user_education_nation"]').each(function(e) { arrayEducationNation.push($(this).val()); });
        jQuery('textarea[name*="user_education_description"]').each(function(e) { arrayEducationDescription.push($(this).val()); });
        jQuery('input[name*="user_education_startDate"]').each(function(e) { arrayEducationStartDate.push($(this).val()); });
        jQuery('input[name*="user_education_endDate"]').each(function(e) { arrayEducationEndDate.push($(this).val()); });
        jQuery('input[name*="user_education_currentlyattend"]').each(function(e) { arrayEducationCurrentlyAttend.push($(this).is(':checked')); });

        return {'istitution':arrayEducationIstituion,
                'degree':arrayEducationDegree,
                'location':arrayEducationLocation,
                'nation':arrayEducationNation,
                'description':arrayEducationDescription,
                'startDate':arrayEducationStartDate,
                'endDate':arrayEducationEndDate,
                'currently_attend':arrayEducationCurrentlyAttend};
    }
    return 0;
}

function formNoComplete() {
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'form_no_complete',
        language: language, 
    }, function( data ) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, '');   
    });
}

function successAddCareers() {
    $.post('./code/function/alg_function_sweetalert.php', {
        type: 'add_careers_success',
        language: language, 
    }, function(data) {
        detailMessage=JSON.parse(data);
        msgAlert((detailMessage.title).toUpperCase(), detailMessage.description, detailMessage.type, detailMessage.btn_left, detailMessage.btn_right, afterMsgSuccessAdded);   
        clearInput();
    });
}

function afterMsgSuccessAdded() {
    window.location.href = "index.php?language="+language;
}

function clearInput() {
    $("#user_firstname").val('');
    $("#user_lastname").val('');
    $("#user_location").val('');
    $("#user_phone").val('');
    $("#user_email").val('');
    $("#user_linkedin").val('');
    $("#user_facebook").val('');
    $("#user_twitter").val('');
    $("#user_website").val('');
    $("#user_messagehiring").val('');
}