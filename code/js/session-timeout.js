"use strict";

var sessionTime=0;
var sessionTimeAfter=0;
var sessionTitle="";
var sessionMessage="";
var sessionRedirectMessage="";

var KTSessionTimeoutDemo = function () {
    var initDemo = function () {
        $.sessionTimeout({
            title: sessionTitle,
            message: sessionMessage,
            keepAliveUrl: '',
            redirUrl: 'login.php?timeout=1',
            logoutUrl: 'login.php?logout=1',
            warnAfter: sessionTime, //warn after 5 seconds
            redirAfter: sessionTimeAfter, //redirect after 15 secons,
            ignoreUserActivity: true,
            countdownMessage: sessionRedirectMessage,
            countdownBar: true
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            initDemo();
        }
    };
}();

jQuery(document).ready(function() {
    sessionTime=parseInt($("#session_time").val());
    sessionTimeAfter=parseInt(sessionTime+10000);
    sessionTitle=$("#session_title").val();
    sessionMessage=$("#session_message").val();
    sessionRedirectMessage=$("#session_redirect_message").val();

    KTSessionTimeoutDemo.init();
});
