	
    function msgAlert(title, description, type, txtBtnLeft, txtBtnRight, functionCallback) {
        var showRightBtn=true;
        if(txtBtnRight=="")
            showRightBtn=false;

		Swal.fire({
			title: title,
			text: description,
			icon: type,
			buttonsStyling: false,
			confirmButtonText: txtBtnLeft,
			showCancelButton: showRightBtn,
			cancelButtonText: txtBtnRight,
			customClass: {
				confirmButton: "btn btn-primary",
				cancelButton: "btn btn-default"
            }
            }).then(function (result) {
            if (result.value) {
                /*
                Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                )
                */
               if(functionCallback!="")
                    functionCallback();
            }
        });
    }