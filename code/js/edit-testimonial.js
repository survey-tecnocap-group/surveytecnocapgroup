"use strict";

jQuery(document).ready(function() {
});

KTUtil.ready(function() {
    var avatar = new KTImageInput('kt_user_edit_avatar_testimonial');
    var avatarEdit = new KTImageInput('kt_user_edit_avatar_edit');
});


//mi salvo il file image_profile online
var image_profile="";

function getFileImageTestimonial(){

    var time_unique=Date.now();
    var dataURL=($("#edit_image_testimonial").css('background-image'));
    
    $.post('./code/save_image.php', {
        imgBase64: dataURL, 
        type: 'add_testimonial',
        time_unique: time_unique
    }, function(data) {
        console.log(dataURL);
        var filename=data.split(";;");
        image_profile=filename[1];
    });
}

$("#profile-img_edit_testimonial").change(function(){
    setTimeout(function(){ 
        getFileImageTestimonial();    
    }, 500);
});

$("#profile-img_testimonial").change(function(){
    setTimeout(function(){ 
        getFileImageTestimonialAdd();    
    }, 500);
});

function resetImageProfileTestimonial() {
    image_profile="";
    let resetImage="assets/media/users/blank.png";
    $("#kt_user_edit_avatar_edit").css("background-image", "url('"+resetImage+"')");
    $("#removeIconEditTetimonial").css("display", "none");
}


function getFileImageTestimonialAdd(){
    //return;
    var time_unique=Date.now();
    var dataURL=($("#add_image_testimonial").css('background-image'));
    
    $.post('./code/save_image.php', {
        imgBase64: dataURL, 
        type: 'add_testimonial',
        time_unique: time_unique
    }, function(data) {
        var filename=data.split(";;");
        image_profile=filename[1];
    });
}

