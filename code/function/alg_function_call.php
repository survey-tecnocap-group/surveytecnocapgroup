
<?php 

    include "../class/alg_mysql_class.php";
    include "../class/alg_function_class.php";
    include "../alg_connection_function.php";
	include "../alg_setting.php";

    session_start();

    //in base alla TYPE capisco quale funzione chiamare
    if(isset($_POST['type'])) {
        switch($_POST['type']) {
            case "login":
                $username=$_POST['username'];
                $password=$_POST['password'];
                $remember="";
                $rowUser=$alg_class_myfunction->alg_fnt_checkLoginFull($username,$password);
                if($rowUser>0) {
                    $_SESSION["tecnocap-careers"]=$rowUser['id_user'];
                    setcookie("tecnocap-careers", "czh", strtotime("+1 year"), "/");
                    if(isset($_POST['remember'])) {
                        $remember=$_POST['remember'];
                    }
                    echo "1";
                }
                else {
                    if($rowUser<0)
                        echo "-1";
                    else
                        echo "0";
                }
                break;
            
            case "activeJob":
                $idJob=$_POST['idJob'];
                $isActive=$_POST['is_active'];
                echo $alg_class_myfunction->alg_fnt_activeJob($idJob, $isActive);
                break;
            
            case "approvedCareers":
                $idJob=$_POST['idJob'];
                $idCareers=$_POST['idCareers'];
                $idUser=$_POST['idUser'];
                echo $alg_class_myfunction->alg_fnt_approvedCareers($idJob, $idCareers, $idUser);
                break;

            case "removeApprovedCareers":
                $idJob=$_POST['idJob'];
                $idCareers=$_POST['idCareers'];
                echo $alg_class_myfunction->alg_fnt_removeApprovedCareers($idJob, $idCareers);
                break;
            
            case "update_job":
                $idJob=$_POST['idJob'];
                $detail=$_POST['detail'];
                echo $alg_class_myfunction->alg_fnt_updateJob($idJob, $detail);
                break;

            case "delete_job":
                $idJob=$_POST['id_job'];
                echo $alg_class_myfunction->alg_fnt_deleteJob($idJob);
                break;
            
            case "add_job":
                $detail=$_POST['detail'];
                $language=$_POST['language'];
                echo $alg_class_myfunction->alg_fnt_addJob($detail, $language);
                break;

            case "add_careers":
                $arrayUser=$_POST['arrayUser'];
                $arrayExperience=$_POST['arrayExperience'];
                $arrayEducation=$_POST['arrayEducation'];
                $idJob=$_POST['idJob'];
                $language=$_POST['language'];
                $arrayList=$alg_class_myfunction->alg_fnt_addCarrersToJob($idJob, $language, $arrayUser, $arrayExperience, $arrayEducation);
                echo json_encode($arrayList);    
                break;
            
            case "add_country":
                $value=$_POST['value'];
                echo $alg_class_myfunction->alg_fnt_addNazione($value);                
                break;
            
            case "add_role":
                $value=$_POST['value'];
                echo $alg_class_myfunction->alg_fnt_addRole($value);                
                break;
            
            case "add_formation":
                $value=$_POST['value'];
                echo $alg_class_myfunction->alg_fnt_addFormation($value);                
                break;

            case "add_contract":
                $value=$_POST['value'];
                echo $alg_class_myfunction->alg_fnt_addContract($value);                
                break;

            case "add_area":
                $value=$_POST['value'];
                echo $alg_class_myfunction->alg_fnt_addArea($value);                
                break;

            case "active_account":
                $idUser=$_POST['idUser'];
                $active=$_POST['active'];
                echo $alg_class_myfunction->alg_fnt_activeAccount($idUser, $active);
                break;

            case "get_UserExtraDetail":
                $idUser=$_POST['idUser'];
                $valueArray=$alg_class_myfunction->alg_fnt_getDetailRole($idUser);
                $valueArrayUserDetail=$alg_class_myfunction->alg_fnt_getOtherDetailUser($idUser);
                $js_array=json_encode(array_merge($valueArray, $valueArrayUserDetail));
                echo $js_array;
                break;

            case "get_TestimonailExtraDetail":
                $idUser=$_POST['idUser'];
                $valueArrayUserDetail=$alg_class_myfunction->alg_fnt_getTestimonial($idUser);
                $js_array=json_encode($valueArrayUserDetail);
                echo $js_array;
                break;

            case "update_UserExtraDetail":
                $idUser=$_POST['id_user'];
                $idRole=$_POST['id_role'];
                $detail=$_POST['detail'];
                echo $alg_class_myfunction->alg_fnt_updateExtraUser($idUser, $idRole, $detail);
                break;
            
            case "update_UserDetail":
                $idUser=$_POST['id_user'];
                $detail=$_POST['detail'];
                echo $alg_class_myfunction->alg_fnt_updateUser($idUser, $detail);
                break;

            case "update_Testimonial":
                $idUser=$_POST['id_user'];
                $detail=$_POST['detail'];
                echo $alg_class_myfunction->alg_fnt_updateTestimonial($idUser, $detail);
                break;
            
            case "delete_account":
                $idUser=$_POST['id_user'];
                echo $alg_class_myfunction->alg_fnt_deleteAccount($idUser);
                break;

            case "delete_testimonial":
                $idUser=$_POST['id_user'];
                echo $alg_class_myfunction->alg_fnt_deleteTestimonial($idUser);
                break;
            
            case "delete_SettingSelected":
                $id=$_POST['id'];
                $type=$_POST['choose'];
                echo $alg_class_myfunction->delete_SettingSelected($id, $type);
                break;

            case "checkCanDelete_SettingSelected":
                $id=$_POST['id'];
                $type=$_POST['choose'];
                echo $alg_class_myfunction->checkCanDelete_SettingSelected($id, $type);
                break;

            case "add_user":
                $detail=$_POST['detail'];
                echo $alg_class_myfunction->alg_fnt_addUser($detail);
                break;

            case "add_testimonial":
                $detail=$_POST['detail'];
                echo $alg_class_myfunction->alg_fnt_addTestimonial($detail);
                break;

            case "get_ListTypeForm":
                $choose=$_POST['choose'];
                $arrayList=array();
                switch ($choose) {
                    case "country":
                        $arrayList=$alg_class_myfunction->alg_fnt_getListNazione();
                        break;

                    case "role":
                        $arrayList=$alg_class_myfunction->alg_fnt_getListJobType();
                        break;
                    
                    case "formation":
                        $arrayList=$alg_class_myfunction->alg_fnt_getListFormazione();
                        break;
                    
                    case "contract":
                        $arrayList=$alg_class_myfunction->alg_fnt_getListContract();
                        break;

                    case "area":
                        $arrayList=$alg_class_myfunction->alg_fnt_getListArea();
                        break;
                }
                echo json_encode($arrayList);
                break;
            
            case "update_ListTypeForm":
                $detail=$_POST['detail'];
                switch ($detail['type']) {
                    case "country":
                        echo $alg_class_myfunction->alg_fnt_updateNazione($detail['id_selected'], $detail['value']);
                        break;

                    case "role":
                        echo $alg_class_myfunction->alg_fnt_updateJobType($detail['id_selected'], $detail['value']);
                        break;
                    
                    case "formation":
                        echo $alg_class_myfunction->alg_fnt_updateFormazione($detail['id_selected'], $detail['value']);
                        break;
                    
                    case "contract":
                        echo $alg_class_myfunction->alg_fnt_updateContract($detail['id_selected'], $detail['value']);
                        break;

                    case "area":
                        echo $alg_class_myfunction->alg_fnt_updateArea($detail['id_selected'], $detail['value']);
                        break;
                }
                break;

            case "get_Testimonial":
                $idTestimonial=$_POST['idTestimonial'];
                echo json_encode($alg_class_myfunction->alg_fnt_getTestimonial($idTestimonial));
                break;
        }   
    }

?>