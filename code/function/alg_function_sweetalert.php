
<?php 

    include "../class/alg_mysql_class.php";
    include "../class/alg_function_class.php";
    include "../alg_connection_function.php";
	include "../alg_setting.php";

    session_start();

    //in base alla TYPE capisco quale funzione chiamare
    if(isset($_POST['type'])) {
        switch($_POST['type']) {
            case "edit_job":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("edit_job", $language);
                echo json_encode($arrayValue);
                break;
            
            case "update_job":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("update_job", $language);
                echo json_encode($arrayValue);
                break;
            
            case "error_update_job":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("error_update_job", $language);
                echo json_encode($arrayValue);
                break;

            case "update_job_success":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("update_job_success", $language);
                echo json_encode($arrayValue);
                break;
            
            case "delete_job":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("delete_job", $language);
                echo json_encode($arrayValue);
                break;
            
            case "delete_account":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("delete_account", $language);
                echo json_encode($arrayValue);
                break;

            case "delete_testimonial":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("delete_testimonial", $language);
                echo json_encode($arrayValue);
                break;
            
            case "add_job_success":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("add_job_success", $language);
                echo json_encode($arrayValue);
                break;
            
            case "form_no_complete":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("form_no_complete", $language);
                echo json_encode($arrayValue);
                break;
            
            case "confirm_cancel":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("confirm_cancel", $language);
                echo json_encode($arrayValue);
                break;

            case "add_careers_success":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("add_careers_success", $language);
                echo json_encode($arrayValue);
                break;

            case "confirm_new_member":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("confirm_new_member", $language);
                echo json_encode($arrayValue);
                break;

            case "confirm_new_testimonial":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("confirm_new_testimonial", $language);
                echo json_encode($arrayValue);
                break;

            case "update_quick_edit":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("update_quick_edit", $language);
                echo json_encode($arrayValue);
                break;

            case "account_disabled":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("account_disabled", $language);
                echo json_encode($arrayValue);
                break;

            case "account_error":
                $language=$_POST['language'];
                $arrayValue=$alg_class_myfunction->alg_get_sweetAlertMsg("account_error", $language);
                echo json_encode($arrayValue);
                break;
        }
        
    }

?>