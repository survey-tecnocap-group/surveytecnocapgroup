<?php
header('Content-Type: text/html; charset=utf-8');
include "class/alg_function_class.php";
include "class/alg_mysql_class.php";
include "class/alg_connection_function.php";

//includo le variabili definite globali
include ("setting/define_variable.php");

//includo il SETTING per il DEFINE per i valori interessati alle pagine
include ("setting/define_page.php");
								



?>

<!DOCTYPE html>

<html lang="en">
<!--begin::Head-->
<head><base href="">
	<meta charset="utf-8" />
	<title><?php echo PAGE_TITLE; ?></title>
	<meta name="description" content="<?php echo PAGE_DESCRIPTION; ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<link rel="canonical" href="<?php echo LINK_CANONICAL; ?>" />
	<!--begin::Fonts-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
	<!--end::Fonts-->
	<!--begin::Page Vendors Styles(used by this page)-->
	<link href="assets/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />

	<!--end::Page Vendors Styles-->

	<link href="assets/css/panel.css" rel="stylesheet" type="text/css" />

	<!--begin::Global Theme Styles(used by all pages)-->
	<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Global Theme Styles-->
	<!--begin::Layout Themes(used by all pages)-->
	<!--end::Layout Themes-->
	<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />

	<!-- IMPORT RATING START -->
	<link rel="canonical" href="Https://preview.keenthemes.com/metronic8" />
	<link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
	<!--begin::Fonts-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700" />
	<!--end::Fonts-->
	<!--begin::Page Vendor Stylesheets(used by this page)-->
	<link href="assets/plugins/custom/prismjs/prismjs.bundle.css" rel="stylesheet" type="text/css" />
	<!--end::Page Vendor Stylesheets-->
	<!--begin::Global Stylesheets Bundle(used by all pages)-->
	<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
	<!-- IMPORT RATING END -->
</head>
<!--end::Head-->
<!--begin::Body-->
<body id="kt_body" class="header-mobile-fixed subheader-enabled aside-enabled aside-fixed aside-secondary-enabled page-loading" style="background-color: #F5F5F5 !important;">
	<!--begin::Main-->
	<!--begin::Header Mobile-->
	<?php include ("pages/templates/header_mobile.php"); 

	?>
	<!--end::Header Mobile-->
	<div class="d-flex flex-column flex-root">
		<!--begin::Page-->
		<div class="d-flex flex-row flex-column-fluid page">
			<!--begin::Aside-->
				<!--<div class="aside aside-left d-flex aside-fixed" id="kt_aside">
				 php include ("pages/templates/menu.php");
				</div> -->
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="flex-row-fluid" id="kt_wrapper">

					<!--begin::Header-->
					<?php include ("pages/form_header.php"); 

					?>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->
						<?php include ("pages/form_subheader.php"); 

						?>
						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container" id="mainBody">

								<!--begin::Survey Title and Description -->

								<main class="cd-main-content">
									<div class="survey-title-tecnocap">Errore nel caricamento del sondaggio</div>
									


								<br>


							<div class="survey-subtitle-tecnocap">

									<div class="survey-subtitle-tecnocap">Il link che hai provato ad aprire è scaduto oppure il sondaggio è stato rimosso.</div>
									



							</div>
						</main>
						<div class="cd-panel cd-panel--from-right js-cd-panel-main" style="z-index:999">
							<header class="cd-panel__header">
								<h1>Algoritmica Lab</h1>
								<a href="#0" class="cd-panel__close js-cd-close">Close</a>
							</header>
							<div class="cd-panel__container">
								<div class="cd-panel__content">

								</div> <!-- cd-panel__content -->
							</div> <!-- cd-panel__container -->
						</div> <!-- cd-panel -->


					</div>
					<!--end::Container-->
				</div>
				<!--end::Entry-->
			</div>




			<!--end::Content-->
			<!--begin::Footer-->
			<!--doc: add "bg-white" class to have footer with solod background color-->

			<!--end::Footer-->
		</div>
		<!--end::Wrapper-->
	</div>
	<!--end::Page-->
</div>
<script>var HOST_URL = "<?php echo HOME_WEBAPP; ?>";</script>
<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#1BC5BD", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#6993FF", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#1BC5BD", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#E1E9FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
<!--end::Global Config-->
<!--begin::Global Theme Bundle(used by all pages)-->
<script src="assets/plugins/global/plugins.bundle.js"></script>
<script src="assets/js/scripts.bundle.js"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<script src="assets/plugins/custom/prismjs/prismjs.bundle.js"></script>
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="assets/js/custom/documentation/documentation.js"></script>
<script src="assets/js/custom/documentation/search.js"></script>

<!--end::Page Scripts-->
</body>
<footer>
	<?php include ("pages/templates/footer.php"); ?>
</footer>
<!--end::Body-->
</html>