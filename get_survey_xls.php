<?php
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);




use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;




require_once 'vendor/phpoffice/phpspreadsheet/samples/Bootstrap.php';


include "class/alg_function_class.php";
include "class/alg_mysql_class.php";


    // istanza della classe
$alg_class_mysql = new MySqlClass();    
$alg_var_dbConnect=$alg_class_mysql->openConnection();

    //function global

$alg_class_myfunction = new FunctionPrimaryClass();
$alg_class_myfunction->alg_fnt_setConnection($alg_var_dbConnect);


$id_survey = '1';

$rowSurvey = $alg_class_myfunction->alg_fnt_getAnsweredSurvey($id_survey);

$singleSurvey = $alg_class_myfunction->alg_fnt_getFullSurvey($id_survey);

//DATI SURVEY SINGOLO
$surveyData = json_decode($singleSurvey, true);

//DATI INTERVISTATI
$surveyList = json_decode($rowSurvey, true); 


$helper = new Sample();
if ($helper->isCli()) {
    $helper->log('This example should only be run from a Web Browser' . PHP_EOL);

    return;
}
// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document properties
$spreadsheet->getProperties()->setCreator('Survey Tecnocap Group')
->setLastModifiedBy('Algoritmica Lab')
->setTitle('Office 2007 XLSX Test Document')
->setSubject('Office 2007 XLSX Test Document')
->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
->setKeywords('office 2007 openxml php')
->setCategory('Test result file');


//IN BASE ALLA COLONNA - RIGA - VALORE RIEMPO UNA CELLA
$spreadsheet->setActiveSheetIndex(0)->setCellValueByColumnAndRow(1, 1, "Ruolo");
$spreadsheet->setActiveSheetIndex(0)->setCellValueByColumnAndRow(2, 1, "Azienda");
$spreadsheet->setActiveSheetIndex(0)->setCellValueByColumnAndRow(3, 1, "Nazione");
$spreadsheet->setActiveSheetIndex(0)->setCellValueByColumnAndRow(4, 1, "Email");
$spreadsheet->setActiveSheetIndex(0)->setCellValueByColumnAndRow(5, 1, "Telefono");

//INDICE DA CUI FAR INIZIARE L'INTESTAZIONE DEL SONDAGGIO
$start_survey_index_title = 6;


for ($i = 0; $i < count($surveyData[1]['all_categories']); $i++){

            $spreadsheet->setActiveSheetIndex(0)->setCellValueByColumnAndRow($start_survey_index_title, 1, $surveyData[1]['all_categories'][$i]['single_category']['title_category']);
            $start_survey_index_title++;
           

            foreach($surveyData[1]['all_categories'][$i]['single_category']['questions_category'] as $cat){
                $spreadsheet->setActiveSheetIndex(0)->setCellValueByColumnAndRow($start_survey_index_title, 1, $cat['title_question']);
                $start_survey_index_title++;
            } 
        } 



// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('Simple');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="01simple.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;

?>

